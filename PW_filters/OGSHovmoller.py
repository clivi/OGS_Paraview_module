#Source for Hovmoller Plot
#
#Cosimo Livi, MHPC 2017

Name  = 'OGSHovmoeller'
Label = 'OGS Hovmoeller Plot'
Help  = 'Produces Hovmoller plot for selected variables'

NumberOfInputs = 0 # programable source
InputDataType = ''
OutputDataType = 'vtkTable'
ExtraXml = ''

Properties = dict(
	Mesh_resolution      = 2, # To work with enum 1: high 2: mid 3: low
	Path_to_mesh         = '/home/clivi/OGS/MESHMASK/',
	Path_to_python       = '/home/clivi/OGS/bit.sea/',
	Path_to_file     	 = '/data/',
	Start_date			 = '19000101',
	End_date 			 = '22000101',
	# Biogeochemical variables
	Variable 			 = 'N3n',
	
	path_to_library="/home/cosimo/MHPC/OGS/clib/src",


	# Coast Selection
	coast = True,    # Value of 1 in CoastMask
	open_sea = True,  # Value of 2 in CoastMask
	

	# Sub Basins Selection

	Alboran_Sea                      = True, # alb  => 1
	South_Western_Mediterranean_west = True, # swm1 => 2
	South_Western_Mediterranean_east = True, # swm2 => 3
	North_Western_Mediterranean      = True, # nwm  => 4
	Northern_Tyrrhenian              = True, # tyr1 => 5
	Southern_Tyrrhenian              = True, # tyr2 => 6
	Western_Ionian                   = True, # ion1 => 7
	Eastern_Ionian                   = True, # ion2 => 8
	Northern_Ionian                  = True, # ion3 => 9
	Northern_Adriatic                = True, # adr1 => 10
	Southern_Adriatic                = True, # adr2 => 11
	Western_Levantine                = True, # lev1 => 12
	Northern_Levantine               = True, # lev2 => 13
	Southern_Levantine               = True, # lev3 => 14
	Eastern_Levantine                = True, # lev4 => 15
	Aegean_Sea                       = True,  # aeg  => 16

	# Statistics Options
	Statistic_type=1,
	#Average = True,
	#Std = False,
	#Percentili = False,
	#Minima = False,
	#Maxima = False,
	perc_to_visualize = "0.05"

);


def RequestInformation():
	executive = self.GetExecutive()
	outInfo = executive.GetOutputInformation(0)
	dataType = 10 # VTK_FLOAT
	numberOfComponents = 1
	vtk.vtkDataObject.SetPointDataActiveScalarInfo(outInfo, dataType, numberOfComponents)


def RequestData():
	import sys, os, glob
	import numpy as np
	import vtk
	sys.path.append(Path_to_python);
	from vtk.util import numpy_support as npvtk
	from OGSParaviewSuite import OGSvtk,OGSStatistics
	from mpi4py import MPI

	comm = MPI.COMM_WORLD
	rank = comm.Get_rank()
	size = comm.Get_size()

	if rank == 0:

		if Variable == 'Velocity' or Variable == 'Velocity (forcings)':
			raise ValueError('ERROR: Statistics analysis on velocity not implemented yet ')
		# Get info object
		executive = self.GetExecutive()
		outInfo = executive.GetOutputInformation(0)
		# Set type of data
		dataType = 10 # VTK_FLOAT
		numberOfComponents = 1
		vtk.vtkDataObject.SetPointDataActiveScalarInfo(outInfo,dataType,numberOfComponents)
		# Scan the AVE_FREQ folder for data
		if Variable in ['vosaline','votemper','votkeavt','sowindsp','sowaflcd']:
			fileList = glob.glob( "%s/ave.*.phys.nc" % (Path_to_file) ); 
		else:
			fileList = glob.glob( "%s/ave.*.%s.nc" % (Path_to_file,Variable) ); 
		fileList.sort(); l = len(fileList);
		# Set time steps
		dateList = []
		for ii in range(0,l):
			date = ''.join(list(os.path.basename(fileList[ii])[4:12]))
			if (int(Start_date) <= int(date)) and (int(End_date)>= int(date)):
				dateList.append( fileList[ii].split('.')[1] );


		perc = np.array([0.05,0.25,0.5,0.75,0.95],dtype=np.float64)

		output = vtk.vtkTable()

		
		# Initialize class
		OGS = OGSvtk(Path_to_mesh);
		output.GetFieldData().AddArray( OGS.createVTKstrf("Plot Type","Hovmoller") );

		# Mesh resolution
		if Mesh_resolution == 1: OGS.SetResolution('high');
		if Mesh_resolution == 2: OGS.SetResolution('mid');
		if Mesh_resolution == 3: OGS.SetResolution('low');

		# Main Loop over dates
		
		coastness = ""
		subbasins = []
		aux = np.zeros(OGS.jpk-1)



		# Define Domain for statistics
		if coast and open_sea:     coastness = "everywhere"; output.GetFieldData().AddArray( OGS.createVTKstrf('Coast level:','everywhere'));

		if coast and not open_sea: coastness = "coast";output.GetFieldData().AddArray( OGS.createVTKstrf('Coast level:','coast'));

		if not coast and open_sea: coastness = "open_sea";output.GetFieldData().AddArray( OGS.createVTKstrf('Coast level:','open_sea'));
		if not coast and not open_sea: 
			raise ValueError('ERROR: cannot perform analysis on only the ground') # Just the ground

		if Alboran_Sea:						 subbasins.append('alb')
		if South_Western_Mediterranean_west: subbasins.append('swm1')
		if South_Western_Mediterranean_east: subbasins.append('swm2')
		if North_Western_Mediterranean: 	 subbasins.append('nwm')
		if Northern_Tyrrhenian: 			 subbasins.append('tyr1')
		if Southern_Tyrrhenian: 			 subbasins.append('tyr2')
		if Western_Ionian: 					 subbasins.append('ion1')
		if Eastern_Ionian: 					 subbasins.append('ion2')
		if Northern_Ionian: 				 subbasins.append('ion3')
		if Northern_Adriatic: 				 subbasins.append('adr1')
		if Southern_Adriatic: 				 subbasins.append('adr2');
		if Western_Levantine: 				 subbasins.append('lev1');
		if Northern_Levantine: 				 subbasins.append('lev2');
		if Southern_Levantine: 				 subbasins.append('lev3');
		if Eastern_Levantine: 				 subbasins.append('lev4');
		if Aegean_Sea: 						 subbasins.append('aeg');


		output.GetFieldData().AddArray( OGS.createVTKstrf('Sub-basins:',','.join(subbasins)))

		

		depth = OGS.nav_lev[:OGS.jpk-1]/1000.
		depth_vtk = npvtk.numpy_to_vtk(depth,1)
		depth_vtk.SetName("depth (m)")
		output.AddColumn(depth_vtk)

		for date in dateList:
		
			# Create a VTK rectilinear grid
			rg = OGS.CreateRectilinearGrid();
			# Initialize the sub masks
			OGS.AddMask(rg,AddSubMask=True,AddCoastMask=True,AddDepth=False);



			# Process biogeochemical variables
			# Postprocess variables in batch mode
			if Variable in ['vosaline','votemper','votkeavt','sowindsp','sowaflcd']:
				fname = "%s/ave.%s.phys.nc" % (Path_to_file,date)
			else:
				fname = "%s/ave.%s.%s.nc" % (Path_to_file,date,Variable)
			if os.path.exists(fname):
				# Load from NetCDF
				data = OGS.LoadNetCDF.GetVariable(fname,Variable);
				# Write data
				rg.GetCellData().AddArray( OGS.createVTKscaf(Variable,data) );
				# Postprocess composite variables
				if Variable == "Chlorophyll":
					computedVars.append("Chlorophyll")
					data = OGS.LoadNetCDF.GetVariable("%s/ave.%s.P1l.nc" % (Path_to_file,File_date),"P1l") \
					     + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P2l.nc" % (Path_to_file,File_date),"P2l") \
					     + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P3l.nc" % (Path_to_file,File_date),"P3l") \
					     + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P4l.nc" % (Path_to_file,File_date),"P4l");
					rg.GetCellData().AddArray( OGS.createVTKscaf("Chlorophyll",data) );	
			else:
				raise ValueError("ERROR: file not found")

			# Statistic session
			var = Variable
			Statistic = OGSStatistics(path_to_library)

			ArealStatistics   = Statistic.FastArealStatistic(OGS,rg,subbasins,coastness,var,perc)
				
			if Statistic_type==1:
				aux = np.array(ArealStatistics[:,0])
				avg = npvtk.numpy_to_vtk(aux,1)
				avg.SetName(var+" Avg "+str(date))
				output.AddColumn(avg)
			if Statistic_type==2:
				aux1 = np.array(ArealStatistics[:,1])
				std = npvtk.numpy_to_vtk(aux1,1)
				avg.SetName(var+" std "+str(date))
				output.AddColumn(std)

			if Statistic_type==3:
				aux2 = np.array(ArealStatistics[:,2])
				minima = npvtk.numpy_to_vtk(aux2,1)
				minima.SetName(var+" Minima "+str(date))
				output.AddColumn(minima)
			if Statistic_type==4:
				for p in range(len(perc)):
					if str(perc[p])==perc_to_visualize:
						aux3 = np.array(ArealStatistics[:,p+3])
						percentili = npvtk.numpy_to_vtk(aux3,1)
						percentili.SetName(var+" "+str(perc[p])+"wp "+str(date))
						output.AddColumn(percentili)
						
			if Statistic_type==5:
				aux4 = np.array(ArealStatistics[:,-1])
				maxima = npvtk.numpy_to_vtk(aux4,1)
				maxima.SetName(var+" Maxima "+str(date))
				output.AddColumn(maxima)


		self.GetOutput().ShallowCopy(output)




















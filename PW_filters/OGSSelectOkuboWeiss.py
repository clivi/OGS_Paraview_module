# Select coast, ParaView programable filter script.
#
# Programable filter intended to select a coast type from 
# OGS data sets using the CoastMask variable. Afterwards
# removes the CoastMask array since it is no longer needed.
#
# This filter is to be applied after loading the data or
# computing the OkuboWeiss criterion.
#
# Coast values are:
#	1: coast
#	2: open sea
#
# Combined form all the dataset.
#
# Arnau Miro, SoHPC 2017

Name  = 'OGSSelectOkuboWeiss'
Label = 'OGS Select Okubo-Weiss'
Help  = ''

NumberOfInputs = 1
InputDataType  = 'vtkUnstructuredGrid'
OutputDataType = 'vtkUnstructuredGrid'
ExtraXml = ''

Properties = dict(
	vorticity_dominated = True, # Vorticity dominated field with a value of -1
	strain_dominated    = True, # Strain dominated field with a value of 1
	background_field    = True  # Background field with a value of 0
	);

def RequestData():
	import vtk
	import numpy as np
	from vtk.util import numpy_support as npvtk
	from mpi4py import MPI

	comm = MPI.COMM_WORLD
	rank = comm.Get_rank()
	size = comm.Get_size()

	if rank == 0:
		# Get input data
		pdin = self.GetInput();

		# Recover the numpy array BasinMask from input
		OWmask = npvtk.vtk_to_numpy( pdin.GetCellData().GetArray("OWmask") );

		# Create CutMask array full of zeros
		CutMask = np.zeros( np.shape(OWmask) );

		# Fill with ones according to user input
		if vorticity_dominated: CutMask[OWmask == -1] = 1
		if strain_dominated:    CutMask[OWmask ==  1] = 1
		if background_field:    CutMask[OWmask ==  0] = 1

		# Add array to input
		vtkCutMask = npvtk.numpy_to_vtk(CutMask.ravel(),True,vtk.VTK_FLOAT);
		vtkCutMask.SetName("CutMask");
		pdin.GetCellData().AddArray(vtkCutMask);

		# Threshold construct
		thresh1 = vtk.vtkThreshold();
		thresh1.SetInputData(pdin);
		thresh1.ThresholdByUpper(1); # Erase whatever has 0
		thresh1.SetInputArrayToProcess(0, 0, 0, vtk.vtkDataObject.FIELD_ASSOCIATION_CELLS, "CutMask");
		thresh1.Update();

		# Grab the field and generate output
		field = thresh1.GetOutput();
		field.GetCellData().RemoveArray("CutMask"); # Remove the CutMask

		# Update the output port
		pdout = self.GetOutput().ShallowCopy(field);
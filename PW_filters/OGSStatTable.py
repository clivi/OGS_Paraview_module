# Paraview Plugin for Statistics

# Cosimo Livi, MHPC 2017

Name  = 'OGSStatTable'
Label = 'OGS Stat Table'
Help  = ''

NumberOfInputs = 1
InputDataType  = 'vtkRectilinearGrid'
OutputDataType = 'vtkTable'
ExtraXml = ''

Properties = dict(
	# Path to c libs
	path_to_library="/home/clivi/OGS/clib/src",
	# Coast Selection
	coast = True,    # Value of 1 in CoastMask
	open_sea = True,  # Value of 2 in CoastMask
	

	# Sub Basins Selection

	Alboran_Sea                      = True, # alb  => 1
	South_Western_Mediterranean_west = True, # swm1 => 2
	South_Western_Mediterranean_east = True, # swm2 => 3
	North_Western_Mediterranean      = True, # nwm  => 4
	Northern_Tyrrhenian              = True, # tyr1 => 5
	Southern_Tyrrhenian              = True, # tyr2 => 6
	Northern_Adriatic                = True, # adr1 => 7
	Southern_Adriatic                = True, # adr2 => 8
	Aegean_Sea                       = True, # aeg  => 9
	Western_Ionian                   = True, # ion1 => 10
	Eastern_Ionian                   = True, # ion2 => 11
	Northern_Ionian                  = True, # ion3 => 12
	Western_Levantine                = True, # lev1 => 13
	Northern_Levantine               = True, # lev2 => 14
	Southern_Levantine               = True, # lev3 => 15
	Eastern_Levantine                = True,  # lev4 => 16

	# Statistics Selection
	Statistics  = 1, # Perform Areal Statistics if = 1, Perform Volume Statistics if = 2

	# Statistics Options
	Average = True,
	Std = True,
	Percentili = True,
	Minima = True,
	Maxima = True,

	Climatology = True,

	#Dump To file

	Dump_To_File = False,
	path_to_save="/home/cosimo/MHPC/OGS/ArealStat.txt",
	percs = "0.05,0.25,0.5,0.75,0.95",
	depths = "200,600,6000"



	);


def RequestData():
	from OGSParaviewSuite import OGSvtk,OGSStatistics
	import numpy as np 
	import vtk
	from vtk.util import numpy_support as npvtk
	from commons.layer import Layer
	from basins import V2 as OGS
	from static import climatology
	from basins.basin import SimplePolygonalBasin, ComposedBasin
	from mpi4py import MPI

	comm = MPI.COMM_WORLD
	rank = comm.Get_rank()
	size = comm.Get_size()

	if rank == 0:
		perc = np.array(percs.split(','),dtype=np.float64)
		depth_list = np.array(depths.split(','),dtype=np.float64)
		depth_list = 1000 * depth_list
		# Get input data
		#pdin = self.GetInput();


		pdin = vtk.vtkRectilinearGrid()
		pdin.ShallowCopy(self.GetInput())
		output = vtk.vtkTable()

		path_to_mesh = pdin.GetFieldData().GetAbstractArray('path_to_mesh').GetValue(0)
		resolution = pdin.GetFieldData().GetAbstractArray('resolution').GetValue(0)

		pdin.GetFieldData().RemoveArray('resolution')
		pdin.GetFieldData().RemoveArray('path_to_mesh')
		Statistic = OGSStatistics(str(path_to_library))

		Structure = OGSvtk(str(path_to_mesh))
		Structure.SetResolution(str(resolution))

		output.GetFieldData().AddArray( Structure.createVTKstrf('Plot Type','Statistic'))

		# Init domain variables list

		coastness = ""
		subbasins = []
		aux = np.zeros(Structure.jpk-1)




		# Define Domain for statistics
		if coast and open_sea:     coastness = "everywhere"; output.GetFieldData().AddArray( Structure.createVTKstrf('Coast level:','everywhere'));

		if coast and not open_sea: coastness = "coast";output.GetFieldData().AddArray( Structure.createVTKstrf('Coast level:','coast'));

		if not coast and open_sea: coastness = "open_sea";output.GetFieldData().AddArray( Structure.createVTKstrf('Coast level:','open_sea'));
		if not coast and not open_sea: 
			raise ValueError('ERROR: cannot perform analysis on only the ground') # Just the ground

		if Alboran_Sea:						 subbasins.append('alb')
		if South_Western_Mediterranean_west: subbasins.append('swm1')
		if South_Western_Mediterranean_east: subbasins.append('swm2')
		if North_Western_Mediterranean: 	 subbasins.append('nwm')
		if Northern_Tyrrhenian: 			 subbasins.append('tyr1')
		if Southern_Tyrrhenian: 			 subbasins.append('tyr2')
		if Western_Ionian: 					 subbasins.append('ion1')
		if Eastern_Ionian: 					 subbasins.append('ion2')
		if Northern_Ionian: 				 subbasins.append('ion3')
		if Northern_Adriatic: 				 subbasins.append('adr1')
		if Southern_Adriatic: 				 subbasins.append('adr2');
		if Western_Levantine: 				 subbasins.append('lev1');
		if Northern_Levantine: 				 subbasins.append('lev2');
		if Southern_Levantine: 				 subbasins.append('lev3');
		if Eastern_Levantine: 				 subbasins.append('lev4');
		if Aegean_Sea: 						 subbasins.append('aeg');


		output.GetFieldData().AddArray( Structure.createVTKstrf('Sub-basins:',','.join(subbasins)))

		# Get Metadata from input

		depth = Structure.nav_lev[:Structure.jpk-1]
		depth_vtk = npvtk.numpy_to_vtk(depth/1000.,1)
		depth_vtk.SetName("depth")
		output.AddColumn(depth_vtk)


		# Prepare data for climatology

		if Climatology:
			climate_vars = ['N1p','N3n','O2o','N5s','O3h', 'Ac','O3c', 'DIC' ]
			LayerList = [Layer(0,10), Layer(10,30), Layer(30,60), Layer(60,100), Layer(100,150), Layer(150,300), Layer(300,600), Layer(600,1000)]
			sub_for_climat = []
			SUBList = OGS.Pred.basin_list
			for sub in SUBList:
				if any(sub.name in subbasins):
					sub_for_climat.append(sub)
			custom = ComposedBasin('custom', [sub_for_climat[k] for k in range(len(sub_for_climat))], 'custom basin')


		
		for i in range(pdin.GetCellData().GetNumberOfArrays()):
			var = pdin.GetCellData().GetArrayName(i)
			
			if var == 'basins mask' or var == 'coasts mask' or var =='depth':	continue;
			if var == 'Velocity' or var == 'Velocity (forcings)':	continue; #To Be done in the future

			if Climatology:
				if any(var in climate_vars):
					Clim, std_clim = climatology.get_climatology(var,[custom],LayerList)
					clim_output    = np.zeros(len(Structure.nav_lev)-1)
					climStd_output = np.zeros(len(Structure.nav_lev)-1)
					
					for ii in range(len(LayerList)):
						place = abs(LayerList[ii].top-LayerList[ii].bottom)/2.
						index = (abs(Structure.nav_lev[:len(Structure.nav_lev)-1]+1000*place)).argmin()
						clim_output[index]    = Clim[0][ii]
						climStd_output[index] = std_clim[0][ii]
					clim_output[clim_output==0]       = np.nan
					climStd_output[climStd_output==0] = np.nan	
					climAvg = npvtk.numpy_to_vtk(clim_output,1)
					climAvg.SetName('Climat. Avg '+var)
					output.AddColumn(climAvg)
					climStd = npvtk.numpy_to_vtk(climStd_output,1)
					climStd.SetName('Climat. Std '+var)
					output.AddColumn(climStd)


			if Statistics == 1: 
				ArealStatistics   = Statistic.FastArealStatistic(Structure,pdin,subbasins,coastness,var,perc)
				
				if Average:
					aux = np.array(ArealStatistics[:,0])
					avg = npvtk.numpy_to_vtk(aux,1)
					avg.SetName(var+" Avg")
					output.AddColumn(avg)
				if Std:
					aux1 = np.array(ArealStatistics[:,1])
					std = npvtk.numpy_to_vtk(aux1,1)
					std.SetName(var+" Std")
					output.AddColumn(std)

				if Minima:
					aux2 = np.array(ArealStatistics[:,2])
					minima = npvtk.numpy_to_vtk(aux2,1)
					minima.SetName(var+" Minima")
					output.AddColumn(minima)
				if Percentili:
					for p in range(len(perc)):
						aux3 = np.array(ArealStatistics[:,p+3])
						percentili = npvtk.numpy_to_vtk(aux3,1)
						percentili.SetName(var+" "+str(perc[p])+" wp")
						output.AddColumn(percentili)
				
				
				if Maxima:
					aux4 = np.array(ArealStatistics[:,-1])
					maxima = npvtk.numpy_to_vtk(aux4,1)
					maxima.SetName(var+" Maxima")
					output.AddColumn(maxima)
			if Statistics == 2:
				VolumeStatistics = Statistic.FastVolumeStatistic(Structure,pdin,subbasins,coastness,depth_list,var,perc = perc)
				print VolumeStatistics[:,0]
				print len(depth_list),len(VolumeStatistics[:,0])
				nav_lev = Structure.nav_lev[:Structure.jpk-1]
				
				if Average:
					aux = np.zeros(Structure.jpk-1)
					if len(VolumeStatistics) == len(depth_list):
						for i in range(len(depth_list)):
							if i == 0: aux[nav_lev>=-1*depth_list[i]] = VolumeStatistics[i,0]
							if i  > 0: aux[(nav_lev<-1*depth_list[i-1]) & (nav_lev>=-1*depth_list[i])] = VolumeStatistics[i,0]
					if len(VolumeStatistics) > len(depth_list):
						for i in range(len(depth_list)):
							if i == 0: aux[nav_lev>=-1*depth_list[i]] = VolumeStatistics[i,0]
							if i  > 0: aux[(nav_lev<-1*depth_list[i-1]) & (nav_lev>=-1*depth_list[i])] = VolumeStatistics[i,0]
						aux[nav_lev<-1*depth_list[-1]] = VolumeStatistics[-1,0]
					avg = npvtk.numpy_to_vtk(aux,1)
					avg.SetName(var+" Avg")
					output.AddColumn(avg)
				print "after average"
				if Std:
					aux = np.zeros(Structure.jpk-1)
					if len(VolumeStatistics) == len(depth_list):
						for i in range(len(depth_list)):
							if i == 0: aux[nav_lev>=-1*depth_list[i]] = VolumeStatistics[i,1]
							if i  > 0: aux[(nav_lev<-1*depth_list[i-1]) & (nav_lev>=-1*depth_list[i])] = VolumeStatistics[i,1]
					if len(VolumeStatistics) > len(depth_list):
						for i in range(len(depth_list)):
							if i == 0: aux[nav_lev>=-1*depth_list[i]] = VolumeStatistics[i,1]
							if i  > 0: aux[(nav_lev<-1*depth_list[i-1]) & (nav_lev>=-1*depth_list[i])] = VolumeStatistics[i,1]
						aux[nav_lev<-1*depth_list[-1]] = VolumeStatistics[-1,1]
					std = npvtk.numpy_to_vtk(aux,1)
					std.SetName(var+" Std")
					output.AddColumn(std)
				print "after std"
				if Minima:
					aux = np.zeros(Structure.jpk-1)
					if len(VolumeStatistics) == len(depth_list):
						for i in range(len(depth_list)):
							if i == 0: aux[nav_lev>=-1*depth_list[i]] = VolumeStatistics[i,2]
							if i  > 0: aux[(nav_lev<-1*depth_list[i-1]) & (nav_lev>=-1*depth_list[i])] = VolumeStatistics[i,2]
					if len(VolumeStatistics) > len(depth_list):
						for i in range(len(depth_list)):
							if i == 0: aux[nav_lev>=-1*depth_list[i]] = VolumeStatistics[i,2]
							if i  > 0: aux[(nav_lev<-1*depth_list[i-1]) & (nav_lev>=-1*depth_list[i])] = VolumeStatistics[i,2]
						aux[nav_lev<-1*depth_list[-1]] = VolumeStatistics[-1,2]
					minima = npvtk.numpy_to_vtk(aux,1)
					minima.SetName(var+" Minima")
					output.AddColumn(minima)
				if Percentili:
					for p in range(len(perc)):
						aux = np.zeros(Structure.jpk-1)
						if len(VolumeStatistics) == len(depth_list):
							for i in range(len(depth_list)):
								if i == 0: aux[nav_lev>=-1*depth_list[i]] = VolumeStatistics[i,p+3]
								if i  > 0: aux[(nav_lev<-1*depth_list[i-1]) & (nav_lev>=-1*depth_list[i])] = VolumeStatistics[i,p+3]
						if len(VolumeStatistics) > len(depth_list):
							for i in range(len(depth_list)):
								if i == 0: aux[nav_lev>=-1*depth_list[i]] = VolumeStatistics[i,p+3]
								if i  > 0: aux[(nav_lev<-1*depth_list[i-1]) & (nav_lev>=-1*depth_list[i])] = VolumeStatistics[i,p+3]
						aux[nav_lev<-1*depth_list[-1]] = VolumeStatistics[-1,p+3]
						percentili = npvtk.numpy_to_vtk(aux,1)
						percentili.SetName(var+" "+str(perc[p])+" wp")
						output.AddColumn(percentili)
				
				
				if Maxima:
					aux = np.zeros(Structure.jpk-1)
					if len(VolumeStatistics) == len(depth_list):
						for i in range(len(depth_list)):
							if i == 0: aux[nav_lev>=-1*depth_list[i]] = VolumeStatistics[i,-1]
							if i  > 0: aux[(nav_lev<-1*depth_list[i-1]) & (nav_lev>=-1*depth_list[i])] = VolumeStatistics[i,-1]
					if len(VolumeStatistics) > len(depth_list):
						for i in range(len(depth_list)):
							if i == 0: aux[nav_lev>=-1*depth_list[i]] = VolumeStatistics[i,-1]
							if i  > 0: aux[(nav_lev<-1*depth_list[i-1]) & (nav_lev>=-1*depth_list[i])] = VolumeStatistics[i,-1]
						aux[nav_lev<-1*depth_list[-1]] = VolumeStatistics[-1,-1]
					maxima = npvtk.numpy_to_vtk(aux,1)
					maxima.SetName(var+" Maxima")
					output.AddColumn(maxima)

			if Dump_To_File:
				np.savetxt(path_to_save,ArealStatistics)



			# if Volume_Statistics:
			# 	VolumeStatistics, CutMask = Statistic.FastVolumeStatistic(Structure,pdin,subbasins,coastness,depth_list,var,perc = perc,ReturnCutMask = True)

			
			# 	if Average:
			# 		Vdata=np.zeros((Structure.jpk-1,Structure.jpj-1,Structure.jpi-1))
				
			# 		i = 0
			# 		for z in range(len(Structure.nav_lev)-1):
			# 			if abs(Structure.nav_lev[z]) > depth_list[i]:	i = i +1
			# 			Vdata[z] = VolumeStatistics[i,0]
			# 		Vdata[CutMask == 0] = 0
			# 		pdin.GetCellData().AddArray(Structure.createVTKscaf(str(var)+" Volume Average",Vdata))
				
			# 	if Std:
			# 		Vdata=np.zeros((Structure.jpk-1,Structure.jpj-1,Structure.jpi-1))
				
			# 		i = 0
			# 		for z in range(len(Structure.nav_lev)-1):
			# 			if abs(Structure.nav_lev[z]) > depth_list[i]:	i = i +1
			# 			Vdata[z] = VolumeStatistics[i,1]
			# 		Vdata[CutMask == 0] = 0
			# 		pdin.GetCellData().AddArray(Structure.createVTKscaf(str(var)+" Volume Std",Vdata))

			# 	if Percentili:
			# 		Vdata=np.zeros((Structure.jpk-1,Structure.jpj-1,Structure.jpi-1))
					
			# 		i = 0

			# 		for p in range(len(perc)):
			# 			for z in range(len(Structure.nav_lev)-1):
			# 				if abs(Structure.nav_lev[z]) > depth_list[i]:	i = i +1
			# 				Vdata[z] = VolumeStatistics[i,p+3]
			# 			Vdata[CutMask == 0] = 0
			# 			pdin.GetCellData().AddArray(Structure.createVTKscaf(str(var)+" "+str(perc[p])+" Volume wp",Vdata))
			# 	if Minima:
			# 		Vdata=np.zeros((Structure.jpk-1,Structure.jpj-1,Structure.jpi-1))
			# 		i = 0
			# 		for z in range(len(Structure.nav_lev)-1):
			# 			if abs(Structure.nav_lev[z]) > depth_list[i]:	i = i +1
			# 			Vdata[z] = VolumeStatistics[i,2]

			# 		Vdata[CutMask == 0] = 0
			# 		pdin.GetCellData().AddArray(Structure.createVTKscaf(str(var)+" Volume Minima",Vdata))
				
			# 	if Maxima:
			# 		Vdata=np.zeros((Structure.jpk-1,Structure.jpj-1,Structure.jpi-1))
					
			# 		i = 0
			# 		for z in range(len(Structure.nav_lev)-1):
			# 			if abs(Structure.nav_lev[z]) > depth_list[i]:	i = i +1
			# 			Vdata[z] = VolumeStatistics[i,-1]
			# 		Vdata[CutMask == 0] = 0
			# 		pdin.GetCellData().AddArray(Structure.createVTKscaf(str(var)+" Volume Maxima",Vdata))




		# Update the output port

		self.GetOutput().ShallowCopy(output)
#Source for Time Dependendt statistics anaysis filter
#
#Cosimo Livi, MHPC 2017

Name  = 'OGSParallelTimeStat'
Label = 'OGS Parallel Time Statistic'
Help  = 'Produces temporal statistics at fixed depth, that can be performed on daily, weekly, monthly, seasonly and yearly periods'

NumberOfInputs = 0 # programable source
InputDataType = ''
OutputDataType = 'vtkTable'
ExtraXml = ''

Properties = dict(
	Mesh_resolution      = 3, # To work with enum 1: high 2: mid 3: low
	Path_to_mesh         = '/home/civi/OGS/MESHMASK/',
	Path_to_python       = '/home/clivi/OGS/bit.sea/',
	Path_to_ave_freq	 = '/home/clivi/OGS/INPUT/LOW',
	Start_date			 = '19000101',
	End_date			 = '22000101',
	# Biogeochemical variables
	Variables 			 = 'N3n',


	path_to_library="/home/clivi/OGS/clib/src",
	# Coast Selection
	coast = True,    # Value of 1 in CoastMask
	open_sea = True,  # Value of 2 in CoastMask
	


	# Sub Basins Selection

	Alboran_Sea                      = True, # alb  => 1
	South_Western_Mediterranean_west = True, # swm1 => 2
	South_Western_Mediterranean_east = True, # swm2 => 3
	North_Western_Mediterranean      = True, # nwm  => 4
	Northern_Tyrrhenian              = True, # tyr1 => 5
	Southern_Tyrrhenian              = True, # tyr2 => 6
	Western_Ionian                   = True, # ion1 => 7
	Eastern_Ionian                   = True, # ion2 => 8
	Northern_Ionian                  = True, # ion3 => 9
	Northern_Adriatic                = True, # adr1 => 10
	Southern_Adriatic                = True, # adr2 => 11
	Western_Levantine                = True, # lev1 => 12
	Northern_Levantine               = True, # lev2 => 13
	Southern_Levantine               = True, # lev3 => 14
	Eastern_Levantine                = True, # lev4 => 15
	Aegean_Sea                       = True,  # aeg  => 16

	# Statistics Options

	Periodicity = 1,
	depth = float(200),
	Average = True,
	Std = False,
	Percentili = False,
	Minima = False,
	Maxima = False,

	# Save to file

	Dump_To_File = False,
	path_to_save="/home/cosimo/MHPC/OGS/ArealStat.txt"

);


def RequestInformation():
	executive = self.GetExecutive()
	outInfo = executive.GetOutputInformation(0)
	dataType = 10 # VTK_FLOAT
	numberOfComponents = 1
	vtk.vtkDataObject.SetPointDataActiveScalarInfo(outInfo, dataType, numberOfComponents)


def RequestData():


	from commons.Timelist import TimeList
	import commons.genUserDateList as DL
	from commons.time_interval import TimeInterval
	from commons import season
	import sys, os, glob
	import numpy as np
	import vtk
	sys.path.append(Path_to_python);
	from vtk.util import numpy_support as npvtk
	from OGSParaviewSuite import OGSvtk,OGSStatistics
	import time
	from mpi4py import MPI


	comm = MPI.COMM_WORLD
	rank = comm.Get_rank()
	size = comm.Get_size()

	if rank == 0: start_time = time.time()
	# Get info object
	executive = self.GetExecutive()
	outInfo = executive.GetOutputInformation(0)
	# Set type of data and get input 
	dataType = 10 # VTK_FLOAT
	numberOfComponents = 1
	vtk.vtkDataObject.SetPointDataActiveScalarInfo(outInfo,dataType,numberOfComponents)
	varList = Variables.split(',')
	for var in varList:
		if var == 'Velocity' or var == 'Velocity (forcings)':
			raise ValueError('ERROR: Statistics analysis on velocity not implemented yet ')
	perc = np.array([0.05,0.25,0.5,0.75,0.95],dtype=np.float64)
	
	INPUT = Path_to_ave_freq
	interval = TimeInterval(Start_date,End_date,'%Y%m%d')

	# Initialize class
	OGS = OGSvtk(Path_to_mesh);
	# Mesh resolution
	if Mesh_resolution == 1: OGS.SetResolution('high');
	if Mesh_resolution == 2: OGS.SetResolution('mid');
	if Mesh_resolution == 3: OGS.SetResolution('low');

	coastness = ""
	subbasins = []

	# # Define Domain for statistics and eventually for naming plots (TODO)
	
	if coast and open_sea:     coastness = "everywhere";
	if coast and not open_sea: coastness = "coast";
	if not coast and open_sea: coastness = "open_sea";
	if not coast and not open_sea: 
		raise ValueError('ERROR: cannot perform analysis on only the ground') # Just the ground
	aux_sublist = []
	if Alboran_Sea:						 subbasins.append('alb');
	if South_Western_Mediterranean_west: subbasins.append('swm1');
	if South_Western_Mediterranean_east: subbasins.append('swm2');
	if North_Western_Mediterranean: 	 subbasins.append('nwm');
	if Northern_Tyrrhenian: 			 subbasins.append('tyr1');
	if Southern_Tyrrhenian: 			 subbasins.append('tyr2');
	if Western_Ionian: 					 subbasins.append('ion1');
	if Eastern_Ionian: 					 subbasins.append('ion2');
	if Northern_Ionian: 				 subbasins.append('ion3');
	if Northern_Adriatic: 				 subbasins.append('adr1');
	if Southern_Adriatic: 				 subbasins.append('adr2');
	if Western_Levantine: 				 subbasins.append('lev1');
	if Northern_Levantine: 				 subbasins.append('lev2');
	if Southern_Levantine: 				 subbasins.append('lev3');
	if Eastern_Levantine: 				 subbasins.append('lev4');
	if Aegean_Sea: 						 subbasins.append('aeg');
	



	# Evaluate proper depth index to pick later in statistics
	index = (abs(OGS.nav_lev[:len(OGS.nav_lev)-1]+1000*depth)).argmin()
	
	output = vtk.vtkTable()
	output.GetFieldData().AddArray( OGS.createVTKstrf("Plot Type","TimeStatistics"))
	output.GetFieldData().AddArray( OGS.createVTKstrf('Coast level',coastness));
	output.GetFieldData().AddArray( OGS.createVTKstrf('Sub-basins',','.join(subbasins)))

	# Main loop
	comm.Barrier()
	outData = []
	if Periodicity == 1:
		# Get dates from input folder
		TL = TimeList.fromfilenames(interval,INPUT,"*"+str(varList[0])+"*.nc")
		Dates = [os.path.basename(TL.filelist[k]).split('.')[1] for k in range(len(TL.filelist))]
		
		# Distribute files across processes
		dateList = []
		loc_size = len(Dates) / size
		rest = len(Dates) % size
		start = rank * loc_size
		end = start + loc_size
		dateList = []
		if rest != 0:
			for i in range(rest):
				if rank == i:
					end += 1
				elif rank > i:
					start += 1
					end += 1
		comm.Barrier()
		for i in range(start,end):
			dateList.append(Dates[i])

		# Define output and add dates column
		if rank == 0:


			Result = []
			vtkDateList = vtk.vtkStringArray()
			vtkDateList.SetName('Dates')
			vtkDateList.SetNumberOfTuples(len(Dates))
			for i in range(len(Dates)):
				vtkDateList.SetValue(i,Dates[i])
			output.AddColumn(vtkDateList)

		# Perform statistics for every variable at every dates
		for Variable in varList:
			
			for date in dateList:
				
				# Create a VTK rectilinear grid
				rg = OGS.CreateRectilinearGrid();
				# Initialize the sub masks
				OGS.AddMask(rg,AddSubMask=True,AddCoastMask=True,AddDepth=False);

				# Process biogeochemical variables, Postprocess variables in batch mode
				
				fname = "%s/ave.%s.%s.nc" % (Path_to_ave_freq,date,Variable)
				if os.path.exists(fname):
					# Load from NetCDF

					data = OGS.LoadNetCDF.GetVariable(fname,Variable);

					# Write data
					rg.GetCellData().AddArray( OGS.createVTKscaf(Variable,data) );
					# Postprocess composite variables
					if Variable == "Chlorophyll":
						computedVars.append("Chlorophyll")
						data = OGS.LoadNetCDF.GetVariable("%s/ave.%s.P1l.nc" % (Path_to_ave_freq,File_date),"P1l") \
							 + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P2l.nc" % (Path_to_ave_freq,File_date),"P2l") \
							 + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P3l.nc" % (Path_to_ave_freq,File_date),"P3l") \
							 + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P4l.nc" % (Path_to_ave_freq,File_date),"P4l");
						rg.GetCellData().AddArray( OGS.createVTKscaf("Chlorophyll",data) );	


				# Statistic session
				
				Statistic = OGSStatistics(path_to_library)

				ArealStatistics   = Statistic.FastArealStatistic(OGS,rg,subbasins,coastness,Variable,perc)
				outData.append(ArealStatistics[index])
			
			# Communication patern: rank 0 retrieves the results
			if rank == 0:
				for i in range(len(outData)):
					Result.append(outData[i])
			comm.Barrier()

			for i in range(1,size):
				loc_size = len(dateList)
				if rank == i:
					comm.Send([np.array(loc_size),MPI.INT], dest = 0, tag = 1)
				if rank == 0:
					aux = np.zeros(1,dtype = np.int)
					comm.Recv(aux,source = i, tag = 1)
					loc_size = aux[0]

				for j in range(loc_size):
					if rank == i:
						message = np.array(outData[j])
						comm.Send([message,MPI.FLOAT], dest = 0, tag=1)
					if rank == 0:
						data = np.zeros(9)
						comm.Recv(data,source = i, tag =1)
						Result.append(data)
				comm.Barrier()

	# Weekly averages
	WeeksList = []
	if Periodicity == 2:
		# rank 0 defines the output
		if rank == 0:
			Result = []
		loc_Res = []
		flag = 0
		for Variable in varList:
			TL = TimeList.fromfilenames(interval,INPUT,"*"+Variable+"*")
			# Check input files frequency
			if (TL.inputFrequency == "yearly" or TL.inputFrequency == "monthly"): raise ValueError("Error: cannot perform weakly average on input files with annual or monthly frequency")
			REQS = TL.getWeeklyList(3)
			# Distribute data across processes
			loc_REQS = []
			loc_size = len(REQS) / size
			rest = len(REQS) % size
			start = rank * loc_size
			end = start + loc_size
			
			if rest != 0:
				for i in range(rest):
					if rank == i:
						end += 1
					elif rank > i:
						start += 1
						end += 1
			comm.Barrier()
			for i in range(start,end):
				loc_REQS.append(REQS[i])


			if rank == 0:
				if flag == 0:
					for req in REQS:
						week = ''.join(list(str(req).split(" ")[-1])[0:4])+"/"+''.join(list(str(req).split(" ")[-1])[4:6])+"/"+''.join(list(str(req).split(" ")[-1])[6:-1])
						WeeksList.append(week)

			# Main Loop
			for req in REQS:

				
				ii,w = TL.select(req)
				files = [TL.filelist[k] for k in ii]
				if len(files) == 0: continue


				outData = []
			
				for file in files:

					# Create a VTK rectilinear grid
					rg = OGS.CreateRectilinearGrid();
					# Initialize the sub masks
					OGS.AddMask(rg,AddSubMask=True,AddCoastMask=True,AddDepth=False);

					# Process biogeochemical variables, Postprocess variables in batch mode
					
					
					if os.path.exists(file):
						# Load from NetCDF
						data = OGS.LoadNetCDF.GetVariable(file,Variable);
						# Write data
						rg.GetCellData().AddArray( OGS.createVTKscaf(Variable,data) );
						# Postprocess composite variables
						if Variable == "Chlorophyll":
							File_date = os.path.basename(file).split(".")[1]
							computedVars.append("Chlorophyll")
							data = OGS.LoadNetCDF.GetVariable("%s/ave.%s.P1l.nc" % (Path_to_ave_freq,File_date),"P1l") \
								 + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P2l.nc" % (Path_to_ave_freq,File_date),"P2l") \
								 + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P3l.nc" % (Path_to_ave_freq,File_date),"P3l") \
								 + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P4l.nc" % (Path_to_ave_freq,File_date),"P4l");
							rg.GetCellData().AddArray( OGS.createVTKscaf("Chlorophyll",data) );	


					# Statistic session
					
					Statistic = OGSStatistics(path_to_library)

					ArealStatistics   = Statistic.FastArealStatistic(OGS,rg,subbasins,coastness,Variable,perc)
					outData.append(ArealStatistics[index])
					
				outData = np.vstack(outData)
				aux = [outData[:,i].mean() for i in range(9)]
				loc_Res.append(aux)

			# Communication pattern
			if rank == 0:
				for i in range(len(loc_Res)):
					Result.append(loc_Res[i])
			comm.Barrier()

			for i in range(1,size):
					loc_size = len(loc_REQS)
					if rank == i:
						comm.Send([np.array(loc_size),MPI.INT], dest = 0, tag = 1)
					if rank == 0:
						aux = np.zeros(1,dtype = np.int)
						comm.Recv(aux,source = i, tag = 1)
						loc_size = aux[0]

					for j in range(loc_size):
						if rank == i:
							message = np.array(loc_Res[j])
							comm.Send([message,MPI.FLOAT], dest = 0, tag=1)
						if rank == 0:
							data = np.zeros(9)
							comm.Recv(data,source = i, tag =1)
							Result.append(data)
						comm.Barrier()

				

			flag = flag + 1
			if rank == 0: 
				vtkWeeksList = vtk.vtkStringArray()
				vtkWeeksList.SetName('Weeks')
				vtkWeeksList.SetNumberOfTuples(len(WeeksList))
				for i in range(len(WeeksList)):
					vtkWeeksList.SetValue(i,WeeksList[i])
				output.AddColumn(vtkWeeksList)

	# Monthly averages
	MonthsList = []
	if Periodicity == 3:
		# Get proper files
		if rank == 0:
			Result = []
		loc_Res = []
		flag = 0
		for Variable in varList:
			TL = TimeList.fromfilenames(interval,INPUT,"*"+Variable+"*")
			# Check input files frequency
			if (TL.inputFrequency == "yearly"): raise ValueError("Error: cannot perform month average on input files with annual frequency")
			REQS = TL.getMonthlist()


			loc_REQS = []
			loc_size = len(REQS) / size
			rest = len(REQS) % size
			start = rank * loc_size
			end = start + loc_size
			
			if rest != 0:
				for i in range(rest):
					if rank == i:
						end += 1
					elif rank > i:
						start += 1
						end += 1
			comm.Barrier()

			for i in range(start,end):
				loc_REQS.append(REQS[i])

			if rank == 0:
				for req in REQS:
					month = ''.join(list(str(req).split(" ")[-1])[0:4])+"/"+''.join(list(str(req).split(" ")[-1])[4:])
					if flag == 0: MonthsList.append(month)





			for req in loc_REQS:
				ii,w = TL.select(req)
				files = [TL.filelist[k] for k in ii]
				if len(files) == 0: continue

				outData = []
			
				for file in files:

					# Create a VTK rectilinear grid
					rg = OGS.CreateRectilinearGrid();
					# Initialize the sub masks
					OGS.AddMask(rg,AddSubMask=True,AddCoastMask=True,AddDepth=False);

					# Process biogeochemical variables, Postprocess variables in batch mode
					
					
					if os.path.exists(file):
						# Load from NetCDF
						data = OGS.LoadNetCDF.GetVariable(file,Variable);
						# Write data
						rg.GetCellData().AddArray( OGS.createVTKscaf(Variable,data) );
						# Postprocess composite variables
						if Variable == "Chlorophyll":
							File_date = os.path.basename(file).split(".")[1]
							computedVars.append("Chlorophyll")
							data = OGS.LoadNetCDF.GetVariable("%s/ave.%s.P1l.nc" % (Path_to_ave_freq,File_date),"P1l") \
								 + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P2l.nc" % (Path_to_ave_freq,File_date),"P2l") \
								 + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P3l.nc" % (Path_to_ave_freq,File_date),"P3l") \
								 + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P4l.nc" % (Path_to_ave_freq,File_date),"P4l");
							rg.GetCellData().AddArray( OGS.createVTKscaf("Chlorophyll",data) );	


					# Statistic session
					
					Statistic = OGSStatistics(path_to_library)

					ArealStatistics   = Statistic.FastArealStatistic(OGS,rg,subbasins,coastness,Variable,perc)
					outData.append(ArealStatistics[index])
					
				outData = np.vstack(outData)
				aux = [outData[:,i].mean() for i in range(9)]
				loc_Res.append(aux)

			# Comm pattern
			if rank == 0:
				for i in range(len(loc_Res)):
					Result.append(loc_Res[i])
			comm.Barrier()

			for i in range(1,size):
					loc_size = len(loc_REQS)
					if rank == i:
						comm.Send([np.array(loc_size),MPI.INT], dest = 0, tag = 1)
					if rank == 0:
						aux = np.zeros(1,dtype = np.int)
						comm.Recv(aux,source = i, tag = 1)
						loc_size = aux[0]
					#if rank == 0: print "i=%d, loc_size = %d" % (i,loc_size)
					#helper = loc_size
					for j in range(loc_size):

						if rank == i:
							message = np.array(loc_Res[j])
							comm.Send([message,MPI.FLOAT], dest = 0, tag=1)
						if rank == 0:
							data = np.zeros(9)
							comm.Recv(data,source = i, tag =1)
							Result.append(data)
					comm.Barrier()

			flag = flag + 1

			if rank == 0:
				vtkMonthsList = vtk.vtkStringArray()
				vtkMonthsList.SetName('Months')
				vtkMonthsList.SetNumberOfTuples(len(MonthsList))
				for i in range(len(MonthsList)):
					vtkMonthsList.SetValue(i,MonthsList[i])
				output.AddColumn(vtkMonthsList)



	# Season averages
	SeasonsList = []
	seasonobj = season.season()
	if Periodicity == 4:

		# rank 0 defines output
		if rank == 0:
			Result = []
		loc_Res = []
		flag = 0
		for Variable in varList:
			TL = TimeList.fromfilenames(interval,INPUT,"*"+Variable+"*")
			if (TL.inputFrequency == "yearly"): raise ValueError("Error: cannot perform season average on input files with annual frequency")

			REQS = TL.getSeasonList(seasonobj)
			# Distribute data
			loc_REQS = []
			loc_size = len(REQS) / size
			rest = len(REQS) % size
			start = rank * loc_size
			end = start + loc_size
			
			if rest != 0:
				for i in range(rest):
					if rank == i:
						end += 1
					elif rank > i:
						start += 1
						end += 1
			comm.Barrier()
			for i in range(start,end):
				loc_REQS.append(REQS[i])


			if rank == 0:
				for req in REQS:
					season = ''.join(list(str(req).split(" ")[3])[1:])+" "+str(req).split(" ")[4]
					SeasonsList.append(season)
			comm.Barrier()

			for req in loc_REQS:
				ii,w = TL.select(req)
				files = [TL.filelist[k] for k in ii]
				if len(files) == 0: continue

				outData = []
			
				for file in files:
					# Create a VTK rectilinear grid
					rg = OGS.CreateRectilinearGrid();
					# Initialize the sub masks
					OGS.AddMask(rg,AddSubMask=True,AddCoastMask=True,AddDepth=False);

					# Process biogeochemical variables, Postprocess variables in batch mode
					
					
					if os.path.exists(file):
						# Load from NetCDF
						data = OGS.LoadNetCDF.GetVariable(file,Variable);
						# Write data
						rg.GetCellData().AddArray( OGS.createVTKscaf(Variable,data) );
						# Postprocess composite variables
						if Variable == "Chlorophyll":
							File_date = os.path.basename(file).split(".")[1]
							computedVars.append("Chlorophyll")
							data = OGS.LoadNetCDF.GetVariable("%s/ave.%s.P1l.nc" % (Path_to_ave_freq,File_date),"P1l") \
								 + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P2l.nc" % (Path_to_ave_freq,File_date),"P2l") \
								 + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P3l.nc" % (Path_to_ave_freq,File_date),"P3l") \
								 + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P4l.nc" % (Path_to_ave_freq,File_date),"P4l");
							rg.GetCellData().AddArray( OGS.createVTKscaf("Chlorophyll",data) );	


					# Statistic session
					
					Statistic = OGSStatistics(path_to_library)

					ArealStatistics   = Statistic.FastArealStatistic(OGS,rg,subbasins,coastness,Variable,perc)
					outData.append(ArealStatistics[index])
				outData = np.vstack(outData)
				aux = [outData[:,i].mean() for i in range(9)]
				
				loc_Res.append(aux)
			comm.Barrier()

			if rank == 0:
				for i in range(len(loc_Res)):
					Result.append(loc_Res[i])
			comm.Barrier()

			for i in range(1,size):
					loc_size = len(loc_REQS)
					if rank == i:
						comm.Send([np.array(loc_size),MPI.INT], dest = 0, tag = 1)
					if rank == 0:
						aux = np.zeros(1,dtype = np.int)
						comm.Recv(aux,source = i, tag = 1)
						loc_size = aux[0]
					#if rank == 0: print "i=%d, loc_size = %d" % (i,loc_size)
					#helper = loc_size
					for j in range(loc_size):

						if rank == i:
							message = np.array(loc_Res[j])
							comm.Send([message,MPI.FLOAT], dest = 0, tag=1)
						if rank == 0:
							data = np.zeros(9)
							comm.Recv(data,source = i, tag =1)
							Result.append(data)
					comm.Barrier()

			flag = flag + 1

			if rank == 0:
				vtkSeasonsList = vtk.vtkStringArray()
				vtkSeasonsList.SetName('Seasons')
				vtkSeasonsList.SetNumberOfTuples(len(SeasonsList))
				for i in range(len(SeasonsList)):
					vtkSeasonsList.SetValue(i,SeasonsList[i])
				output.AddColumn(vtkSeasonsList)


	# Yearly averages
	yearsList = []
	if Periodicity == 5:

		# Get proper files
		if rank == 0:
			Result = []
		loc_Res = []
		flag = 0
		for Variable in varList:
			TL = TimeList.fromfilenames(interval,INPUT,"*"+Variable+"*")
			REQS = TL.getYearlist()


			loc_REQS = []
			loc_size = len(REQS) / size
			rest = len(REQS) % size
			start = rank * loc_size
			end = start + loc_size
			
			if rest != 0:
				for i in range(rest):
					if rank == i:
						end += 1
					elif rank > i:
						start += 1
						end += 1
			comm.Barrier()
			for i in range(start,end):
				loc_REQS.append(REQS[i])


			if rank == 0:
				for req in REQS:
					year = str(req).split(" ")[-1]
					ii,w = TL.select(req)
					if (flag == 0) and (len(ii)>0): yearsList.append(year)



			for req in loc_REQS:
				
				ii,w = TL.select(req)
				files = [TL.filelist[k] for k in ii]
				if len(files) == 0: continue

				outData = []
			
				for file in files:

					# Create a VTK rectilinear grid
					rg = OGS.CreateRectilinearGrid();
					# Initialize the sub masks
					OGS.AddMask(rg,AddSubMask=True,AddCoastMask=True,AddDepth=False);

					# Process biogeochemical variables, Postprocess variables in batch mode
					
					
					if os.path.exists(file):
						# Load from NetCDF
						data = OGS.LoadNetCDF.GetVariable(file,Variable);
						# Write data
						rg.GetCellData().AddArray( OGS.createVTKscaf(Variable,data) );
						# Postprocess composite variables
						if Variable == "Chlorophyll":
							File_date = os.path.basename(file).split(".")[1]
							computedVars.append("Chlorophyll")
							data = OGS.LoadNetCDF.GetVariable("%s/ave.%s.P1l.nc" % (Path_to_ave_freq,File_date),"P1l") \
								 + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P2l.nc" % (Path_to_ave_freq,File_date),"P2l") \
								 + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P3l.nc" % (Path_to_ave_freq,File_date),"P3l") \
								 + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P4l.nc" % (Path_to_ave_freq,File_date),"P4l");
							rg.GetCellData().AddArray( OGS.createVTKscaf("Chlorophyll",data) );	


					# Statistic session
					
					Statistic = OGSStatistics(path_to_library)

					ArealStatistics   = Statistic.FastArealStatistic(OGS,rg,subbasins,coastness,Variable,perc)
					outData.append(ArealStatistics[index])
					
				outData = np.vstack(outData)
				aux = [outData[:,i].mean() for i in range(9)]
				
				loc_Res.append(aux)
			comm.Barrier()
			if rank == 0:
				for i in range(len(loc_Res)):
					Result.append(loc_Res[i])
			comm.Barrier()

			for i in range(1,size):
					loc_size = len(loc_REQS)
					if rank == i:
						comm.Send([np.array(loc_size),MPI.INT], dest = 0, tag = 1)
					if rank == 0:
						aux = np.zeros(1,dtype = np.int)
						comm.Recv(aux,source = i, tag = 1)
						loc_size = aux[0]
					#if rank == 0: print "i=%d, loc_size = %d" % (i,loc_size)
					#helper = loc_size
					for j in range(loc_size):

						if rank == i:
							message = np.array(loc_Res[j])
							comm.Send([message,MPI.FLOAT], dest = 0, tag=1)
						if rank == 0:
							data = np.zeros(9)
							comm.Recv(data,source = i, tag =1)
							Result.append(data)
					comm.Barrier()

		flag = flag + 1

		if rank == 0:
			vtkYearsList = npvtk.numpy_to_vtk(np.asarray(yearsList,dtype=np.float64),1)
			vtkYearsList.SetName('Years')
			output.AddColumn(vtkYearsList)
	
	# Collect results in output vtkTable by rank 0
	if rank == 0:
		Result = np.vstack(Result)
		var = Variable
		if Average:
			aux = np.array(Result[:,0])
			avg = npvtk.numpy_to_vtk(aux,1)
			avg.SetName(var+" Avg")
			output.AddColumn(avg)
		if Std:
			aux1 = np.array(Result[:,1])
			std = npvtk.numpy_to_vtk(aux1,1)
			std.SetName(var+" std")
			output.AddColumn(std)

		if Minima:
			aux2 = np.array(Result[:,2])
			minima = npvtk.numpy_to_vtk(aux2,1)
			minima.SetName(var+" Minima")
			output.AddColumn(minima)
		if Percentili:
			for p in range(len(perc)):
				#if str(perc[p])==perc_to_visualize:
				aux3 = np.array(Result[:,p+3])
				percentili = npvtk.numpy_to_vtk(aux3,1)
				percentili.SetName(var+" "+str(perc[p])+"wp")
				output.AddColumn(percentili)
					
		if Maxima:
			aux4 = np.array(Result[:,-1])
			maxima = npvtk.numpy_to_vtk(aux4,1)
			maxima.SetName(var+" Maxima")
			output.AddColumn(maxima)






		if Dump_To_File:
			np.savetxt(path_to_save,Result)
		self.GetOutput().ShallowCopy(output)
		print time.time() - start_time




















# Domain Analysis, Paraview programmable filter

# Cosimo Livi, MHPC 2017
Name  = 'OGSDomainAnalysis'
Label = 'OGS Domain Analysis'
Help  = ''

NumberOfInputs = 1
InputDataType  = 'vtkRectilinearGrid'
OutputDataType = 'vtkRectilinearGrid'
ExtraXml = ''

Properties = dict(
	# Path to c libs
	path_to_library="/pico/scratch/userexternal/clivi001/OGS/clib/src",
	# Coast Selection
	coast = True,    # Value of 1 in CoastMask
	open_sea = True,  # Value of 2 in CoastMask
	
	# Sub Basins Selection
	All_Mediterrean					 = False, # All sea
	Alboran_Sea                      = True, # alb  => 1
	South_Western_Mediterranean_west = True, # swm1 => 2
	South_Western_Mediterranean_east = True, # swm2 => 3
	North_Western_Mediterranean      = True, # nwm  => 4
	Northern_Tyrrhenian              = True, # tyr1 => 5
	Southern_Tyrrhenian              = True, # tyr2 => 6
	Western_Ionian                   = True, # ion1 => 7
	Eastern_Ionian                   = True, # ion2 => 8
	Northern_Ionian                  = True, # ion3 => 9
	Northern_Adriatic                = True, # adr1 => 10
	Southern_Adriatic                = True, # adr2 => 11
	Western_Levantine                = True, # lev1 => 12
	Northern_Levantine               = True, # lev2 => 13
	Southern_Levantine               = True, # lev3 => 14
	Eastern_Levantine                = True, # lev4 => 15
	Aegean_Sea                       = True,  # aeg  => 16

	# Statistics Selection
	Areal_Statistics  = True, # Perform Areal Statistics
	Volume_Statistics = True,  # Perform Volume Statistics

	# Statistics Options
	Average = True,
	Std = True,
	Percentili = True,
	Minima = True,
	Maxima = True,

	percs = "0.05,0.25,0.5,0.75,0.95",
	depths = "200,600,6000"



	);


def RequestData():
	from OGSParaviewSuite import OGSvtk,OGSStatistics
	import numpy as np 
	import vtk
	from vtk.util import numpy_support as npvtk

	perc = np.array(percs.split(','),dtype=np.float64)
	depth_list = np.array(depths.split(','),dtype=np.float64)
	depth_list = 1000 * depth_list
	# Get input data
	#pdin = self.GetInput();


	pdin = vtk.vtkRectilinearGrid()
	pdin.ShallowCopy(self.GetInput())




	# Init domain variables list

	coastness = ""
	subbasins = []
	
	# Define Domain for statistics
	if coast and open_sea:     coastness = "everywhere"
	if coast and not open_sea: coastness = "coast"
	if not coast and open_sea: coastness = "open_sea"
	if not coast and not open_sea: 
		raise ValueError('ERROR: cannot perform analysis on only the ground') # Just the ground
	if All_Mediterrean:					 subbasins.append('med')
	if Alboran_Sea:						 subbasins.append('alb')
	if South_Western_Mediterranean_west: subbasins.append('swm1')
	if South_Western_Mediterranean_east: subbasins.append('swm2')
	if North_Western_Mediterranean: 	 subbasins.append('nwm')
	if Northern_Tyrrhenian: 			 subbasins.append('tyr1')
	if Southern_Tyrrhenian: 			 subbasins.append('tyr2')
	if Western_Ionian: 					 subbasins.append('ion1')
	if Eastern_Ionian: 					 subbasins.append('ion2')
	if Northern_Ionian: 				 subbasins.append('ion3')
	if Northern_Adriatic: 				 subbasins.append('adr1')
	if Southern_Adriatic: 				 subbasins.append('adr2')
	if Western_Levantine: 				 subbasins.append('lev1')
	if Northern_Levantine: 				 subbasins.append('lev2')
	if Southern_Levantine: 				 subbasins.append('lev3')
	if Eastern_Levantine: 				 subbasins.append('lev4')
	if Aegean_Sea: 						 subbasins.append('aeg')

	# Get Metadata from input
	path_to_mesh = pdin.GetFieldData().GetAbstractArray('path_to_mesh').GetValue(0)
	resolution = pdin.GetFieldData().GetAbstractArray('resolution').GetValue(0)

	pdin.GetFieldData().RemoveArray('resolution')
	pdin.GetFieldData().RemoveArray('path_to_mesh')
	Statistic = OGSStatistics(str(path_to_library))

	Structure = OGSvtk(str(path_to_mesh))
	Structure.SetResolution(str(resolution))

	for i in range(pdin.GetCellData().GetNumberOfArrays()):
		var = pdin.GetCellData().GetArrayName(i)
		







		if var == 'basins mask' or var == 'coasts mask' or var =='depth':	continue;

		if var == 'Velocity' or var == 'Velocity (forcings)':	continue; #To Be done in the future
		
		if Areal_Statistics: 
			ArealStatistics, CutMask   = Statistic.FastArealStatistic(Structure,pdin,subbasins,coastness,var,perc,ReturnCutMask = True)
			if Average:
					Adata=np.zeros((Structure.jpk-1,Structure.jpj-1,Structure.jpi-1))
					i = 0
					for z in range(len(Structure.nav_lev)-1):
						Adata[z] = ArealStatistics[z,0]
					Adata[CutMask == 0] = 0
					pdin.GetCellData().AddArray(Structure.createVTKscaf(str(var)+" Areal  Average",Adata))
			if Std:
					Adata=np.zeros((Structure.jpk-1,Structure.jpj-1,Structure.jpi-1))
					i = 0
					for z in range(len(Structure.nav_lev)-1):
						Adata[z] = ArealStatistics[z,1]
					Adata[CutMask == 0] = 0
					pdin.GetCellData().AddArray(Structure.createVTKscaf(str(var)+" Areal Std",Adata))
			if Percentili:
				Adata=np.zeros((Structure.jpk-1,Structure.jpj-1,Structure.jpi-1))
				
				i = 0

				for p in range(len(perc)):
					for z in range(len(Structure.nav_lev)-1):
						Adata[z] = ArealStatistics[z,p+3]
					Adata[CutMask == 0] = 0
					pdin.GetCellData().AddArray(Structure.createVTKscaf(str(var)+" "+str(perc[p])+" Areal wp" ,Adata))
			if Minima:
				Adata=np.zeros((Structure.jpk-1,Structure.jpj-1,Structure.jpi-1))
				i = 0
				for z in range(len(Structure.nav_lev)-1):
					Adata[z] = ArealStatistics[z,2]
				Adata[CutMask == 0] = 0
				pdin.GetCellData().AddArray(Structure.createVTKscaf(str(var)+" Areal Minima" ,Adata))
			
			if Maxima:
				Adata=np.zeros((Structure.jpk-1,Structure.jpj-1,Structure.jpi-1))
				Vdata=np.zeros((Structure.jpk-1,Structure.jpj-1,Structure.jpi-1))
				
				i = 0
				for z in range(len(Structure.nav_lev)-1):
					Adata[z] = ArealStatistics[z,-1]
				Adata[CutMask == 0] = 0
				pdin.GetCellData().AddArray(Structure.createVTKscaf(str(var)+" Areal Maxima" ,Adata))



		if Volume_Statistics:
			VolumeStatistics, CutMask = Statistic.FastVolumeStatistic(Structure,pdin,subbasins,coastness,depth_list,var,perc = perc,ReturnCutMask = True)

		
			if Average:
				Vdata=np.zeros((Structure.jpk-1,Structure.jpj-1,Structure.jpi-1))
			
				i = 0
				for z in range(len(Structure.nav_lev)-1):
					if abs(Structure.nav_lev[z]) > depth_list[i]:	i = i +1
					Vdata[z] = VolumeStatistics[i,0]
				Vdata[CutMask == 0] = 0
				pdin.GetCellData().AddArray(Structure.createVTKscaf(str(var)+" Volume Average",Vdata))
			
			if Std:
				Vdata=np.zeros((Structure.jpk-1,Structure.jpj-1,Structure.jpi-1))
			
				i = 0
				for z in range(len(Structure.nav_lev)-1):
					if abs(Structure.nav_lev[z]) > depth_list[i]:	i = i +1
					Vdata[z] = VolumeStatistics[i,1]
				Vdata[CutMask == 0] = 0
				pdin.GetCellData().AddArray(Structure.createVTKscaf(str(var)+" Volume Std",Vdata))

			if Percentili:
				Vdata=np.zeros((Structure.jpk-1,Structure.jpj-1,Structure.jpi-1))
				
				i = 0

				for p in range(len(perc)):
					for z in range(len(Structure.nav_lev)-1):
						if abs(Structure.nav_lev[z]) > depth_list[i]:	i = i +1
						Vdata[z] = VolumeStatistics[i,p+3]
					Vdata[CutMask == 0] = 0
					pdin.GetCellData().AddArray(Structure.createVTKscaf(str(var)+" "+str(perc[p])+" Volume wp",Vdata))
			if Minima:
				Vdata=np.zeros((Structure.jpk-1,Structure.jpj-1,Structure.jpi-1))
				i = 0
				for z in range(len(Structure.nav_lev)-1):
					if abs(Structure.nav_lev[z]) > depth_list[i]:	i = i +1
					Vdata[z] = VolumeStatistics[i,2]

				Vdata[CutMask == 0] = 0
				pdin.GetCellData().AddArray(Structure.createVTKscaf(str(var)+" Volume Minima",Vdata))
			
			if Maxima:
				Vdata=np.zeros((Structure.jpk-1,Structure.jpj-1,Structure.jpi-1))
				
				i = 0
				for z in range(len(Structure.nav_lev)-1):
					if abs(Structure.nav_lev[z]) > depth_list[i]:	i = i +1
					Vdata[z] = VolumeStatistics[i,-1]
				Vdata[CutMask == 0] = 0
				pdin.GetCellData().AddArray(Structure.createVTKscaf(str(var)+" Volume Maxima",Vdata))




	# Update the output port

	self.GetOutput().ShallowCopy(pdin)
	

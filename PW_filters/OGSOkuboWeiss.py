# Okubo-Weiss Criterion, ParaView programable filter script.
#
# Computation of the Okubo-Weiss parameter in 2D geophysical turbulence
# according to Okubo 1970, Weiss 1991 and Isern-Fontanet 2004.
#
#	W = Sn2 + Ss2 - w2
#
# Where Sn2 is the normal strain squared, Ss2 is the shear strain squared
# and w2 is the vorticity squared.
#
# To be used with OGSTM-BFM data.
#
# Generates the following variables:
#	OW: array containing the Okubo-Weiss parameter expanded in the 
#		third dimension.
#	OWmask: mask to differentiate from the 3 different flows:
#		-1 for vorticity-dominated (W < -W0)
#		 1 for strain-dominated (W > W0)
#		 0 for background field (abs(W) <= W0)
#
# Parameters to modify
#	var: Name of the velocity vector as ParaView variable
#	coef: Okubo-Weiss filter coefficient as W0 = coef*std_dev(W)
#
# Arnau Miro, SoHPC 2017

Name  = 'OGSOkuboWeiss'
Label = 'OGS Okubo-Weiss Criterion'
Help  = 'Computation of the Okubo-Weiss parameter in 2D geophysical turbulence \
         according to Okubo 1970, Weiss 1991 and Isern-Fontanet 2004.'

NumberOfInputs = 1
InputDataType  = 'vtkRectilinearGrid'
OutputDataType = 'vtkRectilinearGrid'
ExtraXml = ''

Properties = dict(
	var  = "Velocity", # ParaView variable
	coef = 0.2      # Okubo-Weiss coefficient
	);

def RequestData():
	# Import
	import vtk
	import numpy as np
	from vtk.util import numpy_support as npvtk
	from mpi4py import MPI

	comm = MPI.COMM_WORLD
	rank = comm.Get_rank()
	size = comm.Get_size()
	# Grab input and output from ParaView
	if rank == 0:
		pdin  = self.GetInput();  # Get input data
		pdout = self.GetOutput(); # Get output data

		# Grab x,y,z from the grid
		x = npvtk.vtk_to_numpy(pdin.GetXCoordinates());
		y = npvtk.vtk_to_numpy(pdin.GetYCoordinates());
		z = npvtk.vtk_to_numpy(pdin.GetZCoordinates());

		# Get dimensions, jpk = 124; jpj = 379; jpi = 1084
		jpi = np.shape(x)[0] - 1;
		jpj = np.shape(y)[0] - 1;
		jpk = np.shape(z)[0] - 1;

		# Convert VTK to U,V,W
		vtkarray = pdin.GetCellData().GetArray(var); # this is vtkFloatArray
		vecf = npvtk.vtk_to_numpy(vtkarray); # this is a numpy array
		U = vecf[:,0].reshape((jpk,jpj,jpi));
		V = vecf[:,1].reshape((jpk,jpj,jpi));
		W = vecf[:,2].reshape((jpk,jpj,jpi));

		# Compute the gradients
		du = np.array( np.gradient(U[0,:,:],y[:jpj],x[:jpi]) ); # We just need the surface velocity
		dv = np.array( np.gradient(V[0,:,:],y[:jpj],x[:jpi]) ); # We just need the surface velocity

		# Compute strains and vorticity
		Sn2 = np.power(du[1,:,:] - dv[0,:,:], 2); # normal strain
		Ss2 = np.power(dv[1,:,:] + du[0,:,:], 2); # shear strain
		w2  = np.power(dv[1,:,:] - du[0,:,:], 2); # vorticity
		# Compute Okubo-Weiss parameter in 2D
		W = Sn2 + Ss2 - w2; # Okubo-Weiss parameter in 2D
		OW = np.tile(W,(jpk,1,1)); # Fill in 3D for ParaView exportation
		# Now create the OWmask to separate the flow in 3 regions (Isern-Fontanet 2004)
		# 1. vorticity-dominated (W < -W0)
		# 2. strain-dominated (W > W0)
		# 3. background field (abs(W) <= W0)
		W0 = coef*np.std(W);
		#print "W: mean = %.4e, s_W = %.4e, W0 = %.4e" % ( np.mean(W),np.std(W),W0 )
		# Create the 2D mask
		OWmask2d = np.zeros([jpj,jpi]);
		# Populate the mask
		OWmask2d[ W < -W0 ] = -1; # vorticity-dominated
		OWmask2d[ W > W0 ]  =  1; # strain-dominated
		#OWmask2d[ np.abs(W) <= W0 ] = 0; # background field
		# Fill in 3D for ParaView exportation
		OWmask = np.tile(OWmask2d,(jpk,1,1));

		# Convert the two variables into VTK arrays
		OWvtk  = npvtk.numpy_to_vtk(OW.ravel(),True,vtk.VTK_FLOAT);
		OWmvtk = npvtk.numpy_to_vtk(OWmask.ravel(),True,vtk.VTK_FLOAT);
		# Set variable name
		OWvtk.SetName("OW");
		OWmvtk.SetName("OWmask");

		# Generate output, add the arrays
		pdout.ShallowCopy(pdin);
		pdout.GetCellData().AddArray(OWvtk);
		pdout.GetCellData().AddArray(OWmvtk);
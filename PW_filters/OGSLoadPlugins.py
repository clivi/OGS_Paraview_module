#!/usr/bin/env pvpython
#
# ParaView macro to load all OGS plugins
#
# Arnau Miro, SoHPC 2017

plug_dir  = '/home/cosimo/MHPC/OGS/PW_filters';
plug_list = ['OGSImportNetCDF.xml',\
	     'OGSImportNetCDFTime.xml',\
             'OGSSelectCoast.xml',\
             'OGSSelectBasin.xml',\
             'OGSOkuboWeiss.xml',\
	     'OGSDomainAnalysis.xml',\
             'OGSStatTable.xml',\
	     'OGSHovmoller.xml',\
	     'OGSTimeStat.xml',\
             'OGSCustomStatTable.xml',\
             'OGSSelectOkuboWeiss.xml'\
            ];

# Import the simple module from ParaView
from paraview.simple import *

# Load plugins
for plugin in plug_list:
	LoadPlugin('%s/%s' % (plug_dir,plugin),remote=False,ns=globals());

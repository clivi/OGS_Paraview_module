#Source for Time Dependendt statistics anaysis filter
#
#Cosimo Livi, MHPC 2017

Name  = 'OGSTimeStat'
Label = 'OGS Time Statistic '
Help  = 'Produces temporal statistics at fixed depth, that can be performed on daily, weekly, monthly, seasonly and yearly periods'

NumberOfInputs = 0 # programable source
InputDataType = ''
OutputDataType = 'vtkTable'
ExtraXml = ''

Properties = dict(
	Mesh_resolution      = 3, # To work with enum 1: high 2: mid 3: low
	Path_to_mesh         = '/home/cosimo/MHPC/OGS/MESHMASK/',
	Path_to_python       = '/home/cosimo/MHPC/OGS/bit.sea/',
	Path_to_ave_freq	 = '/home/cosimo/MHPC/OGS/INPUT/low_res',
	Start_date			 = '19000101',
	End_date			 = '22000101',
	# Biogeochemical variables
	Variables 			 = 'N3n',


	path_to_library="/home/cosimo/MHPC/OGS/clib/src",
	# Coast Selection
	coast = True,    # Value of 1 in CoastMask
	open_sea = True,  # Value of 2 in CoastMask
	


	# Sub Basins Selection

	Alboran_Sea                      = True, # alb  => 1
	South_Western_Mediterranean_west = True, # swm1 => 2
	South_Western_Mediterranean_east = True, # swm2 => 3
	North_Western_Mediterranean      = True, # nwm  => 4
	Northern_Tyrrhenian              = True, # tyr1 => 5
	Southern_Tyrrhenian              = True, # tyr2 => 6
	Western_Ionian                   = True, # ion1 => 7
	Eastern_Ionian                   = True, # ion2 => 8
	Northern_Ionian                  = True, # ion3 => 9
	Northern_Adriatic                = True, # adr1 => 10
	Southern_Adriatic                = True, # adr2 => 11
	Western_Levantine                = True, # lev1 => 12
	Northern_Levantine               = True, # lev2 => 13
	Southern_Levantine               = True, # lev3 => 14
	Eastern_Levantine                = True, # lev4 => 15
	Aegean_Sea                       = True,  # aeg  => 16

	# Statistics Options

	Periodicity = 1,
	depth = float(200),
	Average = True,
	Std = False,
	Percentili = False,
	Minima = False,
	Maxima = False,

	# Save to file

	Dump_To_File = False,
	path_to_save="/home/cosimo/MHPC/OGS/ArealStat.txt"

);


def RequestInformation():
	executive = self.GetExecutive()
	outInfo = executive.GetOutputInformation(0)
	dataType = 10 # VTK_FLOAT
	numberOfComponents = 1
	vtk.vtkDataObject.SetPointDataActiveScalarInfo(outInfo, dataType, numberOfComponents)


def RequestData():


	from commons.Timelist import TimeList
	import commons.genUserDateList as DL
	from commons.time_interval import TimeInterval
	from commons import season
	import sys, os, glob
	import numpy as np
	import vtk
	sys.path.append(Path_to_python);
	from vtk.util import numpy_support as npvtk
	from OGSParaviewSuite import OGSvtk,OGSStatistics

	# Get info object
	executive = self.GetExecutive()
	outInfo = executive.GetOutputInformation(0)
	# Set type of data and get input 
	dataType = 10 # VTK_FLOAT
	numberOfComponents = 1
	vtk.vtkDataObject.SetPointDataActiveScalarInfo(outInfo,dataType,numberOfComponents)
	varList = Variables.split(',')
	for var in varList:
		if var == 'Velocity' or var == 'Velocity (forcings)':
			raise ValueError('ERROR: Statistics analysis on velocity not implemented yet ')



	perc = np.array([0.05,0.25,0.5,0.75,0.95],dtype=np.float64)
	
	INPUT = Path_to_ave_freq
	interval = TimeInterval(Start_date,End_date,'%Y%m%d')
	TL = TimeList.fromfilenames(interval,INPUT,"*"+str(varList[0])+"*.nc")
	dateList = [os.path.basename(TL.filelist[k]).split('.')[1] for k in range(len(TL.filelist))]
	# Initialize class
	OGS = OGSvtk(Path_to_mesh);
	# Mesh resolution
	if Mesh_resolution == 1: OGS.SetResolution('high');
	if Mesh_resolution == 2: OGS.SetResolution('mid');
	if Mesh_resolution == 3: OGS.SetResolution('low');

	# Create auxiliary columns for subbasins and coastness identification	
	coastness = ""
	subbasins = []
	aux = np.zeros(len(dateList))
	TableSub = npvtk.numpy_to_vtk(aux,1)
	TableCoast = npvtk.numpy_to_vtk(aux,1)

	TableSub.SetName("Subbasins")
	TableCoast.SetName("Coastness")


	# # Define Domain for statistics and eventually for naming plots (TODO)
	
	if coast and open_sea:     coastness = "everywhere";TableCoast.SetValue(0,1);
	if coast and not open_sea: coastness = "coast";TableCoast.SetValue(0,2);
	if not coast and open_sea: coastness = "open_sea";TableCoast.SetValue(0,3);
	if not coast and not open_sea: 
		raise ValueError('ERROR: cannot perform analysis on only the ground') # Just the ground
	aux_sublist = []
	if Alboran_Sea:						 subbasins.append('alb');TableSub.SetValue(0,1);aux_sublist.append(1)
	if South_Western_Mediterranean_west: subbasins.append('swm1');TableSub.SetValue(0,2);aux_sublist.append(1)
	if South_Western_Mediterranean_east: subbasins.append('swm2');TableSub.SetValue(0,3);aux_sublist.append(1)
	if North_Western_Mediterranean: 	 subbasins.append('nwm');TableSub.SetValue(0,4);aux_sublist.append(1)
	if Northern_Tyrrhenian: 			 subbasins.append('tyr1');TableSub.SetValue(0,5);aux_sublist.append(1)
	if Southern_Tyrrhenian: 			 subbasins.append('tyr2');TableSub.SetValue(0,6);aux_sublist.append(1)
	if Western_Ionian: 					 subbasins.append('ion1');TableSub.SetValue(0,7);aux_sublist.append(1)
	if Eastern_Ionian: 					 subbasins.append('ion2');TableSub.SetValue(0,8);aux_sublist.append(1)
	if Northern_Ionian: 				 subbasins.append('ion3');TableSub.SetValue(0,9);aux_sublist.append(1)
	if Northern_Adriatic: 				 subbasins.append('adr1');TableSub.SetValue(0,10);aux_sublist.append(1)
	if Southern_Adriatic: 				 subbasins.append('adr2');TableSub.SetValue(0,11);aux_sublist.append(1)
	if Western_Levantine: 				 subbasins.append('lev1');TableSub.SetValue(0,12);aux_sublist.append(1)
	if Northern_Levantine: 				 subbasins.append('lev2');TableSub.SetValue(0,13);aux_sublist.append(1)
	if Southern_Levantine: 				 subbasins.append('lev3');TableSub.SetValue(0,14);aux_sublist.append(1)
	if Eastern_Levantine: 				 subbasins.append('lev4');TableSub.SetValue(0,15);aux_sublist.append(1)
	if Aegean_Sea: 						 subbasins.append('aeg');TableSub.SetValue(0,16);aux_sublist.append(1)
	if len(aux_sublist)>1:				 TableSub.SetValue(0,100)

	# output.AddColumn(TableCoast)
	# output.AddColumn(TableSub)

	# Evaluate proper depth index to pick later in statistics
	index = (abs(OGS.nav_lev[:len(OGS.nav_lev)-1]+1000*depth)).argmin()
	

	
	# Main loop

	outData = []
	if Periodicity == 1:
		# Define output vtkTable

		output = vtk.vtkTable()
		vtkDateList = vtk.vtkStringArray()
		vtkDateList.SetName('Dates')
		Result = []


		# Define column with dates
		vtkDateList.SetNumberOfTuples(len(dateList))
		for i in range(len(dateList)):
			vtkDateList.SetValue(i,dateList[i])
		output.AddColumn(vtkDateList)

		# Perform statistics for every variable at every dates
		for Variable in varList:
			
			for date in dateList:
				
				# Create a VTK rectilinear grid
				rg = OGS.CreateRectilinearGrid();
				# Initialize the sub masks
				OGS.AddMask(rg,AddSubMask=True,AddCoastMask=True,AddDepth=False);

				# Process biogeochemical variables, Postprocess variables in batch mode
				
				fname = "%s/ave.%s.%s.nc" % (Path_to_ave_freq,date,Variable)
				if os.path.exists(fname):
					# Load from NetCDF

					data = OGS.LoadNetCDF.GetVariable(fname,Variable);

					# Write data
					rg.GetCellData().AddArray( OGS.createVTKscaf(Variable,data) );
					# Postprocess composite variables
					if Variable == "Chlorophyll":
						computedVars.append("Chlorophyll")
						data = OGS.LoadNetCDF.GetVariable("%s/ave.%s.P1l.nc" % (Path_to_ave_freq,File_date),"P1l") \
							 + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P2l.nc" % (Path_to_ave_freq,File_date),"P2l") \
							 + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P3l.nc" % (Path_to_ave_freq,File_date),"P3l") \
							 + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P4l.nc" % (Path_to_ave_freq,File_date),"P4l");
						rg.GetCellData().AddArray( OGS.createVTKscaf("Chlorophyll",data) );	


				# Statistic session
				
				Statistic = OGSStatistics(path_to_library)

				ArealStatistics   = Statistic.FastArealStatistic(OGS,rg,subbasins,coastness,Variable,perc)
				outData.append(ArealStatistics[index])
			
			Result = outData

	# Weekly averages
	WeeksList = []
	if Periodicity == 2:
		# Get proper files
		output = vtk.vtkTable()
		Result = []
		flag = 0
		for Variable in varList:
			TL = TimeList.fromfilenames(interval,INPUT,"*"+Variable+"*")
			# Check input files frequency
			if (TL.inputFrequency == "yearly" or TL.inputFrequency == "monthly"): raise ValueError("Error: cannot perform weakly average on input files with annual or monthly frequency")
			REQS = TL.getWeeklyList(3)
			for req in REQS:
				week = ''.join(list(str(req).split(" ")[-1])[0:4])+"/"+''.join(list(str(req).split(" ")[-1])[4:6])+"/"+''.join(list(str(req).split(" ")[-1])[6:-1])

				
				ii,w = TL.select(req)
				files = [TL.filelist[k] for k in ii]
				if len(files) == 0: continue
				if flag == 0: WeeksList.append(week)

				outData = []
			
				for file in files:

					# Create a VTK rectilinear grid
					rg = OGS.CreateRectilinearGrid();
					# Initialize the sub masks
					OGS.AddMask(rg,AddSubMask=True,AddCoastMask=True,AddDepth=False);

					# Process biogeochemical variables, Postprocess variables in batch mode
					
					
					if os.path.exists(file):
						# Load from NetCDF
						data = OGS.LoadNetCDF.GetVariable(file,Variable);
						# Write data
						rg.GetCellData().AddArray( OGS.createVTKscaf(Variable,data) );
						# Postprocess composite variables
						if Variable == "Chlorophyll":
							File_date = os.path.basename(file).split(".")[1]
							computedVars.append("Chlorophyll")
							data = OGS.LoadNetCDF.GetVariable("%s/ave.%s.P1l.nc" % (Path_to_ave_freq,File_date),"P1l") \
								 + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P2l.nc" % (Path_to_ave_freq,File_date),"P2l") \
								 + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P3l.nc" % (Path_to_ave_freq,File_date),"P3l") \
								 + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P4l.nc" % (Path_to_ave_freq,File_date),"P4l");
							rg.GetCellData().AddArray( OGS.createVTKscaf("Chlorophyll",data) );	


					# Statistic session
					
					Statistic = OGSStatistics(path_to_library)

					ArealStatistics   = Statistic.FastArealStatistic(OGS,rg,subbasins,coastness,Variable,perc)
					outData.append(ArealStatistics[index])
					
				outData = np.vstack(outData)
				aux = [outData[:,i].mean() for i in range(9)]
				
				Result.append(aux)
			flag = flag + 1
			Result = np.vstack(Result)
		vtkWeeksList = vtk.vtkStringArray()
		vtkWeeksList.SetName('Weeks')
		vtkWeeksList.SetNumberOfTuples(len(WeeksList))
		for i in range(len(WeeksList)):
			vtkWeeksList.SetValue(i,WeeksList[i])
		output.AddColumn(vtkWeeksList)

	# Monthly averages
	MonthsList = []
	if Periodicity == 3:
		# Get proper files
		output = vtk.vtkTable()
		Result = []
		flag = 0
		for Variable in varList:
			TL = TimeList.fromfilenames(interval,INPUT,"*"+Variable+"*")
			# Check input files frequency
			if (TL.inputFrequency == "yearly"): raise ValueError("Error: cannot perform month average on input files with annual frequency")
			REQS = TL.getMonthlist()
			for req in REQS:
				month = ''.join(list(str(req).split(" ")[-1])[0:4])+"/"+''.join(list(str(req).split(" ")[-1])[4:])
				ii,w = TL.select(req)
				files = [TL.filelist[k] for k in ii]
				if len(files) == 0: continue
				if flag == 0: MonthsList.append(month)

				outData = []
			
				for file in files:

					# Create a VTK rectilinear grid
					rg = OGS.CreateRectilinearGrid();
					# Initialize the sub masks
					OGS.AddMask(rg,AddSubMask=True,AddCoastMask=True,AddDepth=False);

					# Process biogeochemical variables, Postprocess variables in batch mode
					
					
					if os.path.exists(file):
						# Load from NetCDF
						data = OGS.LoadNetCDF.GetVariable(file,Variable);
						# Write data
						rg.GetCellData().AddArray( OGS.createVTKscaf(Variable,data) );
						# Postprocess composite variables
						if Variable == "Chlorophyll":
							File_date = os.path.basename(file).split(".")[1]
							computedVars.append("Chlorophyll")
							data = OGS.LoadNetCDF.GetVariable("%s/ave.%s.P1l.nc" % (Path_to_ave_freq,File_date),"P1l") \
								 + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P2l.nc" % (Path_to_ave_freq,File_date),"P2l") \
								 + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P3l.nc" % (Path_to_ave_freq,File_date),"P3l") \
								 + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P4l.nc" % (Path_to_ave_freq,File_date),"P4l");
							rg.GetCellData().AddArray( OGS.createVTKscaf("Chlorophyll",data) );	


					# Statistic session
					
					Statistic = OGSStatistics(path_to_library)

					ArealStatistics   = Statistic.FastArealStatistic(OGS,rg,subbasins,coastness,Variable,perc)
					outData.append(ArealStatistics[index])
					
				outData = np.vstack(outData)
				aux = [outData[:,i].mean() for i in range(9)]
				
				Result.append(aux)
			flag = flag + 1
			Result = np.vstack(Result)
		vtkMonthsList = vtk.vtkStringArray()
		vtkMonthsList.SetName('Months')
		vtkMonthsList.SetNumberOfTuples(len(MonthsList))
		for i in range(len(MonthsList)):
			vtkMonthsList.SetValue(i,MonthsList[i])
		output.AddColumn(vtkMonthsList)


	# Season averages
	SeasonsList = []
	seasonobj = season.season()
	if Periodicity == 4:

		# Get proper files
		output = vtk.vtkTable()
		Result = []
		flag = 0
		for Variable in varList:
			TL = TimeList.fromfilenames(interval,INPUT,"*"+Variable+"*")
			if (TL.inputFrequency == "yearly"): raise ValueError("Error: cannot perform season average on input files with annual frequency")

			REQS = TL.getSeasonList(seasonobj)
			for req in REQS:
				season = ''.join(list(str(req).split(" ")[3])[1:])+" "+str(req).split(" ")[4]
				ii,w = TL.select(req)
				files = [TL.filelist[k] for k in ii]
				if len(files) == 0: continue
				if flag == 0: SeasonsList.append(season)

				outData = []
			
				for file in files:
					# Create a VTK rectilinear grid
					rg = OGS.CreateRectilinearGrid();
					# Initialize the sub masks
					OGS.AddMask(rg,AddSubMask=True,AddCoastMask=True,AddDepth=False);

					# Process biogeochemical variables, Postprocess variables in batch mode
					
					
					if os.path.exists(file):
						# Load from NetCDF
						data = OGS.LoadNetCDF.GetVariable(file,Variable);
						# Write data
						rg.GetCellData().AddArray( OGS.createVTKscaf(Variable,data) );
						# Postprocess composite variables
						if Variable == "Chlorophyll":
							File_date = os.path.basename(file).split(".")[1]
							computedVars.append("Chlorophyll")
							data = OGS.LoadNetCDF.GetVariable("%s/ave.%s.P1l.nc" % (Path_to_ave_freq,File_date),"P1l") \
								 + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P2l.nc" % (Path_to_ave_freq,File_date),"P2l") \
								 + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P3l.nc" % (Path_to_ave_freq,File_date),"P3l") \
								 + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P4l.nc" % (Path_to_ave_freq,File_date),"P4l");
							rg.GetCellData().AddArray( OGS.createVTKscaf("Chlorophyll",data) );	


					# Statistic session
					
					Statistic = OGSStatistics(path_to_library)

					ArealStatistics   = Statistic.FastArealStatistic(OGS,rg,subbasins,coastness,Variable,perc)
					outData.append(ArealStatistics[index])
				outData = np.vstack(outData)
				aux = [outData[:,i].mean() for i in range(9)]
				
				Result.append(aux)
			flag = flag + 1
		vtkSeasonsList = vtk.vtkStringArray()
		vtkSeasonsList.SetName('Seasons')
		vtkSeasonsList.SetNumberOfTuples(len(SeasonsList))
		for i in range(len(SeasonsList)):
			vtkSeasonsList.SetValue(i,SeasonsList[i])
		output.AddColumn(vtkSeasonsList)


	# Yearly averages
	yearsList = []
	if Periodicity == 5:

		# Get proper files
		output = vtk.vtkTable()
		Result = []
		flag = 0
		for Variable in varList:
			TL = TimeList.fromfilenames(interval,INPUT,"*"+Variable+"*")
			REQS = TL.getYearlist()
			for req in REQS:
				year = str(req).split(" ")[-1]
				
				ii,w = TL.select(req)
				files = [TL.filelist[k] for k in ii]
				if len(files) == 0: continue
				if flag == 0: yearsList.append(year)

				outData = []
			
				for file in files:

					# Create a VTK rectilinear grid
					rg = OGS.CreateRectilinearGrid();
					# Initialize the sub masks
					OGS.AddMask(rg,AddSubMask=True,AddCoastMask=True,AddDepth=False);

					# Process biogeochemical variables, Postprocess variables in batch mode
					
					
					if os.path.exists(file):
						# Load from NetCDF
						data = OGS.LoadNetCDF.GetVariable(file,Variable);
						# Write data
						rg.GetCellData().AddArray( OGS.createVTKscaf(Variable,data) );
						# Postprocess composite variables
						if Variable == "Chlorophyll":
							File_date = os.path.basename(file).split(".")[1]
							computedVars.append("Chlorophyll")
							data = OGS.LoadNetCDF.GetVariable("%s/ave.%s.P1l.nc" % (Path_to_ave_freq,File_date),"P1l") \
								 + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P2l.nc" % (Path_to_ave_freq,File_date),"P2l") \
								 + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P3l.nc" % (Path_to_ave_freq,File_date),"P3l") \
								 + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P4l.nc" % (Path_to_ave_freq,File_date),"P4l");
							rg.GetCellData().AddArray( OGS.createVTKscaf("Chlorophyll",data) );	


					# Statistic session
					
					Statistic = OGSStatistics(path_to_library)

					ArealStatistics   = Statistic.FastArealStatistic(OGS,rg,subbasins,coastness,Variable,perc)
					outData.append(ArealStatistics[index])
					
				outData = np.vstack(outData)
				aux = [outData[:,i].mean() for i in range(9)]
				
				Result.append(aux)
			flag = flag + 1
	
		vtkYearsList = npvtk.numpy_to_vtk(np.asarray(yearsList,dtype=np.float64),1)
		vtkYearsList.SetName('Years')
		output.AddColumn(vtkYearsList)
	
	# Collect results in output vtkTable
	Result = np.vstack(Result)
	var = Variable
	if Average:
		aux = np.array(Result[:,0])
		avg = npvtk.numpy_to_vtk(aux,1)
		avg.SetName(var+" Avg")
		output.AddColumn(avg)
	if Std:
		aux1 = np.array(Result[:,1])
		std = npvtk.numpy_to_vtk(aux1,1)
		std.SetName(var+" std")
		output.AddColumn(std)

	if Minima:
		aux2 = np.array(Result[:,2])
		minima = npvtk.numpy_to_vtk(aux2,1)
		minima.SetName(var+" Minima")
		output.AddColumn(minima)
	if Percentili:
		for p in range(len(perc)):
			#if str(perc[p])==perc_to_visualize:
			aux3 = np.array(Result[:,p+3])
			percentili = npvtk.numpy_to_vtk(aux3,1)
			percentili.SetName(var+" "+str(perc[p])+"wp")
			output.AddColumn(percentili)
				
	if Maxima:
		aux4 = np.array(Result[:,-1])
		maxima = npvtk.numpy_to_vtk(aux4,1)
		maxima.SetName(var+" Maxima")
		output.AddColumn(maxima)






	if Dump_To_File:
		np.savetxt(path_to_save,Result)
	self.GetOutput().ShallowCopy(output)




















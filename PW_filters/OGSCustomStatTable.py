# Paraview Plugin for Statistics

# Cosimo Livi, MHPC 2017

Name  = 'OGSCustomStatTable'
Label = 'OGS Custom Stat Table'
Help  = ''

NumberOfInputs = 1
InputDataType  = 'vtkUnstructuredGrid'
OutputDataType = 'vtkTable'
ExtraXml = ''

Properties = dict(
	# Path to c libs
	path_to_library="/home/cosimo/MHPC/OGS/clib/src",
	
	# Statistics Selection
	Statistics  = 1, # Perform Areal Statistics if = 1, Perform Volume Statistics if = 2

	# Statistics Options
	Average = True,
	Std = True,
	Percentili = True,
	Minima = True,
	Maxima = True,

	#Dump To file

	Dump_To_File = False,
	path_to_save="/home/clivi/OGS/ArealStat.txt",
	percs = "0.05,0.25,0.5,0.75,0.95",
	depths = "200,600,6000"



	);


def RequestData():
	from OGSParaviewSuite import OGSvtk,OGSStatistics
	import numpy as np 
	import vtk
	from vtk.util import numpy_support as npvtk
	from mpi4py import MPI

	comm = MPI.COMM_WORLD
	rank = comm.Get_rank()
	size = comm.Get_size()

	if rank == 0:
		perc = np.array(percs.split(','),dtype=np.float64)
		depth_list = np.array(depths.split(','),dtype=np.float64)
		depth_list = 1000 * depth_list
		# Get input data



		pdin = vtk.vtkUnstructuredGrid()
		pdin.ShallowCopy(self.GetInput())
		output = vtk.vtkTable()

		
		# Get Metadata from input
		path_to_mesh = pdin.GetFieldData().GetAbstractArray('path_to_mesh').GetValue(0)
		resolution = pdin.GetFieldData().GetAbstractArray('resolution').GetValue(0)

		pdin.GetFieldData().RemoveArray('resolution')
		pdin.GetFieldData().RemoveArray('path_to_mesh')
		Statistic = OGSStatistics(str(path_to_library))

		Structure = OGSvtk(str(path_to_mesh))
		Structure.SetResolution(str(resolution))
		output.GetFieldData().AddArray( Structure.createVTKstrf('Plot Type','Statistic'))

		depth = Structure.nav_lev[:Structure.jpk-1]
		depth_vtk = npvtk.numpy_to_vtk(depth/1000.,1)
		depth_vtk.SetName("depth")
		output.AddColumn(depth_vtk)


		for i in range(pdin.GetCellData().GetNumberOfArrays()):
			var = pdin.GetCellData().GetArrayName(i)
			
			if var == 'basins mask' or var == 'coasts mask' or var =='depth':	continue;
			if var == 'Velocity' or var == 'Velocity (forcings)':	continue; #To Be done in the future

			if Statistics == 1: 
				ArealStatistics   = Statistic.FlexibleArealStatistic(inp = pdin,nav = Structure.nav_lev,varname =var, percs = perc)
				
				if Average:
					aux = np.array(ArealStatistics[:len(ArealStatistics[:,0])-1,0])
					avg = npvtk.numpy_to_vtk(aux,1)
					avg.SetName(var+" Avg")
					output.AddColumn(avg)
				if Std:
					aux1 = np.array(ArealStatistics[:len(ArealStatistics[:,0])-1,1])
					std = npvtk.numpy_to_vtk(aux1,1)
					std.SetName(var+" Std")
					output.AddColumn(std)

				if Minima:
					aux2 = np.array(ArealStatistics[:len(ArealStatistics[:,0])-1,2])
					minima = npvtk.numpy_to_vtk(aux2,1)
					minima.SetName(var+" Minima")
					output.AddColumn(minima)
				if Percentili:
					for p in range(len(perc)):
						aux3 = np.array(ArealStatistics[:len(ArealStatistics[:,0])-1,p+3])
						percentili = npvtk.numpy_to_vtk(aux3,1)
						percentili.SetName(var+" "+str(perc[p])+" wp")
						output.AddColumn(percentili)
				
				
				if Maxima:
					aux4 = np.array(ArealStatistics[:len(ArealStatistics[:,0])-1,-1])
					maxima = npvtk.numpy_to_vtk(aux4,1)
					maxima.SetName(var+" Maxima")
					output.AddColumn(maxima)
			if Statistics == 2:
				VolumeStatistics = Statistic.FlexibleVolumeStatistic(inp = pdin,nav = Structure.nav_lev,varname = var, depth_list = depth_list, percs = perc)
				
				if Average:
					aux = np.array(VolumeStatistics[:len(VolumeStatistics[:,0])-1,0])
					avg = npvtk.numpy_to_vtk(aux,1)
					avg.SetName(var+" Avg")
					output.AddColumn(avg)
				if Std:
					aux1 = np.array(VolumeStatistics[:len(VolumeStatistics[:,0])-1,1])
					std = npvtk.numpy_to_vtk(aux1,1)
					std.SetName(var+" Std")
					output.AddColumn(std)

				if Minima:
					aux2 = np.array(VolumeStatistics[:len(VolumeStatistics[:,0])-1,2])
					minima = npvtk.numpy_to_vtk(aux2,1)
					minima.SetName(var+" Minima")
					output.AddColumn(minima)
				if Percentili:
					for p in range(len(perc)):
						aux3 = np.array(VolumeStatistics[:len(VolumeStatistics[:,0])-1,p+3])
						percentili = npvtk.numpy_to_vtk(aux3,1)
						percentili.SetName(var+" "+str(perc[p])+" wp")
						output.AddColumn(percentili)
				
				
				if Maxima:
					aux4 = np.array(VolumeStatistics[:len(VolumeStatistics[:,0])-1,-1])
					maxima = npvtk.numpy_to_vtk(aux4,1)
					maxima.SetName(var+" Maxima")
					output.AddColumn(maxima)

			if Dump_To_File:
				np.savetxt(path_to_save,ArealStatistics)


		# Update the output port

		self.GetOutput().ShallowCopy(output)
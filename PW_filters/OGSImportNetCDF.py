# NetCDF4 Import. Paraview programable source script.
#
# Access scalar data from OGSTM-BFM code in NetCDF format
# and import it directly to ParaView.
#
# To be used with OGSTM-BFM data.
#
# Arnau Miro, SoHPC 2017

Name  = 'OGSImportNetCDF'
Label = 'OGS Import NetCDF'
Help  = 'Imports NetCDF data from OGS'

NumberOfInputs = 0 # programable source
InputDataType = ''
OutputDataType = 'vtkRectilinearGrid'
ExtraXml = ''

Properties = dict(
	File_date            = '20150605-12:00:00',
	File_time_forcings   = '00:00:00', # Forcings are assumed to have different date
	Mesh_resolution      = 1, # To work with enum 1: high 2: mid 3: low
	Path_to_mesh         = '/media/projects/SoHPC2017/netcdf4_paraview/MESHMASK/',
	Path_to_python       = '/media/projects/SoHPC2017/netcdf4_paraview/',
	Path_to_ave_freq	 = '/media/projects/SoHPC2017/netcdf4_paraview/AVE_FREQ/',
	Path_to_ave_phys     = '/media/projects/SoHPC2017/netcdf4_paraview/AVE_PHYS/',
	Path_to_forcings     = '/media/projects/SoHPC2017/netcdf4_paraview/FORCING/',
	# Biogeochemical variables
	V_Ac                 = True,
	V_B1c                = False,
	V_B1n                = False,
	V_B1p                = False,
	V_CaCO3flux_alk      = False,
	V_CaCO3flux_dic      = False,
	V_CO2airflux         = False,
	V_CO2                = False,
	V_CO3                = False,
	V_dALK_NH4           = False,
	V_dALK_NO3_PO4       = False,
	V_DIC                = True,             
	V_HCO3               = False,
	V_N1p                = True,
	V_N3n                = True,
	V_N4n                = True,
	V_N5s                = True,
	V_N6r                = False,
	V_O2o                = True,
	V_O3c_concdilu_flux  = False,
	V_O3c                = False,
	V_O3h_concdilu_flux  = False,
	V_O3h                = False,
	V_O4n                = False, 
	V_P1c                = True,
	V_P1l                = True,             
	V_P1n                = False,              
	V_P1p                = False,              
	V_P1s                = False,              
	V_P2c                = True,
	V_P2l                = True,              
	V_P2n                = False,              
	V_P2p                = False,              
	V_P3c                = True,
	V_P3l                = True,             
	V_P3n                = False,              
	V_P3p                = False,              
	V_P4c                = True,
	V_P4l                = True,
	V_P4n                = False,
	V_P4p                = False,
	V_pCO2               = False,
	V_PH                 = False,
	V_ppb                = False,
	V_ppg                = False,
	V_ppn                = True,
	V_R1c                = False,
	V_R1n                = False,
	V_R1p                = False,
	V_R1s                = False,
	V_R2c                = False,
	V_R6c                = False,
	V_R6n                = False,
	V_R6p                = False,
	V_R6s                = False,
	V_R7c                = False,
    V_resMEZ1c           = False,
    V_resMEZ2c           = False,
    V_resMIZ1c           = False,
    V_resMIZ2c           = False,
	V_resPBA             = False,
	V_resPPY1c           = False,
	V_resPPY2c           = False,
	V_resPPY3c           = False,
	V_resPPY4c           = False,
	V_Z3c                = False,
	V_Z3n                = False,
	V_Z3p                = False,
	V_Z4c                = False,
	V_Z4n                = False,
	V_Z4p                = False,
	V_Z5c                = False,
	V_Z5n                = False,
	V_Z5p                = False,
	V_Z6c                = False,
	V_Z6n                = False,
	V_Z6p                = False,
	# Composite variables 
	V_chlorophyll        = True,
	# Physical variables
	V_velocity           = True,
	V_temperature        = True,
	V_salinity           = False,
	V_eddy_diff          = False,
	V_short_rad          = False,
	V_wind_spd           = False,
	V_wat_flux           = False,
	# Forcings variables
	V_f_velocity         = False,
	V_f_temperature      = False,
);

def RequestInformation():
	executive = self.GetExecutive()
	outInfo = executive.GetOutputInformation(0)
	dataType = 10 # VTK_FLOAT
	numberOfComponents = 1
	vtk.vtkDataObject.SetPointDataActiveScalarInfo(outInfo, dataType, numberOfComponents)

def RequestData():
	import sys, os
	import numpy as np

	sys.path.append(Path_to_python);

	from OGSParaviewSuite import OGSvtk
	from mpi4py import MPI

	comm = MPI.COMM_WORLD
	rank = comm.Get_rank()
	size = comm.Get_size()

	if rank == 0:
		# Initialize class
		OGS = OGSvtk(Path_to_mesh);

		# Mesh resolution
		if Mesh_resolution == 1: OGS.SetResolution('high');
		if Mesh_resolution == 2: OGS.SetResolution('mid');
		if Mesh_resolution == 3: OGS.SetResolution('low');

		# Create a VTK rectilinear grid
		rg = OGS.CreateRectilinearGrid();

		# Initialize the sub masks
		OGS.AddMask(rg,AddSubMask=True,AddCoastMask=True,AddDepth=True);

		# Append the date to the dataset
		rg.GetFieldData().AddArray( OGS.createVTKstrf("Date","%s" % File_date) );

		# Process biogeochemical variables
		if os.path.isdir(Path_to_ave_freq): # Failsafe for dir existance
			varn_list = ["Ac",  "B1c", "B1n", "B1p", "CaCO3flux_alk", "CaCO3flux_dic", "CO2airflux",\
			 			 "CO2", "CO3", "dALK_NH4", "dALK_NO3_PO4", "DIC", "HCO3", "N1p", "N3n", "N4n",\
			 			 "N5s", "N6r", "O2o", "O3c_concdilu_flux", "O3c", "O3h_concdilu_flux", "O3h",\
			 			 "O4n", "P1c", "P1l", "P1n", "P1p", "P1s", "P2c", "P2l", "P2n", "P2p", "P3c",\
			 			 "P3l", "P3n", "P3p", "P4c", "P4l", "P4n", "P4p", "pCO2", "PH", "ppb", "ppg",\
			 			 "ppn", "R1c", "R1n", "R1p", "R1s", "R2c", "R6c", "R6n", "R6p", "R6s", "R7c",\
			 			 "resMEZ1c", "resMEZ2c", "resMIZ1c", "resMIZ2c", "resPBA", "resPPY1c", "resPPY2c",\
			 			 "resPPY3c", "resPPY4c", "Z3c", "Z3n", "Z3p", "Z4c", "Z4n", "Z4p", "Z5c", "Z5n",\
			 			 "Z5p", "Z6c", "Z6n", "Z6p"] 
			varb_list = [V_Ac, V_B1c, V_B1n, V_B1p, V_CaCO3flux_alk, V_CaCO3flux_dic, V_CO2airflux,\
			 			 V_CO2, V_CO3, V_dALK_NH4, V_dALK_NO3_PO4, V_DIC, V_HCO3, V_N1p, V_N3n, V_N4n,\
			 			 V_N5s, V_N6r, V_O2o, V_O3c_concdilu_flux, V_O3c, V_O3h_concdilu_flux, V_O3h,\
			 			 V_O4n, V_P1c, V_P1l, V_P1n, V_P1p, V_P1s, V_P2c, V_P2l, V_P2n, V_P2p, V_P3c,\
			 			 V_P3l, V_P3n, V_P3p, V_P4c, V_P4l, V_P4n, V_P4p, V_pCO2, V_PH, V_ppb, V_ppg,\
			 			 V_ppn, V_R1c, V_R1n, V_R1p, V_R1s, V_R2c, V_R6c, V_R6n, V_R6p, V_R6s, V_R7c,\
			 			 V_resMEZ1c, V_resMEZ2c, V_resMIZ1c, V_resMIZ2c, V_resPBA, V_resPPY1c, V_resPPY2c,\
			 			 V_resPPY3c, V_resPPY4c, V_Z3c, V_Z3n, V_Z3p, V_Z4c, V_Z4n, V_Z4p, V_Z5c, V_Z5n,\
			 			 V_Z5p, V_Z6c, V_Z6n, V_Z6p]
			# Postprocess variables in batch mode
			for ii in range(0,len(varn_list)):
				if varb_list[ii]:
					fname = "%s/ave.%s.%s.nc" % (Path_to_ave_freq,File_date,varn_list[ii])
					if os.path.exists(fname):
						# Load from NetCDF
						data = OGS.LoadNetCDF.GetVariable(fname,varn_list[ii]);
						# Write data
						rg.GetCellData().AddArray( OGS.createVTKscaf(varn_list[ii],data) );
			# Postprocess composite variables
			if V_chlorophyll:
				data = OGS.LoadNetCDF.GetVariable("%s/ave.%s.P1l.nc" % (Path_to_ave_freq,File_date),"P1l") \
				     + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P2l.nc" % (Path_to_ave_freq,File_date),"P2l") \
				     + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P3l.nc" % (Path_to_ave_freq,File_date),"P3l") \
				     + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P4l.nc" % (Path_to_ave_freq,File_date),"P4l");
				rg.GetCellData().AddArray( OGS.createVTKscaf("Chlorophyll",data) );		

		# Process physical variables
		fname = "%s/ave.%s.phys.nc" % (Path_to_ave_phys,File_date)
		if os.path.exists(fname):
			# Velocity
			if V_velocity:
				# Load from NetCDF
				U = OGS.LoadNetCDF.GetVariable(fname,"vozocrtx");
				V = OGS.LoadNetCDF.GetVariable(fname,"vomecrty");
				W = OGS.LoadNetCDF.GetVariable(fname,"vovecrtz");
				# Convert to cell centers
				#U, V, W = OGS.LoadNetCDF.faces2cellcenter(U,V,W);
				# Write data
				rg.GetCellData().AddArray( OGS.createVTKvecf3("Velocity",U,V,W) );
			# Temperature
			if V_temperature:
				# Load from NetCDF
				data = OGS.LoadNetCDF.GetVariable(fname,"votemper");
				# Write data
				rg.GetCellData().AddArray( OGS.createVTKscaf("Temperature",data) );
			# Salinity
			if V_salinity:
				# Load from NetCDF
				data = OGS.LoadNetCDF.GetVariable(fname,"vosaline");
				# Write data
				rg.GetCellData().AddArray( OGS.createVTKscaf("Salinity",data) );
			# Vertical eddy diffusivity
			if V_eddy_diff:
				# Load from NetCDF
				data = OGS.LoadNetCDF.GetVariable(fname,"votkeavt");
				# Write data
				rg.GetCellData().AddArray( OGS.createVTKscaf("Vertical eddy diffusivity",data) );
			# Short wave radiation
			if V_short_rad:
				# Load from NetCDF
				data = OGS.LoadNetCDF.GetVariable(fname,"votkeavt");
				# Write data
				rg.GetCellData().AddArray( OGS.createVTKscaf("Short wave radiation",data) );
			# Wind speed
			if V_wind_spd:
				# Load from NetCDF
				data = OGS.LoadNetCDF.GetVariable(fname,"sowindsp");
				# Write data
				rg.GetCellData().AddArray( OGS.createVTKscaf("Wind speed",data) );
			# Water flux
			if V_wat_flux:
				# Load from NetCDF
				data = OGS.LoadNetCDF.GetVariable(fname,"sowaflcd");
				# Write data
				rg.GetCellData().AddArray( OGS.createVTKscaf("Water flux",data) );

		# Process forcings variables
		if os.path.isdir(Path_to_forcings):
			d = File_date.split('-')[0]
			if V_f_velocity:
				U = OGS.LoadNetCDF.GetVariable("%s/U%s-%s.nc" % (Path_to_forcings,d,File_time_forcings),"vozocrtx");
				V = OGS.LoadNetCDF.GetVariable("%s/V%s-%s.nc" % (Path_to_forcings,d,File_time_forcings),"vomecrty");
				W = OGS.LoadNetCDF.GetVariable("%s/W%s-%s.nc" % (Path_to_forcings,d,File_time_forcings),"vovecrtz");
				# Convert to cell centers
				U, V, W = OGS.LoadNetCDF.faces2cellcenter(U,V,W);
				# Write data
				rg.GetCellData().AddArray( OGS.createVTKvecf3("Velocity (forcings)",U,V,W) );
			if V_f_temperature:
				# Load from NetCDF
				data = OGS.LoadNetCDF.GetVariable("%s/T%s-%s.nc" % (Path_to_forcings,d,File_time_forcings),"votemper");
				# Write data
				rg.GetCellData().AddArray( OGS.createVTKscaf("Temperature (forcings)",data) );

		# Output the file to ParaView
		self.GetOutput().ShallowCopy(rg);
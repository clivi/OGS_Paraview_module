# Domain Analysis, Paraview programmable filter

# Cosimo Livi, MHPC 2017
Name  = 'OGSFlexibleAnalysis'
Label = 'OGS Flexible Analysis'
Help  = ''

NumberOfInputs = 1
InputDataType  = 'vtkUnstructuredGrid'
OutputDataType = 'vtkUnstructuredGrid'
ExtraXml = ''

Properties = dict(
	# Path to c libs
	path_to_library="/pico/scratch/userexternal/clivi001/OGS/clib/src",

	# Statistics Selection
	Areal_Statistics  = True, # Perform Areal Statistics
	Volume_Statistics = True,  # Perform Volume Statistics

	# Statistics Options
	Average = True,
	Std = True,
	Percentili = True,
	Minima = True,
	Maxima = True,

	percs = "0.05,0.25,0.5,0.75,0.95",
	depths = "200,600,6000"



	);


def RequestData():
	from OGSParaviewSuite import OGSvtk,OGSStatistics
	import numpy as np 
	import vtk
	from vtk.util import numpy_support as npvtk

	perc = np.array(percs.split(','),dtype=np.float64)
	depth_list = np.array(depths.split(','),dtype=np.float64)
	depth_list = 1000 * depth_list
	# Get input data
	#pdin = self.GetInput();


	pdin = vtk.vtkUnstructuredGrid()
	pdin.ShallowCopy(self.GetInput())



	path_to_mesh = pdin.GetFieldData().GetAbstractArray('path_to_mesh').GetValue(0)
	resolution = pdin.GetFieldData().GetAbstractArray('resolution').GetValue(0)

	pdin.GetFieldData().RemoveArray('resolution')
	pdin.GetFieldData().RemoveArray('path_to_mesh')
	Statistic = OGSStatistics(str(path_to_library))

	Structure = OGSvtk(str(path_to_mesh))
	Structure.SetResolution(str(resolution))

	for i in range(pdin.GetCellData().GetNumberOfArrays()):
		var = pdin.GetCellData().GetArrayName(i)
		
		if var == 'basins mask' or var == 'coasts mask' or var =='depth':	continue;
		
		if Areal_Statistics: 
			ArealStatistics  = Statistic.FlexibleArealStatistic(inp = pdin,nav = Structure.nav_lev,varname =var,DoPercentili = True, percs = perc)
			if Average:
				aux = np.array(ArealStatistics[:,0],dtype=np.float64)
				
				Statistic.Statistic_to_vtkUnstructuredGrid(pdin,var+" Areal Average",aux)
			if Std:
				aux = np.array(ArealStatistics[:,1],dtype=np.float64)
				Statistic.Statistic_to_vtkUnstructuredGrid(pdin,var+" Areal Std",aux)

			if Minima:
				aux = np.array(ArealStatistics[:,2],dtype=np.float64)
				Statistic.Statistic_to_vtkUnstructuredGrid(pdin,var+" Areal Minima",aux)

			if Percentili:
				for p in range(len(perc)):		
					aux = np.array(ArealStatistics[:,p+3],dtype=np.float64)
					Statistic.Statistic_to_vtkUnstructuredGrid(pdin,var+" "+str(perc[p])+" Areal wp",aux)
			
			if Maxima:
				aux = np.array(ArealStatistics[:,-1],dtype=np.float64)
				Statistic.Statistic_to_vtkUnstructuredGrid(pdin,var+" Areal Maxima",aux)




		if Volume_Statistics:
			VolumeStatistics = Statistic.FlexibleVolumeStatistic(inp = pdin,nav = Structure.nav_lev,varname = var, depth_list = depth_list,DoPercentili = True, percs = perc)			
			if Average:
				aux = np.array(VolumeStatistics[:,0],dtype=np.float64)
				Statistic.Statistic_to_vtkUnstructuredGrid(pdin,var+" Volume Average",aux)
			if Std:
				aux = np.array(VolumeStatistics[:,1],dtype=np.float64)
				Statistic.Statistic_to_vtkUnstructuredGrid(pdin,var+" Volume Std",aux)

			if Minima:
				aux = np.array(VolumeStatistics[:,2],dtype=np.float64)
				Statistic.Statistic_to_vtkUnstructuredGrid(pdin,var+" Volume Minima",aux)

			if Percentili:
				for p in range(len(perc)):		
					aux = np.array(VolumeStatistics[:,p+3],dtype=np.float64)
					Statistic.Statistic_to_vtkUnstructuredGrid(pdin,var+" "+str(perc[p])+" Volume wp",aux)
			
			if Maxima:
				aux = np.array(VolumeStatistics[:,-1],dtype=np.float64)
				Statistic.Statistic_to_vtkUnstructuredGrid(pdin,var+" Volume Maxima",aux)





	# Update the output port

	self.GetOutput().ShallowCopy(pdin)
	

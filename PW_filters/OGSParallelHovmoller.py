#Source for Hovmoller Plot
#
#Cosimo Livi, MHPC 2017

Name  = 'OGSParallelHovmoller'
Label = 'OGS Parallel Hovmoller Plot'
Help  = 'Produces Hovmoller plot for selected variables'

NumberOfInputs = 0 # programable source
InputDataType = ''
OutputDataType = 'vtkTable'
ExtraXml = ''

Properties = dict(
	Mesh_resolution      = 2, # To work with enum 1: high 2: mid 3: low
	Path_to_mesh         = '/home/clivi/OGS/MESHMASK',
	Path_to_python       = '/home/clivi/OGS/bit.sea/',
	Path_to_ave_freq	 = '/home/clivi/OGS/INPUT/LOW',
	Start_date			 = '19000101',
	End_date 			 = '22000101',
	# Biogeochemical variables
	Variable 			 = 'N3n',


	path_to_library="/home/clivi/OGS/clib/src",
	# Coast Selection
	coast = True,    # Value of 1 in CoastMask
	open_sea = True,  # Value of 2 in CoastMask
	

	# Sub Basins Selection

	Alboran_Sea                      = True, # alb  => 1
	South_Western_Mediterranean_west = True, # swm1 => 2
	South_Western_Mediterranean_east = True, # swm2 => 3
	North_Western_Mediterranean      = True, # nwm  => 4
	Northern_Tyrrhenian              = True, # tyr1 => 5
	Southern_Tyrrhenian              = True, # tyr2 => 6
	Western_Ionian                   = True, # ion1 => 7
	Eastern_Ionian                   = True, # ion2 => 8
	Northern_Ionian                  = True, # ion3 => 9
	Northern_Adriatic                = True, # adr1 => 10
	Southern_Adriatic                = True, # adr2 => 11
	Western_Levantine                = True, # lev1 => 12
	Northern_Levantine               = True, # lev2 => 13
	Southern_Levantine               = True, # lev3 => 14
	Eastern_Levantine                = True, # lev4 => 15
	Aegean_Sea                       = True,  # aeg  => 16

	# Statistics Options
	Statistic_type=1,
	#Average = True,
	#Std = False,
	#Percentili = False,
	#Minima = False,
	#Maxima = False,
	perc_to_visualize = "0.05"

);


def RequestInformation():
	executive = self.GetExecutive()
	outInfo = executive.GetOutputInformation(0)
	dataType = 10 # VTK_FLOAT
	numberOfComponents = 1
	vtk.vtkDataObject.SetPointDataActiveScalarInfo(outInfo, dataType, numberOfComponents)


def RequestData():
	import sys, os, glob
	import numpy as np
	import vtk
	sys.path.append(Path_to_python);
	from vtk.util import numpy_support as npvtk
	from OGSParaviewSuite import OGSvtk,OGSStatistics
	from mpi4py import MPI

	comm = MPI.COMM_WORLD
	rank = comm.Get_rank()
	size = comm.Get_size()

	if rank == 0: start_time = time.time()
	# Get info object
	executive = self.GetExecutive()
	outInfo = executive.GetOutputInformation(0)
	# Set type of data
	dataType = 10 # VTK_FLOAT
	numberOfComponents = 1
	vtk.vtkDataObject.SetPointDataActiveScalarInfo(outInfo,dataType,numberOfComponents)
	# Scan the AVE_FREQ folder for data

	fileList = glob.glob( "%s/ave.*.%s.nc" % (Path_to_ave_freq,Variable) ); 
	fileList.sort(); l = len(fileList);
	# Set time steps
	Dates = []
	for ii in range(0,l):
		date = ''.join(list(os.path.basename(fileList[ii])[4:12]))
		if (int(Start_date) <= int(date)) and (int(End_date)>= int(date)):
			Dates.append( fileList[ii].split('.')[1] );

	perc = np.array([0.05,0.25,0.5,0.75,0.95],dtype=np.float64)



	if rank == 0:
		output = vtk.vtkTable()
	# Initialize class
	OGS = OGSvtk(Path_to_mesh);

	# Mesh resolution
	if Mesh_resolution == 1: OGS.SetResolution('high');
	if Mesh_resolution == 2: OGS.SetResolution('mid');
	if Mesh_resolution == 3: OGS.SetResolution('low');

	# Main Loop over dates
	
	coastness = ""
	subbasins = []
	aux = np.zeros(OGS.jpk-1)
	TableSub = npvtk.numpy_to_vtk(aux,1)
	TableCoast = npvtk.numpy_to_vtk(aux,1)

	TableSub.SetName("Subbasins")
	TableCoast.SetName("Coastness")

	for i in range(16):
		if i <= 2: TableCoast.SetValue(i,0);
		TableSub.SetValue(i,0)
	# Define Domain for statistics
	if coast and open_sea:     coastness = "everywhere";TableCoast.SetValue(0,1);
	if coast and not open_sea: coastness = "coast";TableCoast.SetValue(1,1);
	if not coast and open_sea: coastness = "open_sea";TableCoast.SetValue(2,1);
	if not coast and not open_sea: 
		raise ValueError('ERROR: cannot perform analysis on only the ground') # Just the ground

	if Alboran_Sea:						 subbasins.append('alb');TableSub.SetValue(0,1);
	if South_Western_Mediterranean_west: subbasins.append('swm1');TableSub.SetValue(1,1);
	if South_Western_Mediterranean_east: subbasins.append('swm2');TableSub.SetValue(2,1);
	if North_Western_Mediterranean: 	 subbasins.append('nwm');TableSub.SetValue(3,1);
	if Northern_Tyrrhenian: 			 subbasins.append('tyr1');TableSub.SetValue(4,1);
	if Southern_Tyrrhenian: 			 subbasins.append('tyr2');TableSub.SetValue(5,1);
	if Western_Ionian: 					 subbasins.append('ion1');TableSub.SetValue(6,1);
	if Eastern_Ionian: 					 subbasins.append('ion2');TableSub.SetValue(7,1);
	if Northern_Ionian: 				 subbasins.append('ion3');TableSub.SetValue(8,1);
	if Northern_Adriatic: 				 subbasins.append('adr1');TableSub.SetValue(9,1);
	if Southern_Adriatic: 				 subbasins.append('adr2');TableSub.SetValue(10,1);
	if Western_Levantine: 				 subbasins.append('lev1');TableSub.SetValue(11,1);
	if Northern_Levantine: 				 subbasins.append('lev2');TableSub.SetValue(12,1);
	if Southern_Levantine: 				 subbasins.append('lev3');TableSub.SetValue(13,1);
	if Eastern_Levantine: 				 subbasins.append('lev4');TableSub.SetValue(14,1);
	if Aegean_Sea: 						 subbasins.append('aeg');TableSub.SetValue(15,1);
	
	if rank ==0:
		depth = OGS.nav_lev[:OGS.jpk-1]/1000.
		depth_vtk = npvtk.numpy_to_vtk(depth,1)
		depth_vtk.SetName("depth (m)")
		output.AddColumn(depth_vtk)
		output.AddColumn(TableCoast)
		output.AddColumn(TableSub)

	# Distribute data

	dateList = []
	loc_size = len(Dates) / size
	rest = len(Dates) % size
	start = rank * loc_size
	end = start + loc_size
	dateList = []
	if rest != 0:
		for i in range(rest):
			if rank == i:
				end += 1
			elif rank > i:
				start += 1
				end += 1
	comm.Barrier()
	for i in range(start,end):
		dateList.append(Dates[i])
		Result = []
	for date in dateList:
	
		# Create a VTK rectilinear grid
		rg = OGS.CreateRectilinearGrid();
		# Initialize the sub masks
		OGS.AddMask(rg,AddSubMask=True,AddCoastMask=True,AddDepth=False);



		# Process biogeochemical variables
		# Postprocess variables in batch mode

		fname = "%s/ave.%s.%s.nc" % (Path_to_ave_freq,date,Variable)
		if os.path.exists(fname):
			# Load from NetCDF
			data = OGS.LoadNetCDF.GetVariable(fname,Variable);
			# Write data
			rg.GetCellData().AddArray( OGS.createVTKscaf(Variable,data) );
			# Postprocess composite variables
			if Variable == "Chlorophyll":
				computedVars.append("Chlorophyll")
				data = OGS.LoadNetCDF.GetVariable("%s/ave.%s.P1l.nc" % (Path_to_ave_freq,File_date),"P1l") \
				     + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P2l.nc" % (Path_to_ave_freq,File_date),"P2l") \
				     + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P3l.nc" % (Path_to_ave_freq,File_date),"P3l") \
				     + OGS.LoadNetCDF.GetVariable("%s/ave.%s.P4l.nc" % (Path_to_ave_freq,File_date),"P4l");
				rg.GetCellData().AddArray( OGS.createVTKscaf("Chlorophyll",data) );	


		# Statistic session
		var = Variable
		Statistic = OGSStatistics(path_to_library)

		ArealStatistics   = Statistic.FastArealStatistic(OGS,rg,subbasins,coastness,var,perc)
		Result.append(ArealStatistics)



	if rank == 0:	
		for j in range(len(Result)):
			ArealStatistics = Result[j]
			if Statistic_type==1:
				aux = np.array(ArealStatistics[:,0])
				avg = npvtk.numpy_to_vtk(aux,1)
				avg.SetName(var+" Avg "+str(date))
				output.AddColumn(avg)
			if Statistic_type==2:
				aux1 = np.array(ArealStatistics[:,1])
				std = npvtk.numpy_to_vtk(aux1,1)
				avg.SetName(var+" std "+str(date))
				output.AddColumn(std)

			if Statistic_type==3:
				aux2 = np.array(ArealStatistics[:,2])
				minima = npvtk.numpy_to_vtk(aux2,1)
				minima.SetName(var+" Minima "+str(date))
				output.AddColumn(minima)
			if Statistic_type==4:
				for p in range(len(perc)):
					if str(perc[p])==perc_to_visualize:
						aux3 = np.array(ArealStatistics[:,p+3])
						percentili = npvtk.numpy_to_vtk(aux3,1)
						percentili.SetName(var+" "+str(perc[p])+"wp "+str(date))
						output.AddColumn(percentili)
						
			if Statistic_type==5:
				aux4 = np.array(ArealStatistics[:,-1])
				maxima = npvtk.numpy_to_vtk(aux4,1)
				maxima.SetName(var+" Maxima "+str(date))
				output.AddColumn(maxima)

	comm.Barrier()
	for i in range(1,size):
		loc_size = len(Result)
		if rank == i:
			comm.Send([np.array(loc_size)],MPI.INT, dest = 0)
		if rank == 0:
			aux = np.zeros(1,dtype=np.int)
			comm.Recv(aux, source = i)
			loc_size = aux[0]
		for j in range(loc_size):
			if rank == i:
				message = Result[j]
				comm.Send([message,MPI.Float], dest = 0)
			if rank == 0:
				comm.Recv(ArealStatistics,source = i)

				if Statistic_type==1:
					aux = np.array(ArealStatistics[:,0])
					avg = npvtk.numpy_to_vtk(aux,1)
					avg.SetName(var+" Avg "+str(date))
					output.AddColumn(avg)
				if Statistic_type==2:
					aux1 = np.array(ArealStatistics[:,1])
					std = npvtk.numpy_to_vtk(aux1,1)
					avg.SetName(var+" std "+str(date))
					output.AddColumn(std)

				if Statistic_type==3:
					aux2 = np.array(ArealStatistics[:,2])
					minima = npvtk.numpy_to_vtk(aux2,1)
					minima.SetName(var+" Minima "+str(date))
					output.AddColumn(minima)
				if Statistic_type==4:
					for p in range(len(perc)):
						if str(perc[p])==perc_to_visualize:
							aux3 = np.array(ArealStatistics[:,p+3])
							percentili = npvtk.numpy_to_vtk(aux3,1)
							percentili.SetName(var+" "+str(perc[p])+"wp "+str(date))
							output.AddColumn(percentili)
							
				if Statistic_type==5:
					aux4 = np.array(ArealStatistics[:,-1])
					maxima = npvtk.numpy_to_vtk(aux4,1)
					maxima.SetName(var+" Maxima "+str(date))
					output.AddColumn(maxima)
				comm.Barrier()		


	self.GetOutput().ShallowCopy(output)




















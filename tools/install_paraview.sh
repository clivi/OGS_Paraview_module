#!/bin/bash
#
# Automated script to configure and install PARAVIEW
# from source code, using legacy OpenGL backend
#
# Arnau Miro, SoHPC 2017

# Paraview version
VERS=5.4.0
INSTALL_PREFIX=/opt/paraview/$VERS


# Download PARAVIEW
MVERS=${VERS%.*}
if [ "${MVERS%.*}" -eq "4" ]; then
	PARAVIEW_DIR="ParaView-v${VERS}-source"
	PARAVIEW_TAR="${PARAVIEW_DIR}.tar.gz"
	PARAVIEW_DOWNLOAD="https://www.paraview.org/paraview-downloads/download.php?submit=Download&version=v${MVERS}&type=source&os=all&"
	PARAVIEW_SOURCE="downloadFile=${PARAVIEW_TAR}"
	if [ ! -f ${PARAVIEW_TAR} ]; then
	    wget -O ${PARAVIEW_TAR} --no-check-certificate ${PARAVIEW_DOWNLOAD}${PARAVIEW_SOURCE}
	fi
else
	PARAVIEW_DIR="ParaView-v${VERS}"
	PARAVIEW_TAR="${PARAVIEW_DIR}.tar.gz"
	PARAVIEW_DOWNLOAD="https://www.paraview.org/paraview-downloads/download.php?submit=Download&version=v${MVERS}&type=source&os=all&"
	PARAVIEW_SOURCE="downloadFile=${PARAVIEW_TAR}"
	if [ ! -f ${PARAVIEW_TAR} ]; then
	    wget -O ${PARAVIEW_TAR} --no-check-certificate ${PARAVIEW_DOWNLOAD}${PARAVIEW_SOURCE}
	fi
fi

# Move to PARAVIEW dir and prepare build
tar xvzf $PARAVIEW_TAR
cd $PARAVIEW_DIR
mkdir build
cd build

# Configure CMAKE
cmake ../ \
  -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_INSTALL_PREFIX=$INSTALL_PREFIX \
  -DBUILD_SHARED_LIBS=ON \
  -DBUILD_TESTING=OFF \
  -DVTK_RENDERING_BACKEND=OpenGL \
  -DVTK_SMP_IMPLEMENTATION_TYPE=OpenMP \
  -DVTK_USE_X=ON \
  -DPARAVIEW_QT_VERSION=4 \
  -DPARAVIEW_BUILD_QT_GUI=ON \
  -DPARAVIEW_ENABLE_CATALYST=ON \
  -DPARAVIEW_BUILD_CATALYST_ADAPTORS=ON \
  -DPARAVIEW_ENABLE_PYTHON=ON \
  -DPYTHON_INCLUDE_DIR="/usr/include/python2.7" \
  -DPYTHON_LIBRARY="/usr/lib/x86_64-linux-gnu/libpython2.7.so" \
  -DPYTHON_EXECUTABLE="/usr/bin/python" \
  -DPARAVIEW_USE_MPI=ON \
  -DMPI_C_INCLUDE_PATH="/usr/include" \
  -DMPI_CXX_INCLUDE_PATH="/usr/include" \
  -DMPI_C_COMPILER="/usr/bin/mpicc" \
  -DMPI_CXX_COMPILER="/usr/bin/mpicxx" \
  -DMPIEXEC="/usr/bin/mpiexec" \
  -DPARAVIEW_INSTALL_DEVELOPMENT_FILES=ON \
  -DCMAKE_C_COMPILER=mpicc \
  -DCMAKE_CXX_COMPILER=mpicxx

#  -DMPI_C_LIBRARIES="$MPI_DIR/lib/libmpi.so;$MPI_DIR/lib/libmpi_cxx.so" \
# Prompt user to check configuration
read -s -p "Please check install configuration and press [enter]..."

# Make and install
make 
sudo make install

#!/bin/bash
#
# Automated script to configure and install PARAVIEW
# in server-like environment.
#
# Arnau Miro, SoHPC 2017

# Paraview version
VERS=5.4.0
INSTALL_PREFIX=/pico/scratch/userexternal/amiro000/paraview/${VERS}-osmesa
PYTHON_VERS=2.7.9
GCC_VERS=4.8.3
MPI_VERS=1.8.2--gnu--4.8.3
CMAKE_VERS=3.5.2
LLVM_VERS=3.9.1
OMESA_VERS=13.0.6

# Load modules
module purge
module load profile/advanced
module load gnu/$GCC_VERS
module load openmpi/$MPI_VERS
module load cmake/$CMAKE_VERS
module load python/$PYTHON_VERS
module load qt/4.8.6--gnu--4.8.3

# Export llvm 
LLVM_DIR="/pico/scratch/userexternal/amiro000/paraview/llvm/llvm-${LLVM_VERS}"
export PATH=$PATH:$LLVM_DIR/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$LLVM_DIR/lib
export C_INCLUDE_PATH=$C_INCLUDE_PATH:$LLVM_DIR/include

# Variables
PYTHON_DIR="/cineca/prod/compilers/python/2.7.9/none"
MPI_DIR="/cineca/prod/compilers/openmpi/1.8.2/gnu--4.8.3"
MESA_DIR="/pico/scratch/userexternal/amiro000/paraview/mesa/osmesa-${MESA_VERS}"

# Download PARAVIEW
MVERS=${VERS%.*}
if [ "${MVERS%.*}" -eq "4" ]; then
	PARAVIEW_DIR="ParaView-v${VERS}-source"
	PARAVIEW_TAR="${PARAVIEW_DIR}.tar.gz"
	PARAVIEW_DOWNLOAD="https://www.paraview.org/paraview-downloads/download.php?submit=Download&version=v${MVERS}&type=source&os=all&"
	PARAVIEW_SOURCE="downloadFile=${PARAVIEW_TAR}"
	if [ ! -f ${PARAVIEW_TAR} ]; then
	    wget -O ${PARAVIEW_TAR} --no-check-certificate ${PARAVIEW_DOWNLOAD}${PARAVIEW_SOURCE}
	fi
else
	PARAVIEW_DIR="ParaView-v${VERS}"
	PARAVIEW_TAR="${PARAVIEW_DIR}.tar.gz"
	PARAVIEW_DOWNLOAD="https://www.paraview.org/paraview-downloads/download.php?submit=Download&version=v${MVERS}&type=source&os=all&"
	PARAVIEW_SOURCE="downloadFile=${PARAVIEW_TAR}"
	if [ ! -f ${PARAVIEW_TAR} ]; then
	    wget -O ${PARAVIEW_TAR} --no-check-certificate ${PARAVIEW_DOWNLOAD}${PARAVIEW_SOURCE}
	fi
fi

# Move to PARAVIEW dir and prepare build
tar xvzf $PARAVIEW_TAR
cd $PARAVIEW_DIR
mkdir build
cd build

# Configure CMAKE
cmake ../ \
  -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_INSTALL_PREFIX=$INSTALL_PREFIX \
  -DBUILD_SHARED_LIBS=ON \
  -DBUILD_TESTING=OFF \
  -DVTK_RENDERING_BACKEND=OpenGL \
  -DVTK_SMP_IMPLEMENTATION_TYPE=OpenMP \
  -DVTK_USE_X=OFF \
  -DVTK_OPENGL_HAS_OSMESA=ON \
  -DVTK_USE_OFFSCREEN=OFF \
  -DOPENGL_INCLUDE_DIR=IGNORE \
  -DOPENGL_xmesa_INCLUDE_DIR=IGNORE \
  -DOPENGL_gl_LIBRARY=IGNORE \
  -DOSMESA_INCLUDE_DIR=${MESA_DIR}/include \
  -DOSMESA_LIBRARY=${MESA_DIR}/lib/libOSMesa.so \
  -DPARAVIEW_QT_VERSION=4 \
  -DPARAVIEW_BUILD_QT_GUI=OFF \
  -DPARAVIEW_ENABLE_CATALYST=ON \
  -DPARAVIEW_BUILD_CATALYST_ADAPTORS=ON \
  -DPARAVIEW_ENABLE_PYTHON=ON \
  -DPYTHON_INCLUDE_DIR=$PYTHON_DIR/include/python${PYTHON_VERS%.*} \
  -DPYTHON_LIBRARY=$PYTHON_DIR/lib/libpython${PYTHON_VERS%.*}.so \
  -DPYTHON_EXECUTABLE=$PYTHON_DIR/bin/python \
  -DPARAVIEW_USE_MPI=ON \
  -DMPI_C_INCLUDE_PATH="$MPI_DIR/include" \
  -DMPI_CXX_INCLUDE_PATH="$MPI_DIR/include" \
  -DMPI_C_COMPILER="$MPI_DIR/bin/mpicc" \
  -DMPI_CXX_COMPILER="$MPI_DIR/bin/mpicxx" \
  -DMPIEXEC="$MPI_DIR/bin/mpiexec" \
  -DPARAVIEW_INSTALL_DEVELOPMENT_FILES=ON \
  -DCMAKE_C_COMPILER=mpicc \
  -DCMAKE_CXX_COMPILER=mpicxx

#  -DMPI_C_LIBRARIES="$MPI_DIR/lib/libmpi.so;$MPI_DIR/lib/libmpi_cxx.so" \
# Prompt user to check configuration
read -s -p "Please check install configuration and press [enter]..."

# Make and install
make -j4
make -j4 install

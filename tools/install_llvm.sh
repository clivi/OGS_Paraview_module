#!/bin/bash
#
# Automated script to compile LLVM
# language from source
#
# Arnau Miro, SoHPC 2017

LLVM_VERS=3.9.1

LLVM_PREFIX="/pico/scratch/userexternal/amiro000/paraview/llvm/llvm-${LLVM_VERS}"

GCC_VERS=4.8.3
MPI_VERS=1.8.2--gnu--4.8.3
CMAKE_VERS=3.5.2
PYTHON_VERS=2.7.9

module purge
module load profile/advanced
module load gnu/$GCC_VERS
module load openmpi/$MPI_VERS
module load cmake/$CMAKE_VERS
module load python/$PYTHON_VERS

LLVM_DIR="llvm-${LLVM_VERS}.src"
LLVM_TAR="${LLVM_DIR}.tar.xz"
LLVM_URL="http://releases.llvm.org/${LLVM_VERS}/${LLVM_TAR}"

# Download and uncompress
wget -O $LLVM_TAR --no-check-certificate $LLVM_URL
tar xvf $LLVM_TAR
mkdir $LLVM_DIR/build && cd $LLVM_DIR/build

# Set CMAKE
cmake .. \
	-DCMAKE_BUILD_TYPE=Release \
	-DCMAKE_INSTALL_PREFIX=$LLVM_PREFIX \
	-DLLVM_BUILD_LLVM_DYLIB=ON \
	-DLLVM_ENABLE_RTTI=ON \
	-DLLVM_INSTALL_UTILS=ON \
	-DLLVM_TARGETS_TO_BUILD:STRING=X86 \
        -DCMAKE_C_COMPILER=mpicc \
        -DCMAKE_CXX_COMPILER=mpicxx

# Prompt user to check configuration
read -s -p "Please check install configuration and press [enter]..."

# Make and install
make -j4
make -j4 install

#!/bin/bash
#
# Automated script to compile LLVM
# language from source
#
# Arnau Miro, SoHPC 2017

MESA_VERS=13.0.6

MESA_PREFIX="/pico/scratch/userexternal/amiro000/paraview/mesa/osmesa-${MESA_VERS}"

GCC_VERS=4.8.3
MPI_VERS=1.8.2--gnu--4.8.3
PYTHON_VERS=2.7.9
LLVM_VERS=3.9.1

module purge
module load profile/advanced
module load gnu/$GCC_VERS
module load openmpi/$MPI_VERS
module load python/$PYTHON_VERS

# Export llvm 
LLVM_DIR="/pico/scratch/userexternal/amiro000/paraview/llvm/llvm-${LLVM_VERS}"
export PATH=$PATH:$LLVM_DIR/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$LLVM_DIR/lib
export C_INCLUDE_PATH=$C_INCLUDE_PATH:$LLVM_DIR/include

MESA_DIR="mesa-${MESA_VERS}"
MESA_TAR="${MESA_DIR}.tar.gz"
MESA_URL="https://mesa.freedesktop.org/archive/${MESA_VERS}/${MESA_TAR}"

# Download and uncompress
wget -O $MESA_TAR --no-check-certificate $MESA_URL
tar xvzf $MESA_TAR
cd $MESA_DIR

./configure --prefix=$MESA_PREFIX \
  --enable-opengl --disable-gles1 --disable-gles2 \
  --disable-va --disable-xvmc --disable-vdpau \
  --enable-shared-glapi \
  --disable-texture-float \
  --enable-gallium-llvm --enable-llvm-shared-libs \
  --with-gallium-drivers=swrast \
  --disable-dri --with-dri-drivers= \
  --disable-egl --with-egl-platforms= --disable-gbm \
  --disable-glx \
  --disable-osmesa --enable-gallium-osmesa \
  CC=mpicc CXX=mpicxx
#  --with-gallium-drivers=swrast,swr \
# Prompt user to check configuration
read -s -p "Please check install configuration and press [enter]..."

# Make and install
make -j4
make -j4 install

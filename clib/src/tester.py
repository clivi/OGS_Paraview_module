from ctypes import *
import os
cwd = os.getcwd()
dso = CDLL(""+cwd+"/lib.so")
dso.cwp.argtypes = [POINTER(c_double),POINTER(c_double), c_int, POINTER(c_double),c_int,POINTER(c_double)]
#dso.c_wp.restype = c_int

size = 10
conc = (c_double * size)()
weight = (c_double * size)()
per_num= 4
percentiles =(c_double *per_num)()
percentiles[0]=0.25
percentiles[1]=0.5
percentiles[2]=0.7
percentiles[3]=0.9
out = (c_double *per_num)()

res = dso.cwp(conc,weight,size,percentiles,per_num,out)

#res = dso.sum(5,1)
#print res

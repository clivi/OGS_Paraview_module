#include <algorithm>
#include <iostream>
#include <cmath>
#include <vtkPLYReader.h>
#include <vtkPolyData.h>
#include <vtkFloatArray.h>
#include <vtkMultiBlockDataSet.h>
#include <vtkCellType.h>
#include <vtkCellArray.h>
#include <vtkActor.h>
#include <vtkPolyDataMapper.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkDataArray.h>
#include <vtkCallbackCommand.h>
#include <vtkCommand.h>
#include <vtkSmartPointer.h>
#include <vtkNew.h>
#include <vtkCellPicker.h>
#include <vtkPointData.h>
#include <vtkIdTypeArray.h>
#include <vtkIdList.h>
#include <vtkCellData.h>
#include <vtkCutter.h>
#include <vtkThreshold.h>
#include <vtkDataSetAttributes.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPlane.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkXMLPolyDataWriter.h>
#include <vtkLookupTable.h>
#include <vtkTriangle.h>
#include <vtkPoints.h>
#include <vector>
#include <vtkRectilinearGrid.h>
#include <vtkVersion.h>
#include <omp.h>
extern "C"{
//template <typename T>

#define nthreads 2

void sort_indexes(vtkFloatArray *v_int, int *idx_int, int size ) {

  //double *v_int = (double *) v;
  //int * idx_int = (int *) idx;
  // initialize original index locations
  //std::iota(idx_int, idx_int + size, 0);
  // for (int j= 0;j<size;j++) std::cout << idx_int[j] << "  ";
  // std::cout << "\n";
  // sort indexes based on comparing values in v
  std::sort(idx_int , idx_int + size,
       [&v_int](size_t i1, size_t i2) {return *v_int->GetTuple(i1) < *v_int->GetTuple(i2);});
}
//

void sort_indexes_float(std::vector<double> v_int, int *idx_int, int size ) {

  //double *v_int = (double *) v;
  //int * idx_int = (int *) idx;
  // initialize original index locations
  //std::iota(idx_int, idx_int + size, 0);
  // for (int j= 0;j<size;j++) std::cout << idx_int[j] << "  ";
  // std::cout << "\n";
  // sort indexes based on comparing values in v
  std::sort(idx_int , idx_int + size,
       [&v_int](size_t i1, size_t i2) {return v_int[i1] < v_int[i2];});
}

void vtkSetDimensions(vtkFloatArray* daLon, vtkFloatArray* daLat, vtkFloatArray* daDep,
                      double* LontoMeters, double* LattoMeters, double* nav_lev,
                      int jpi, int jpj, int jpk){
    #pragma omp parallel num_threads(nthreads)
    {
    #pragma omp for
    for (int i=0;i<jpi;i++) daLon->SetValue(i,-1.*LontoMeters[i]);
    #pragma omp for
    for (int i=0;i<jpj;i++) daLon->SetValue(i,LontoMeters[i]);
    #pragma omp for
    for (int i=0;i<jpk;i++) daLon->SetValue(i,1000.*nav_lev[i]);
    }


}


void add_vtkvectorC(float *input_array, vtkUnstructuredGrid* data, char * varname){
    vtkFloatArray*  u1 = vtkFloatArray::New();
    //std::cout << "varname in addvector =" << varname << "\n";
    u1->SetName(varname);
    u1->SetNumberOfComponents(1);
    u1->SetNumberOfTuples(data->GetNumberOfCells());
    int i=0;
    double old_sub = data->GetCell(0)->GetBounds()[4];
    double sub_z;

    for (int p = 0; p< data->GetNumberOfCells();p++){
          sub_z = data->GetCell(p)->GetBounds()[4];
          if (sub_z != old_sub) {
                    old_sub = sub_z;
                    i++;

                    }

          u1->SetValue(p, input_array[i] );
      }
      data->GetCellData()->AddArray(u1);
}
void cwp(vtkFloatArray *Conc, std::vector<double> Weight, int size, double *percentiles, int per_num, float* o){

  int idx[size];
  std::iota(idx, idx + size, 0);
//  std::cout << "Before sorting" << "\n";
  sort_indexes(Conc,idx,size);
  //  std::cout << "After sorting" << "\n";

     double aux = Conc->GetValue(idx[0]);
  //  std::cout << "min = " << aux <<  "\n";
  double sd[size],sw[size],aw[size],w[size];
  //std::cout << "Entering sum loop" << "\n";
  double sum =0;
  for (int i=0;i<size;i++){
    sd[i]=Conc->GetValue(idx[i]);
    sw[i]=Weight[idx[i]];
    sum += sw[i];
    aw[i]=sum;
  //  std:: cout << "sum = " << sum << "\n";
    }
  double min=0;
  double max=0;
  //std::cout << "Entering weight loop" << "\n";
  #pragma omp parallel num_threads(nthreads)
  {
  #pragma omp for reduction(min: min) reduction(max: max)
  for (int i=0;i<size;i++) {

    w[i]= (aw[i]-0.5*sw[i])/aw[size-1];
    if (i==0){
      min = w[i];
      max = w[i];
    }
    else {
      if (w[i]<min) min = w[i];
      if (w[i]>max) max = w[i];
    }
  }
  int spots[per_num];
  //std::cout << "Entering spots loop. min = " << min << " max = " << max << "\n";
  #pragma omp for
  for (int i=0;i<per_num;i++){
    if (percentiles[i] <= min) spots[i] = 0;
    if (percentiles[i] >= max) spots[i] = size - 1;
    if ((percentiles[i] > min) && (percentiles[i]< max)) {
      for(int j=0; j<size;j++){
        if ((percentiles[i]>=w[j]) && (percentiles[i]< w[j+1])) spots[i]=j;
         }
      }
  }
  //std::cout << "Entering percentili loop" << "\n";

  #pragma omp for
  for (int i=0;i<per_num;i++){
    //std::cout << "spots[" <<i<<"] = "<< spots[i] << "\n";

    if (spots[i]==0) o[i]=sd[0];//std::cout << sd[0] << std::endl;
    //std::cout << "spots size" << "\n";

    if (spots[i]==size) o[i]=sd[size-1];
    if ((spots[i] != 0) && (spots[i] != size)){
      //std::cout << "other spots" << "\n";

      double f1 = (w[spots[i]]-percentiles[i])/(w[spots[i]]-w[spots[i]-1]);
      double f2 = (percentiles[i]-w[spots[i]-1])/(w[spots[i]]-w[spots[i]-1]);
      o[i]=(float)(sd[spots[i]-1]*f1 + sd[spots[i]]*f2);
    }
  }
}

//std::cout << "End of the function wp" << "\n";
}

void Areal_Statistics_C(vtkUnstructuredGrid* input, double * nav_lev, int size, double * perc, int perc_num, float * WM, float * perc_out, char * varname){
 vtkNew<vtkCutter> cutter;
 vtkNew<vtkPlane> plane;
// int k = 0;



 for (int z = 0; z < size; z++){
   if (z==0)  plane->SetOrigin(0,0,nav_lev[z]+0.1);
   if (z!=0)  plane->SetOrigin(0,0,nav_lev[z]);
   plane->SetNormal(0,0,1);
   #if VTK_MAJOR_VERSION <= 5
     cutter->SetInput(input);
   #else
      cutter->SetInputData(input);
   #endif
   cutter->SetCutFunction(plane.GetPointer());
   cutter->Update();
   auto co = cutter->GetOutput();
   std::vector<double> loc_area(co->GetNumberOfCells());
   //  float * var = vtkArrayDownCast<float>(co->GetCellData()->GetArray(varname));
   //vtkFloatArray *var = co->GetCellData()->GetArray(varname);
   //std::cout << "varname in areal  " << varname << std::endl;
   vtkFloatArray* var = vtkFloatArray::SafeDownCast ( co->GetCellData()->GetArray( varname ));
   double sum =0;
   double var_sum = 0;
   static vtkTriangle* triangle;
  #pragma omp parallel for num_threads(nthreads) reduction(+:sum) reduction(+:var_sum)
  for (int i = 0; i < co->GetNumberOfCells();i++){
    //  printf("Areal statistic from thread %d on index %d\n",omp_get_thread_num(),i);
    //  printf("define cell and triangle from rank %d\n", omp_get_thread_num());

     double p0[3];
     double p1[3];
     double p2[3];
     #pragma omp critical
     triangle = dynamic_cast<vtkTriangle*>(co->GetCell(i));

    //  printf("before triangle from %d\n", omp_get_thread_num());
     triangle->GetPoints()->GetPoint(0,p0);
     triangle->GetPoints()->GetPoint(1,p1);
     triangle->GetPoints()->GetPoint(2,p2);
    //  printf("before sum from %d\n", omp_get_thread_num());

    loc_area[i] = vtkTriangle::TriangleArea(p0,p1,p2);
    sum += loc_area[i];
    var_sum += var->GetValue(i) * loc_area[i];

   }
   //printf("Out omp loop, z=%d\n\n\n",z);
  if (sum != 0){

       WM[z] = (float) (var_sum / sum);
       std::vector<float> perc_aux(perc_num);
       cwp(var, loc_area, co->GetNumberOfCells(), perc, perc_num, &(perc_aux)[0]);
       for (int j = 0; j < perc_num; j++){
         perc_out[perc_num *(z) + j] = perc_aux[j];
       }
     }
  }


}



void cwp2(std::vector<double> Conc, std::vector<double> Weight, int size, double *percentiles, int per_num, float* o){

  int idx[size];
  std::iota(idx, idx + size, 0);
//  std::cout << "Before sorting" << "\n";
  sort_indexes_float(Conc,idx,size);
  //  std::cout << "After sorting" << "\n";

     double aux = Conc[idx[0]];
  //  std::cout << "min = " << aux <<  "\n";
  double sd[size],sw[size],aw[size],w[size];
  //std::cout << "Entering sum loop" << "\n";
  double sum =0;
  for (int i=0;i<size;i++){
    sd[i]=Conc[i];
    sw[i]=Weight[idx[i]];
    sum += sw[i];
    aw[i]=sum;
  //  std:: cout << "sum = " << sum << "\n";
    }
  double min=0;
  double max=0;
  //std::cout << "Entering weight loop" << "\n";
  #pragma omp parallel num_threads(nthreads)
  {
  #pragma omp for reduction(min: min) reduction(max: max)
  for (int i=0;i<size;i++) {

    w[i]= (aw[i]-0.5*sw[i])/aw[size-1];
    if (i==0){
      min = w[i];
      max = w[i];
    }
    else {
      if (w[i]<min) min = w[i];
      if (w[i]>max) max = w[i];
    }
  }
  int spots[per_num];
  //std::cout << "Entering spots loop. min = " << min << " max = " << max << "\n";
  #pragma omp for
  for (int i=0;i<per_num;i++){
    if (percentiles[i] <= min) spots[i] = 0;
    if (percentiles[i] >= max) spots[i] = size - 1;
    if ((percentiles[i] > min) && (percentiles[i]< max)) {
      for(int j=0; j<size;j++){
        if ((percentiles[i]>=w[j]) && (percentiles[i]< w[j+1])) spots[i]=j;
         }
      }
  }
  //std::cout << "Entering percentili loop" << "\n";

  #pragma omp for
  for (int i=0;i<per_num;i++){
    //std::cout << "spots[" <<i<<"] = "<< spots[i] << "\n";

    if (spots[i]==0) o[i]=sd[0];//std::cout << sd[0] << std::endl;
    //std::cout << "spots size" << "\n";

    if (spots[i]==size) o[i]=sd[size-1];
    if ((spots[i] != 0) && (spots[i] != size)){
      //std::cout << "other spots" << "\n";

      double f1 = (w[spots[i]]-percentiles[i])/(w[spots[i]]-w[spots[i]-1]);
      double f2 = (percentiles[i]-w[spots[i]-1])/(w[spots[i]]-w[spots[i]-1]);
      o[i]=(float)(sd[spots[i]-1]*f1 + sd[spots[i]]*f2);
    }
  }
 }
//std::cout << "End of the function wp" << "\n";
}

void Areal_Statistics_C2(vtkUnstructuredGrid* inp, double * nav_lev, int size, double * perc, int perc_num, float * WM, float * perc_out, char * varname){

   
  std::vector<double> loc_area(inp->GetNumberOfCells());
  vtkFloatArray* var = vtkFloatArray::SafeDownCast ( inp->GetCellData()->GetArray( varname ));
  double sum =0;
  double var_sum = 0;
  int z = 0;
  int end_counter = 0;
  int start_counter = 0;

  for (int i = 0; i < inp->GetNumberOfCells();i++){
      
      end_counter += 1;
      vtkCell* cell = inp->GetCell(i);
      loc_area[i] = (fabs(cell->GetBounds()[1]-cell->GetBounds()[0]))*(fabs(cell->GetBounds()[3]-cell->GetBounds()[2]));
      sum += loc_area[i];
      var_sum += var->GetValue(i) * loc_area[i];
      if ((z+1) < size) {

        if (inp->GetCell(i+1)->GetBounds()[4] > nav_lev[z]) {
          WM[z] = (double) (var_sum / sum);
          std::vector<float> perc_aux(perc_num);
          std::vector<double> area_aux(end_counter-start_counter);
          std::vector<double> var_aux(end_counter-start_counter);
          # pragma omp parallel for num_threads(nthreads)
          for (int j= start_counter; j< end_counter; j++){
              area_aux[j-start_counter] = loc_area[j];
              var_aux[j-start_counter]  =  var->GetValue(j);
            }
          cwp2(var_aux, area_aux, end_counter-start_counter, perc, perc_num, &(perc_aux)[0]);
          for (int j = 0; j < perc_num; j++){
             perc_out[perc_num *(z) + j] = perc_aux[j];
             }
          z += 1;
          start_counter = end_counter;
          sum = 0;
          var_sum = 0;
          }
        } 
      if (i == inp->GetNumberOfCells()-1) {

          WM[z] = (double) (var_sum / sum);
          std::vector<float> perc_aux(perc_num);
          std::vector<double> area_aux(end_counter-start_counter);
          std::vector<double> var_aux(end_counter-start_counter);
          # pragma omp parallel for num_threads(nthreads)
          for (int j= start_counter; j< end_counter; j++){
              area_aux[j-start_counter] = loc_area[j];
              var_aux[j-start_counter]  =  var->GetValue(j);
            }
          cwp2(var_aux, area_aux, end_counter-start_counter, perc, perc_num, &(perc_aux)[0]);
          for (int j = 0; j < perc_num; j++){
             perc_out[perc_num *(z) + j] = perc_aux[j];
             }
          z += 1;
          start_counter = end_counter;
            }
    
    }
  }

void Volume_Statistics_C(vtkUnstructuredGrid* input, double* depth_list, int depth_size, double * nav_lev, int size, double * perc, int perc_num, float * WM, float * perc_out, char * varname){
 vtkNew<vtkThreshold> t;
 std::vector<float> aux(depth_size+1);
 std::vector<float> perc_out_aux(perc_num*(depth_size+1));

// int k = 0;

 for (int z = 0; z <= depth_size; z++){
   double lower,upper;
   if (z==0){
     lower = 0;
     upper = depth_list[z];
     }
    else if (z == depth_size){
      lower = depth_list[z-1];
      upper = nav_lev[size-1];
      }
    else {
      lower = depth_list[z-1];
      upper = depth_list[z];
     }

     #if VTK_MAJOR_VERSION <= 5
       t->SetInput(input);
     #else
       t->SetInputData(input);
     #endif

   t->ThresholdBetween(lower,upper);
   t->SetInputArrayToProcess(0,0,0,1,"depth");
   t->Update();

   auto to = t->GetOutput();

   std::vector<double> loc_volume(to->GetNumberOfCells());
   vtkFloatArray* var = vtkFloatArray::SafeDownCast ( to->GetCellData()->GetArray( varname ));

   double sum =0;
   double var_sum = 0;
  #pragma omp parallel for num_threads(nthreads) reduction(+:sum) reduction(+:var_sum)
  for (int i = 0; i < to->GetNumberOfCells();i++){

     static vtkCell* cell;

     cell = to->GetCell(i);

     //std::cout << "cell: \n " << cell << "\n"<< "bounds \n" << cell->GetBounds()[0]<<" "<< cell->GetBounds()[1]<<" "<<  cell->GetBounds()[2]<<" "<<  cell->GetBounds()[3] << "\n";
     loc_volume[i]=(fabs(cell->GetBounds()[1]-cell->GetBounds()[0]))*(fabs(cell->GetBounds()[3]-cell->GetBounds()[2]))*(fabs(cell->GetBounds()[4]-cell->GetBounds()[5]));
     sum += loc_volume[i];
     var_sum += var->GetValue(i) * loc_volume[i];
     //std::cout << " " << loc_volume[i] << " ";

     }
  if (sum == 0){
    aux[z] = 0;
    for (int j = 0; j < perc_num; j++) perc_out_aux[perc_num *(z) + j] = 0;
  }
  if (sum != 0){

       aux[z] = (float) (var_sum / sum);
       std::vector<float> perc_aux(perc_num);
       cwp(var, loc_volume, to->GetNumberOfCells(), perc, perc_num, &(perc_aux)[0]);
       for (int j = 0; j < perc_num; j++){
         perc_out_aux[perc_num *(z) + j] = perc_aux[j];
       }
     }

  }
  int p=0;
  for (int i = 0;i<size;i++){
    if (p < depth_size){
      if (nav_lev[i]>depth_list[p]) p++;
      }
    WM[i] = aux[p];
    for (int j=0;j<perc_num;j++) perc_out[perc_num * (i) + j] = perc_out_aux[perc_num * p + j];
  }
}


void C_TripleLoop(float * var, float* mask, int z, int y, int x){
  #pragma omp parallel for num_threads(nthreads) collapse(3)
  for (int k = 0;k<z;k++){
    for (int j = 0;j<y;j++){
      for (int i = 0; i<x;i++){
        if ((mask[y*x*k+x*j+i] == 0) || (var[y*x*k+x*j+i]>10000)) var[y*x*k+x*j+i] = std::numeric_limits<double>::quiet_NaN();
      }
    }
  }

}

void vtkArrayCreator(vtkFloatArray* u1, char* v, float * var, bool* mask, int z, int y, int x){
  //vtkFloatArray* u1= vtkFloatArray::New();
  u1->SetName(v);
  u1->SetNumberOfComponents(1);
  u1->SetNumberOfTuples((z-1)*(y-1)*(x-1));
  #pragma omp parallel for num_threads(nthreads) collapse(3)
  for (int k = 0;k<(z-1);k++){
    for (int j = 0;j<(y-1);j++){
      for (int i = 0; i<(x-1);i++){
        if ((mask[y*x*k+x*j+i] == false) || (var[y*x*k+x*j+i]>10000)) {
          u1->SetValue(i+j*(x-1)+k*(x-1)*(y-1), std::numeric_limits<double>::quiet_NaN());
           }
        else {
          u1->SetValue(i+j*(x-1)+k*(x-1)*(y-1), var[(x*y)*k+x*j+i] );
        }
      }
    }
  }

}

void C_CreateBlock(vtkRectilinearGrid* in, double* var, int z, int y, int x, double* nav_lev, double* LattoMeters, double* LontoMeters, char** var_list, int nvar){

  //vtkRectilinearGrid *rg = vtkRectilinearGrid::New();
  vtkSmartPointer<vtkRectilinearGrid> rg = in;
  rg->SetDimensions(z,y,x);
  vtkFloatArray* daLon = vtkFloatArray::New();
  vtkFloatArray* daLat= vtkFloatArray::New();
  vtkFloatArray* daDep= vtkFloatArray::New();

  daLon->SetNumberOfComponents(1);
  daLat->SetNumberOfComponents(1);
  daDep->SetNumberOfComponents(1);
  //
  daLon->SetNumberOfTuples(x);
  daLat->SetNumberOfTuples(y);
  daDep->SetNumberOfTuples(z);
  //
  #pragma omp parallel num_threads(nthreads)
  {
  #pragma omp for
  for (int i = 0;i<x;i++) daLon->SetValue(i,-1.0* LontoMeters[i]);
  #pragma omp for
  for (int i = 0;i<y;i++) daLat->SetValue(i, LattoMeters[i]);
  #pragma omp for
  for (int i = 0;i<z;i++) daDep->SetValue(i,1000* nav_lev[i]);
  }
  //
  rg->SetXCoordinates(daLon);
  rg->SetYCoordinates(daLat);
  rg->SetZCoordinates(daDep);
  //
  int jpim1=x-1;
  int jpjm1=y-1;
  int jpkm1=z-1;
  //
  for (int v = 0;v<nvar;v++){
    vtkFloatArray* u1= vtkFloatArray::New();
    u1->SetName(var_list[v]);
    u1->SetNumberOfComponents(1);
    u1->SetNumberOfTuples((jpim1)*(jpjm1)*(jpkm1));
    #pragma omp parallel for collapse(3)
    for (int k = 0;k<jpkm1;k++) {
        for (int j = 0;j<jpjm1;j++) {
            for (int i = 0;i<jpim1;i++) {
              u1->SetValue(i+j*jpim1+k*jpim1*jpjm1, 0);//var[(x*y*z)*v+(x*y)*k+x*j+i] );

            }
          }
        }
     rg->GetCellData()->AddArray(u1);
     }

    // vtkNew<vtkThreshold> t;
    // t->SetInput(rg);
    // t->ThresholdBetween(0,1000);
    // t->SetInputArrayToProcess(0,0,0,1,"N1p");
    // t->Update();
    // std::cout << "out inside c func before" << out->GetCellData() << std::endl;
    // out = t->GetOutput();
    // std::cout << "out after c func before" << out->GetCellData() << std::endl;

  }

void vtkThresholdC(vtkRectilinearGrid* rg, vtkUnstructuredGrid* out, char* var){
  printf("vinside threshol");
  vtkNew<vtkThreshold> t;
  #if VTK_MAJOR_VERSION <= 5
    t->SetInput(rg);
  #else
    t->SetInputData(rg);
  #endif

    t->ThresholdBetween(0,1000);
    printf("var = %s",var);
    t->SetInputArrayToProcess(0,0,0,1,var);
    t->Update();

    out = t->GetOutput();

}

}

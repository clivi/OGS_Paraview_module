#!/bin/bash

pwd="$PWD"


echo "Modifing self.filename variable inside ../bit.sea/static/Carbon_reader.py"
echo "Modifing self.filename variable inside ../bit.sea/static/Nutrient_reader.py"


cd ../bit.sea/static
t=5
sed -i -e 's,self.filename=.*,self.filename="'$pwd'/Dataset_Med_Nutrients.nc",g' Nutrients_reader.py
sed -i -e 's,self.filename=.*,self.filename="'$pwd'/Dataset_Med_CarbSys.nc",g' Carbon_reader.py

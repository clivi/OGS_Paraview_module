#!/usr/bin/env python
#
# Extractor routine from OGS maskload module.
# It will produce basins and coasts masks 
# binary files required in extraction routines.

import numpy as np
import os, argparse, sys
from commons.mask import Mask
from commons.submask import SubMask
from basins import V2 as OGS

def SUB(sub):
    ''' sub is a string'''
    return SUBM[sub]



# SUB[med] is not applied by aveScan. 
#If we want it we hav to apply to each subbasin


def read_Positions_for_Pointprofiles(filename):

    dtIN  = np.dtype([('Name','S20'), ('Lon',np.float32), ('Lat',np.float32)])
    dtOUT = np.dtype([('Name','S20'), ('Lon',np.float32), ('Lat',np.float32), ('i',np.int), ('j',np.int)])  
    MeasPoints=np.loadtxt(filename, dtype=dtIN, skiprows=1,ndmin=1)
    
    nMeas=MeasPoints.size
    MeasPoints_OUT=np.zeros((nMeas),dtype=dtOUT)
    MeasPoints_OUT['Name']= MeasPoints['Name']
    MeasPoints_OUT['Lon' ]= MeasPoints['Lon' ]
    MeasPoints_OUT['Lat' ]= MeasPoints['Lat' ]
    for k in xrange(nMeas):
        DIST = (Lon - MeasPoints['Lon'][k])**2 + (Lat - MeasPoints['Lat'][k])**2 
        ind=np.nonzero(DIST==DIST.min())# tuple  
        j = ind[0]
        i = ind[1]
        MeasPoints_OUT['i'][k] = i
        MeasPoints_OUT['j'][k] = j
    
    return MeasPoints_OUT

# Set input arguments
argpar = argparse.ArgumentParser();
argpar.add_argument('-i','--input',type=str,help='Full path to meshmask directory',required=True,dest='inpath');

argpar.add_argument('-r','--resolution',type=str,help='set resolution: low, mid or high',required=True,dest='res');
# Parse arguments
args = argpar.parse_args();
path =args.inpath+'/'+args.res
maskfile = '%s/meshmask.nc' % path

## maskload hardcoded

annaCoast = False

if maskfile is None : 
    print "Error: Environment variable MASKFILE must be defined "
    sys.exit(1)

if annaCoast:
    kcoastfile  = os.getenv( "KCOASTFILE");
    if kcoastfile is None:
        print "Error: Environment variable KCOASTFILE must be defined "
        sys.exit(1)

TheMask=Mask(maskfile,ylevelsmatvar="gphit", xlevelsmatvar="glamt")
jpk,jpj,jpi = TheMask.shape


tmask   =  TheMask.mask
nav_lev =  TheMask.zlevels
Lon     =  TheMask.xlevels
Lat     =  TheMask.ylevels
area    =  TheMask.area
e3t     =  TheMask.dz


MEDmask      = tmask.copy()
MEDmask[:, Lon < -5.3] = False# Atlantic buffer 

mask200_2D   = TheMask.mask_at_level(200.0)
#mask200_2D[Lon < -5.3] = False # Atlantic buffer
mask200_3D = np.zeros((jpk,jpj,jpi),dtype=np.bool)
for i in range(jpk):
    mask200_3D[i,:,:]=mask200_2D

if annaCoast:
    # Read k means class for coastal areas
    kcoastmask = np.load(kcoastfile)
    kmask1_2D =  np.zeros((jpj,jpi),dtype=np.bool)
    kmask1_2D[kcoastmask==1] = True
    kmask2_2D =  np.zeros((jpj,jpi),dtype=np.bool)
    kmask2_2D[kcoastmask==2] = True
    kmask1 =  np.zeros((jpk,jpj,jpi),dtype=np.bool)
    kmask2 =  np.zeros((jpk,jpj,jpi),dtype=np.bool)
    for i in range(jpk):
        kmask1[i,:,:] = kmask1_2D
        kmask2[i,:,:] = kmask2_2D

# Coastness must be defined one by one
COASTNESS_LIST=['coast', 'open_sea','everywhere']
if annaCoast: COASTNESS_LIST=['coast','open_sea','everywhere','coast1','coast2']
    
COASTNESS = np.ones((jpk,jpj,jpi) ,dtype=[(coast,np.bool) for coast in COASTNESS_LIST])
COASTNESS['open_sea']  =  mask200_3D;
COASTNESS['coast']     = ~mask200_3D;

if annaCoast:
    COASTNESS['coast1']  =  kmask1;
    COASTNESS['coast2']  =  kmask2;

# Depth are defined by their names and bottom levels
DEPTHlist   =['shallow','mid','deep']
Bottom_list =[200, 600, 6000]

if annaCoast:
    DEPTHlist   =['shallow','deep']
    Bottom_list =[200, 6000]
    

DEPTH  = np.zeros((jpk,jpj,jpi),dtype=[(depth,np.bool) for depth in DEPTHlist])
tk_top = 0
for idepth, depth in enumerate(DEPTHlist):
    tk_bottom = TheMask.getDepthIndex(Bottom_list[idepth])+1
    DEPTH[depth][tk_top:tk_bottom ,:,:] = True
    tk_top = tk_bottom


# tk_1     = TheMask.getDepthIndex(200.0)+1
# tk_2     = TheMask.getDepthIndex(600.0)+1
# 
# DEPTH['shallow'][0:tk_1   ,:,:] = True
# DEPTH['mid'    ][tk_1:tk_2,:,:] = True
# DEPTH['deep'   ][tk_2:    ,:,:] = True




Volume=np.zeros((jpk,jpj,jpi),dtype=np.double)
dZ     =np.ones ((jpk,jpj,jpi),dtype=np.double)
for i in range(jpk):
    Volume[i,:,:] = area*e3t[i]
    dZ[i,:,:]     = e3t[i]



SUBlist=[ sub.name for sub in OGS.P.basin_list ]
mydtype = [(sub.name, np.bool) for sub in OGS.P.basin_list ]
SUBM = np.zeros((jpk,jpj,jpi),dtype=mydtype)
for sub in SUBlist:
    index= SUBlist.index(sub)
    basin = OGS.P.basin_list[index]
    if args.res == 'low':
        s=SubMask(basin,Regular = False,maskobject = TheMask)
    else: 
        s=SubMask(basin,maskobject = TheMask)
    SUBM[sub] = s.mask


# Actual main extraction routine
#if "__name__" == __name__:

coasts_mask=np.zeros((jpk,jpj,jpi))
for cc in range(0,len(COASTNESS_LIST)-1):
	# Extract coast mask
	coastmask = COASTNESS[ COASTNESS_LIST[cc] ] & SUB('med')
	coasts_mask[coastmask == True] = cc+1;

# Initialize basins for each coast
basins_mask=np.zeros((jpk,jpj,jpi))
# for each sub basin
for ii in range(0,len(SUBlist)-1):
	# Extract subbasin mask
	submask = SUB(SUBlist[ii])
	basins_mask[submask == True] = ii+1; # assigning index ii to basin
	
# Save masks data into binary format
np.save('%s/%s/dims' % (args.inpath,args.res),np.array([jpk,jpj,jpi]))
np.save('%s/%s/basins_mask' % (args.inpath,args.res),basins_mask)
np.save('%s/%s/coasts_mask' % (args.inpath,args.res),coasts_mask)
np.save('%s/%s/nav_lev' % (args.inpath,args.res),nav_lev)
np.save('%s/%s/area' % (args.inpath,args.res),area)
np.save('%s/%s/volume' % (args.inpath,args.res),Volume)

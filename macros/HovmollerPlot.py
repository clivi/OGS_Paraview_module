#Macro for Hovmoller Plot
#
#Cosimo Livi, MHPC 2017

from paraview.simple import *
from vtk.util import numpy_support as npvtk
import numpy as np
paraview.simple._DisableFirstRenderCameraReset()

# get active view

# uncomment following to set a specific view size
# renderView1.ViewSize = [507, 337]

# get layout
layout1 = GetLayout()
# Split view
ActiveView = layout1.GetViewLocation(GetActiveView())
#layout1.SplitHorizontal(ActiveView, 0.5)


# set active view
#SetActiveView(None)
pythonView1 = GetActiveViewOrCreate('PythonView')
pythonView1.ViewSize = [498, 548]
pythonView1.Script = """


def setup_data(view):
	for i in range(view.GetNumberOfVisibleDataObjects()):
		dataObject = view.GetVisibleDataObjectForSetup(i)
		view.EnableAllAttributeArrays()
	

width  = 500;
height = 500;

def render(view,width,height):
	import math
	import vtk
	import numpy as np
	from paraview import python_view;
	from vtk.util import numpy_support as npvtk

	# Initialize PW python environment
	figure = python_view.matplotlib_figure(width,height);
	num = view.GetNumberOfVisibleDataObjects()
	computedVars = []
	# Main Loop over the tables
	k = 0
	for i in xrange(num):
		
		pdin = view.GetVisibleDataObjectForRendering(i)
		Table = vtk.vtkTable()
		Table.DeepCopy(pdin)



		# Compute plot name
		coastlist = Table.GetColumnByName("Coastness")
		sublist   = Table.GetColumnByName("Subbasins")
		name = ""
		counter = 0
		if sublist.GetValue(0)==1: name = str('alb');counter = counter + 1;
		if sublist.GetValue(1)==1: name = str('swm1');counter = counter + 1;
		if sublist.GetValue(2)==1: name = str('swm2');counter = counter + 1;
		if sublist.GetValue(3)==1: name = str('nwm');counter = counter + 1;
		if sublist.GetValue(4)==1: name = str('tyr1');counter = counter + 1;
		if sublist.GetValue(5)==1: name = str('tyr2');counter = counter + 1;
		if sublist.GetValue(6)==1: name = str('ion1');counter = counter + 1;
		if sublist.GetValue(7)==1: name = str('ion2');counter = counter + 1;
		if sublist.GetValue(8)==1: name = str('ion3');counter = counter + 1;
		if sublist.GetValue(9)==1: name = str('adr1');counter = counter + 1;
		if sublist.GetValue(10)==1: name = str('adr2');counter = counter + 1;
		if sublist.GetValue(11)==1: name = str('lev1');counter = counter + 1;
		if sublist.GetValue(12)==1: name = str('lev2');counter = counter + 1;
		if sublist.GetValue(13)==1: name = str('lev3');counter = counter + 1;
		if sublist.GetValue(14)==1: name = str('lev4');counter = counter + 1;
		if sublist.GetValue(15)==1: name = str('aeg');counter = counter + 1;
		if counter >= 2: name = "Select."+str(i+1)
		name = str(name)+", "
		coast_flag=0
		if coastlist.GetValue(0)==1: name = str(name)+"everywhere";coast_flag=0;
		if coastlist.GetValue(1)==1: name = str(name)+"coast";coast_flag=1;
		if coastlist.GetValue(2)==1: name = str(name)+"open";coast_flag=2;
		Table.RemoveColumnByName("Coastness")
		Table.RemoveColumnByName("Subbasins")






		#Compute number of timesteps
		YLabel = (npvtk.vtk_to_numpy(Table.GetColumnByName("depth (m)")))
		dateList = []
		M = []
		for z in range(Table.GetNumberOfColumns()):
			column = Table.GetColumn(z)
			if column.GetName() == "depth (m)"  or column.GetName() == "Coastness" or column.GetName() == "Subbasins": continue
			vName = column.GetName().rsplit(" ")[0]
			dateList.append(column.GetName().rsplit(" ")[2])
			statType = column.GetName().rsplit(" ")[1]

			M.append(npvtk.vtk_to_numpy(column))
		
		name=str(vName)+' '+str(name)+' '+str(statType)
		M = np.vstack(M).T
		tsteps = np.arange(1,len(dateList)+1)-0.5
		
		yTicksSp = 10

		ax = figure.add_subplot(1,num,i+1)
		ax.set_title(name)
		ax.set_yticks(np.arange(0,len(M[:,0]),yTicksSp))
		if i==0:
			ax.set_yticklabels(YLabel[::yTicksSp])
			ax.set_ylabel('depth (m)')
			cbarMax=abs(M.max())
		else:
			ax.set_yticklabels([])
		ax.set_xlabel('timestep')
		ax.set_xticks(tsteps)
		ax.set_xticklabels(dateList,rotation='vertical')
		if (len(dateList) > 10) and (len(dateList) <= 20): ax.set_xticks(ax.get_xticks()[::2])
		if (len(dateList) > 20) and (len(dateList) <= 30): ax.set_xticks(ax.get_xticks()[::3])
		if (len(dateList) > 30): ax.set_xticks(ax.get_xticks()[::10])


		im = ax.pcolormesh(M,cmap='jet')
		figure.colorbar(im)
		figure.gca().invert_yaxis()
		figure.tight_layout()

		computedVars.append(vName)
	return python_view.figure_to_image(figure);

""" 

#layout1.AssignView(ActiveView, pythonView1)


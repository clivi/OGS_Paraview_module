#Macro for Python plots

#

#Cosimo Livi, MHPC 2017



from paraview.simple import *

from vtk.util import numpy_support as npvtk

import numpy as np

paraview.simple._DisableFirstRenderCameraReset()





# get active view



# uncomment following to set a specific view size

# renderView1.ViewSize = [507, 337]



# get layout

layout1 = GetLayout()

# Split view

ActiveView = layout1.GetViewLocation(GetActiveView())

#layout1.SplitHorizontal(ActiveView, 0.5)





# set active view

#SetActiveView(None)

pythonView1 = GetActiveViewOrCreate('PythonView')

#pythonView1.ViewSize = [498, 548]

pythonView1.Script = """



import math

import vtk

import numpy as np

from paraview import python_view;

from vtk.util import numpy_support as npvtk

import datetime as dt



width  = 500;

height = 500;



def setup_data(view):

	for i in range(view.GetNumberOfVisibleDataObjects()):

		dataObject = view.GetVisibleDataObjectForSetup(i)

		view.EnableAllAttributeArrays()



def HovmollerPlot(figure,Table,i,num,computedVars):

	# Compute plot name

	coastlist = Table.GetFieldData().GetAbstractArray('Coast level:').GetValue(0)

	sublist   = Table.GetFieldData().GetAbstractArray('Sub-basins:').GetValue(0)

	name = ""

	if len(sublist.split(',')) > 2:
		name = "Select."+str(i+1)
	else:
		name = sublist
	name = str(name)+", "+ coastlist





	#Compute number of timesteps

	YLabel = (npvtk.vtk_to_numpy(Table.GetColumnByName("depth (m)")))

	dateList = []

	M = []

	for z in range(Table.GetNumberOfColumns()):

		column = Table.GetColumn(z)

		if column.GetName() == "depth (m)"  or column.GetName() == "Coastness" or column.GetName() == "Subbasins": continue

		vName = column.GetName().rsplit(" ")[0]

		dateList.append(column.GetName().rsplit(" ")[2])

		statType = column.GetName().rsplit(" ")[1]



		M.append(npvtk.vtk_to_numpy(column))

	

	name=str(vName)+' '+str(name)+' '+str(statType)

	M = np.vstack(M).T

	tsteps = np.arange(1,len(dateList)+1)-0.5

	

	yTicksSp = 10



	ax = figure.add_subplot(1,num,i+1)

	ax.set_title(name)

	ax.set_yticks(np.arange(0,len(M[:,0]),yTicksSp))

	if i==0:

		ax.set_yticklabels(YLabel[::yTicksSp])

		ax.set_ylabel('depth (m)')

		cbarMax=abs(M.max())

	else:

		ax.set_yticklabels([])

	ax.set_xlabel('timestep')

	ax.set_xticks(tsteps)

	ax.set_xticklabels(dateList,rotation='vertical')

	im = ax.pcolormesh(M,cmap='jet')

	figure.colorbar(im)

	figure.gca().invert_yaxis()

	figure.tight_layout()

	computedVars.append(vName)

	return figure



def VerticalProfilePlot(figure,Table,i,num,computedVars):

	#Compute number of variables for statistics

	computed_vars = []

	for z in range(Table.GetNumberOfColumns()):

		column = Table.GetColumn(z)

		if column.GetName() == "depth"  or column.GetName() == "Coastness" or column.GetName() == "Subbasins": continue

		vName = column.GetName().rsplit(" ")[0]

		if vName == 'Climat.': continue



		if len(computed_vars)==0: 

			computed_vars.append(vName)

		if not any(vName in element for element in computed_vars):

			computed_vars.append(vName)

	nvars = len(computed_vars)



	# Compute plot name

	if Table.GetFieldData().GetAbstractArray('Coast level:') is not None:
		coastlist = Table.GetFieldData().GetAbstractArray('Coast level:').GetValue(0)

		sublist   = Table.GetFieldData().GetAbstractArray('Sub-basins:').GetValue(0)

		if coastlist != None and sublist != None:

			name = ""

			if len(sublist.split(',')) > 2:
				name = "Select."+str(i+1)
			else:
				name = sublist
			name = str(name)+", "+ coastlist



	else: 

		name = "Select."+str(i+1)

	Y = npvtk.vtk_to_numpy(Table.GetColumnByName("depth"))







	# Search for climatology

	Do_climatology = False

	climat_vars = ['N1p','N3n','O2o','N5s','O3h', 'Ac','O3c', 'DIC' ]

	climate_list = [('N1p',[]), ('N3n',[]), ('O2o',[]),

					('N5s',[]), ('O3h',[]), ('Ac',[]),

					('O3c',[]), ('DIC',[])]

	for z in range(Table.GetNumberOfColumns()):

		

		column = Table.GetColumn(z)

		col_name = column.GetName()



		if col_name.rsplit(" ")[0]=="Climat.":

			Do_climatology = True

			type = col_name.rsplit(" ")[1]

			var  = col_name.rsplit(" ")[2]

			

			for element in climate_list:

				if element[0] != var: continue

				if type == 'Avg': element[1].append(npvtk.vtk_to_numpy(column))

				if type == 'Std': element[1].append(npvtk.vtk_to_numpy(column))



	# Loop over different variables for every table

	k = 0

	for var in computed_vars:	

		k = k+1

		

		var_name = str(var)+", "+str(name)



		for z in range(Table.GetNumberOfColumns()):

			column = Table.GetColumn(z)

			vName = column.GetName().rsplit(" ")[0]

			if vName == var:		

				

				legend=""

				legend_list = column.GetName().rsplit(" ")

				for ii in range(1,len(legend_list)):

					legend=str(legend)+" "+str(legend_list[ii])

				if legend_list[1] == 'Avg'   : col='red'

				if legend_list[1] == 'Std'   : col='darkgray'

				if legend_list[1] == 'Minima': col='darkorange'

				if legend_list[1] == '0.05'  : col='blue'

				if legend_list[1] == '0.25'  : col='mediumpurple'

				if legend_list[1] == '0.5'   : col='darkorchid'

				if legend_list[1] == '0.75'  : col='fuchsia'

				if legend_list[1] == '0.95'  : col='gold'

				if legend_list[1] == 'Maxima': col='darkgreen'

				flag=0

				if legend_list[1] == 'Std':

					try:

						Avg = npvtk.vtk_to_numpy(Table.GetColumnByName(var+" Avg"))

					except:

						flag = 1

					



				X = npvtk.vtk_to_numpy(column)

				X[X==0]=np.nan

				if nvars == 1:				



					if num <= 4:  ax = figure.add_subplot(1,num,i+1)
					if num >  4:  ax = figure.add_subplot(math.ceil(num/2.),2,i+1)

					if legend_list[1] == 'Std' and flag==0:

						ax.plot(Avg-X,Y,linewidth='1.5',color=col,label = legend)

						ax.plot(Avg+X,Y,linewidth='1.5',color=col)

					else:

						ax.plot(X,Y,linewidth='1.5',color=col,label = legend)



					ax.set_title(var_name)
		

			

					if i % 4 == 0:	ax.set_ylabel('depth (m)')

					

				

				

				elif nvars > 1:

					ax = figure.add_subplot(num,nvars,k+i*nvars)

					if legend_list[1] == 'Std' and flag==0:

						ax.plot(Avg-X,Y,linewidth='1.5',color=col,label = legend)

						ax.plot(Avg+X,Y,linewidth='1.5',color=col)

					else:

						ax.plot(X,Y,linewidth='1.5',color=col,label = legend)

					ax.set_title(var_name)

					

					



					if k < nvars and num == 1: 

						ax.set_ylabel('depth (m)')

					else:

						ax.set_yticklabels([])

					

				ax.set_xlabel('Conc')	



				

		if Do_climatology:

			for element in climate_list:

				if element[0] == var:

					ax.errorbar(element[1][0],Y,np.zeros(len(Y)),np.array(element[1][1]),fmt='ro',label= "Climat.")

		

		# Handles Legend

		ax.legend(loc=3,ncol = 2)

		

		# Set Plot lim

		#ax.set_ 

		#ax.set_ylim([])

		#ax.set_xlim([])



		figure.tight_layout()

	return figure

	

	

def TimeStaticsPlot(figure,Table,i,num,computedVars):

	#Compute number of variables for statistics

	computed_vars = []

	for z in range(Table.GetNumberOfColumns()):

		column = Table.GetColumn(z)

		vName = column.GetName().rsplit(" ")[0]

		if vName == "Process"  or column.GetName() == "Row" or column.GetName() == "Dates": continue

		if len(computed_vars)==0: 

			computed_vars.append(vName)

		if not any(element in vName for element in computed_vars):

			computed_vars.append(vName)

	nvars = len(computed_vars)



	#Compute number of timesteps

	datelist=[]

	for z in range(Table.GetNumberOfColumns()):

		column     = Table.GetColumn(z)

		columnName = Table.GetColumn(z).GetName()

		print columnName

		print column

		if columnName == 'Dates':

		    print column

		    for q in range(Table.GetNumberOfRows()):

		        datelist.append(column.GetValue(q))

	Xdatelist = [dt.datetime.strptime(date, '%Y%m%d-%H:%M:%S').date() for date in datelist]  	

				

	# Loop over different variables for every table

	k = 0



	for var in computed_vars:	

		k = k+1

		coastness = Table.GetFieldData().GetAbstractArray('Coast level').GetValue(0)
		basins = Table.GetFieldData().GetAbstractArray('Sub-basins').GetValue(0)

		var_name = str(var) 
		if len(basins.split(','))<=4:
			name = var_name+" "+coastness+" "+basins
		else:
			name = var_name+" "+coastness+" Select. 1" 

		#+", "+str(name)



		for z in range(Table.GetNumberOfColumns()):

			column = Table.GetColumn(z)

			vName = column.GetName().rsplit(" ")[0]

			if vName == var:

				

 				legend=""

				legend_list = column.GetName().rsplit(" ")

				for ii in range(1,len(legend_list)):

					legend=str(legend)+" "+str(legend_list[ii])

				if legend_list[1] == 'Avg'   : col='red'

				if legend_list[1] == 'std'   : col='darkgray'

				if legend_list[1] == 'Minima': col='darkorange'

				if legend_list[1] == '0.05wp': col='blue'

				if legend_list[1] == '0.25wp': col='mediumpurple'

				if legend_list[1] == '0.5wp' : col='darkorchid'

				if legend_list[1] == '0.75wp': col='fuchsia'

				if legend_list[1] == '0.95wp': col='gold'

				if legend_list[1] == 'Maxima': col='darkgreen'

				flag=0

				if legend_list[1] == 'std':

					try:

						Avg = npvtk.vtk_to_numpy(Table.GetColumnByName(var+" Avg"))

					except:

						flag = 1	

				Y = npvtk.vtk_to_numpy(column)

				Y[Y==0]=np.nan

				if nvars == 1:				

					if num <= 4:  ax = figure.add_subplot(1,num,i+1)

					if num >  4:  ax = figure.add_subplot(math.ceil(num/2.),2,i+1)

					if legend_list[1] == 'std' and flag==0:

						ax.plot(Xdatelist,Avg-Y,linewidth='1.5',color=col,label = legend)

						ax.plot(Xdatelist,Avg+Y,linewidth='1.5',color=col)

					else:

						ax.plot(Xdatelist,Y,linewidth='1.5',color=col,label = legend)

					ax.set_title(name)

					ax.set_xlabel('Time')	

		            # Handles Legend

					ax.legend(loc=3,ncol = 2)

					

				elif nvars > 1:

					ax = figure.add_subplot(num,nvars,k)

					if legend_list[1] == 'Std' and flag==0:

						ax.plot(Xdatelist,Y,linewidth='1.5',color=col,label = legend)

						ax.plot(Xdatelist,Y,linewidth='1.5',color=col)

					else:

						ax.plot(Xdatelist,Y,linewidth='1.5',color=col,label = legend)

					ax.set_title(var_name)

		        

		figure.tight_layout()

	return figure	

	

def render(view,width,height):



	# Initialize PW python environment

	figure = python_view.matplotlib_figure(width,height);

	num = view.GetNumberOfVisibleDataObjects()

	computedVars = []

	# Main Loop over the tables

	for i in xrange(num):

		

		pdin = view.GetVisibleDataObjectForRendering(i)

		Table = vtk.vtkTable()

		Table.DeepCopy(pdin)

		plot_type = Table.GetFieldData().GetAbstractArray('Plot Type').GetValue(0)



		if plot_type == 'Hovmoller':

			figure = HovmollerPlot(figure,Table,i,num,computedVars)



		if plot_type == 'Statistic':

			figure = VerticalProfilePlot(figure,Table,i,num,computedVars)



		if plot_type == 'TimeStatistics':

			figure = TimeStaticsPlot(figure,Table,i,num,computedVars)

	return python_view.figure_to_image(figure);



""" 



#layout1.AssignView(ActiveView, pythonView1)






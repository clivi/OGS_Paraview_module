# Paraview Macro for plotting with matplotlib

# Cosimo Livi, MHPC 2017

from paraview.simple import *
from vtk.util import numpy_support as npvtk
import numpy as np
paraview.simple._DisableFirstRenderCameraReset()

# get active view

# uncomment following to set a specific view size
# renderView1.ViewSize = [507, 337]

# get layout
layout1 = GetLayout()
# Split view
#layout1.SplitVertical(0, 0.5)
ActiveView = layout1.GetViewLocation(GetActiveView())


# set active view
#SetActiveView(None)
pythonView2 = GetActiveViewOrCreate('PythonView')
#pythonView2.ViewSize = [507, 153]
pythonView2.Script = """


def setup_data(view):
	for i in range(view.GetNumberOfVisibleDataObjects()):
		dataObject = view.GetVisibleDataObjectForSetup(i)
		view.EnableAllAttributeArrays()
	

width  = 500;
height = 500;

def render(view,width,height):
	import math
	import vtk
	from paraview import python_view;
	from vtk.util import numpy_support as npvtk
	
	# Initialize PW python environment
	figure = python_view.matplotlib_figure(width,height);
	num = view.GetNumberOfVisibleDataObjects()
	k=0

	# Main Loop over the tables
	for i in xrange(num):
		
		pdin = view.GetVisibleDataObjectForRendering(i)
		Table = vtk.vtkTable()
		Table.DeepCopy(pdin)

		#Compute number of variables for statistics
		computed_vars = []
		for z in range(Table.GetNumberOfColumns()):
			column = Table.GetColumn(z)
			if column.GetName() == "depth"  or column.GetName() == "Coastness" or column.GetName() == "Subbasins": continue
			vName = column.GetName().rsplit(" ")[0]
			if len(computed_vars)==0: 
				computed_vars.append(vName)
			if not any(element in vName for element in computed_vars):
				computed_vars.append(vName)
		nvars = len(computed_vars)

		# Compute plot name
		coastlist = Table.GetColumnByName("Coastness")
		sublist   = Table.GetColumnByName("Subbasins")
		name = ""
		counter = 0
		if sublist.GetValue(0)==1: name = str('alb');counter = counter + 1;
		if sublist.GetValue(1)==1: name = str('swm1');counter = counter + 1;
		if sublist.GetValue(2)==1: name = str('swm2');counter = counter + 1;
		if sublist.GetValue(3)==1: name = str('nwm');counter = counter + 1;
		if sublist.GetValue(4)==1: name = str('tyr1');counter = counter + 1;
		if sublist.GetValue(5)==1: name = str('tyr2');counter = counter + 1;
		if sublist.GetValue(6)==1: name = str('ion1');counter = counter + 1;
		if sublist.GetValue(7)==1: name = str('ion2');counter = counter + 1;
		if sublist.GetValue(8)==1: name = str('ion3');counter = counter + 1;
		if sublist.GetValue(9)==1: name = str('adr1');counter = counter + 1;
		if sublist.GetValue(10)==1: name = str('adr2');counter = counter + 1;
		if sublist.GetValue(11)==1: name = str('lev1');counter = counter + 1;
		if sublist.GetValue(12)==1: name = str('lev2');counter = counter + 1;
		if sublist.GetValue(13)==1: name = str('lev3');counter = counter + 1;
		if sublist.GetValue(14)==1: name = str('lev4');counter = counter + 1;
		if sublist.GetValue(15)==1: name = str('aeg');counter = counter + 1;
		if counter >= 2: name = "Select."+str(i+1)
		name = str(name)+", "
		coast_flag=0
		if coastlist.GetValue(0)==1: name = str(name)+"everywhere";coast_flag=0;
		if coastlist.GetValue(1)==1: name = str(name)+"coast";coast_flag=1;
		if coastlist.GetValue(2)==1: name = str(name)+"open_sea";coast_flag=2;
		Table.RemoveColumnByName("Coastness")
		Table.RemoveColumnByName("Subbasins")
		Y = npvtk.vtk_to_numpy(Table.GetColumnByName("depth"))
		
		# Loop over different variables for every table
		for var in computed_vars:	
			k = k+1
			
			var_name = str(var)+", "+str(name)
			for z in range(Table.GetNumberOfColumns()):
				column = Table.GetColumn(z)
				vName = column.GetName().rsplit(" ")[0]
				if vName == var:
					
					
					legend=""
					legend_list = column.GetName().rsplit(" ")
					for ii in range(1,len(legend_list)):
						legend=str(legend)+" "+str(legend_list[ii])
					
					X = npvtk.vtk_to_numpy(column)
					# Set the plot type and style
					if nvars == 1:
						if num <= 4:  ax = figure.add_subplot(1,num,i+1)
						if num >  4:  ax = figure.add_subplot(math.ceil(num/4.),4,i+1)
						ax.plot(X,Y,linewidth='1.5',label = legend)
						ax.set_title(name)
						labels = ax.get_yticks()/1000.
						if i % 4 ==0: 
							ax.set_yticklabels(labels)
							ax.set_ylabel('depth (m)')
						if i % 4 != 0:
							ax.set_yticklabels([])
						if i==num-1 and num <= 4: ax.legend(loc='lower center',ncol=4)
						if i==4 and num >  4: ax.legend(loc='lower center')
						if coast_flag == 1: ax.set_ylim([-250000,0])
					elif nvars > 1:
						ax = figure.add_subplot(num,nvars,k)
						ax.plot(X,Y,linewidth='1.5',label = legend)
						if num <= 2: ax.set_title(var_name)
						if num > 2: ax.set_title(var_name,y=0.95)
						labels = ax.get_yticks()/1000.
						if k % nvars !=0: 
							ax.set_yticklabels(labels)
							ax.set_ylabel('depth (m)')
						if k % nvars == 0:
							ax.set_yticklabels(labels)
						if k % nvars == 0 and k <= nvars:
							ax.legend(loc=4)
						
						if coast_flag == 1: ax.set_ylim([-250000,0])
					# Custom ranges: uncomment following lines for modify plot ranges (remember y axis is in mm)
					#ax.set_ylim([-100000,0])
					#ax.set_xlim([0,10])
					figure.tight_layout()

	return python_view.figure_to_image(figure);

""" 

#layout1.AssignView(2, pythonView2)


# Paraview Macro for plotting with matplotlib

# Cosimo Livi, MHPC 2017
from paraview.simple import *
from vtk.util import numpy_support as npvtk
import numpy as np
paraview.simple._DisableFirstRenderCameraReset()

# get active view

# uncomment following to set a specific view size
# renderView1.ViewSize = [507, 337]

# get layout
layout1 = GetLayout()
# Split view
#layout1.SplitHorizontal(0, 0.5)

ActiveView = layout1.GetViewLocation(GetActiveView())


# set active view
#SetActiveView(None)
pythonView3 = GetActiveViewOrCreate('PythonView')
#pythonView3.ViewSize = [498, 548]
pythonView3.Script = """


def setup_data(view):
	for i in range(view.GetNumberOfVisibleDataObjects()):
		dataObject = view.GetVisibleDataObjectForSetup(i)
		view.EnableAllAttributeArrays()
	

width  = 500;
height = 500;

def render(view,width,height):
	import math
	import vtk
	import numpy as np
	from paraview import python_view;
	from vtk.util import numpy_support as npvtk

	# Initialize PW python environment
	figure = python_view.matplotlib_figure(width,height);
	num = view.GetNumberOfVisibleDataObjects()

	# Main Loop over the tables
	k = 0
	for i in xrange(num):
		
		pdin = view.GetVisibleDataObjectForRendering(i)
		Table = vtk.vtkTable()
		Table.DeepCopy(pdin)

		#Compute number of variables for statistics
		computed_vars = []
		for z in range(Table.GetNumberOfColumns()):
			column = Table.GetColumn(z)
			if column.GetName() == "depth"  or column.GetName() == "Coastness" or column.GetName() == "Subbasins": continue
			vName = column.GetName().rsplit(" ")[0]
			if vName == 'Climat.': continue

			if len(computed_vars)==0: 
				computed_vars.append(vName)
			if not any(element in vName for element in computed_vars):
				computed_vars.append(vName)
		nvars = len(computed_vars)

		# Compute plot name
		coastlist = Table.GetColumnByName("Coastness")
		sublist   = Table.GetColumnByName("Subbasins")
		if coastlist != None and sublist != None:
			name = ""
			counter = 0
			if sublist.GetValue(0)==1: name = str('alb');counter = counter + 1;
			if sublist.GetValue(1)==1: name = str('swm1');counter = counter + 1;
			if sublist.GetValue(2)==1: name = str('swm2');counter = counter + 1;
			if sublist.GetValue(3)==1: name = str('nwm');counter = counter + 1;
			if sublist.GetValue(4)==1: name = str('tyr1');counter = counter + 1;
			if sublist.GetValue(5)==1: name = str('tyr2');counter = counter + 1;
			if sublist.GetValue(6)==1: name = str('ion1');counter = counter + 1;
			if sublist.GetValue(7)==1: name = str('ion2');counter = counter + 1;
			if sublist.GetValue(8)==1: name = str('ion3');counter = counter + 1;
			if sublist.GetValue(9)==1: name = str('adr1');counter = counter + 1;
			if sublist.GetValue(10)==1: name = str('adr2');counter = counter + 1;
			if sublist.GetValue(11)==1: name = str('lev1');counter = counter + 1;
			if sublist.GetValue(12)==1: name = str('lev2');counter = counter + 1;
			if sublist.GetValue(13)==1: name = str('lev3');counter = counter + 1;
			if sublist.GetValue(14)==1: name = str('lev4');counter = counter + 1;
			if sublist.GetValue(15)==1: name = str('aeg');counter = counter + 1;
			if counter >= 2: name = "Select."+str(i+1)
			name = str(name)+", "
			coast_flag=0
			if coastlist.GetValue(0)==1: name = str(name)+"everywhere";coast_flag=0;
			if coastlist.GetValue(1)==1: name = str(name)+"coast";coast_flag=1;
			if coastlist.GetValue(2)==1: name = str(name)+"open";coast_flag=2;
			Table.RemoveColumnByName("Coastness")
			Table.RemoveColumnByName("Subbasins")
		else: 
			name = "Select."+str(i+1)
		Y = npvtk.vtk_to_numpy(Table.GetColumnByName("depth"))



		# Search for climatology
		Do_climatology = False
		climat_vars = ['N1p','N3n','O2o','N5s','O3h', 'Ac','O3c', 'DIC' ]
		climate_list = [('N1p',[]), ('N3n',[]), ('O2o',[]),
						('N5s',[]), ('O3h',[]), ('Ac',[]),
						('O3c',[]), ('DIC',[])]
		for z in range(Table.GetNumberOfColumns()):
			
			column = Table.GetColumn(z)
			col_name = column.GetName()

			if col_name.rsplit(" ")[0]=="Climat.":
				Do_climatology = True
				type = col_name.rsplit(" ")[1]
				var  = col_name.rsplit(" ")[2]
				
				for element in climate_list:
					if element[0] != var: continue
					if type == 'Avg': element[1].append(npvtk.vtk_to_numpy(column))
					if type == 'Std': element[1].append(npvtk.vtk_to_numpy(column))

		# Loop over different variables for every table
		for var in computed_vars:	
			k = k+1
			
			var_name = str(var)+", "+str(name)

			for z in range(Table.GetNumberOfColumns()):
				column = Table.GetColumn(z)
				vName = column.GetName().rsplit(" ")[0]
				if vName == var:		
					
					legend=""
					legend_list = column.GetName().rsplit(" ")
					for ii in range(1,len(legend_list)):
						legend=str(legend)+" "+str(legend_list[ii])
					if legend_list[1] == 'Avg'   : col='red'
					if legend_list[1] == 'Std'   : col='darkgray'
					if legend_list[1] == 'Minima': col='darkorange'
					if legend_list[1] == '0.05'  : col='blue'
					if legend_list[1] == '0.25'  : col='mediumpurple'
					if legend_list[1] == '0.5'   : col='darkorchid'
					if legend_list[1] == '0.75'  : col='fuchsia'
					if legend_list[1] == '0.95'  : col='gold'
					if legend_list[1] == 'Maxima': col='darkgreen'
					flag=0
					if legend_list[1] == 'Std':
						try:
							Avg = npvtk.vtk_to_numpy(Table.GetColumnByName(var+" Avg"))
						except:
							flag = 1
						

					X = npvtk.vtk_to_numpy(column)
					X[X==0]=np.nan
					if nvars == 1:				

						if num <= 4:  ax = figure.add_subplot(1,num,i+1)
						if num >  4:  ax = figure.add_subplot(math.ceil(num/2.),2,i+1)
						if legend_list[1] == 'Std' and flag==0:
							ax.plot(Avg-X,Y,linewidth='1.5',color=col,label = legend)
							ax.plot(Avg+X,Y,linewidth='1.5',color=col)
						else:
							ax.plot(X,Y,linewidth='1.5',color=col,label = legend)

						ax.set_title(var_name)
						if coastlist != None: 
							if coast_flag == 1:	ax.set_ylim([-250,0])
						
				
						if i % 4 == 0:	ax.set_ylabel('depth (m)')
						
					
					
					elif nvars > 1:
						ax = figure.add_subplot(num,nvars,k)
						if legend_list[1] == 'Std' and flag==0:
							ax.plot(Avg-X,Y,linewidth='1.5',color=col,label = legend)
							ax.plot(Avg+X,Y,linewidth='1.5',color=col)
						else:
							ax.plot(X,Y,linewidth='1.5',color=col,label = legend)
						ax.set_title(var_name)
						
						
						if coastlist != None: 
							if coast_flag == 1:	ax.set_ylim([-250,0])
						if k < nvars and num == 1: 
							ax.set_ylabel('depth (m)')
						else:
							ax.set_yticklabels([])
						
					ax.set_xlabel('Conc')	

					
			if Do_climatology:
				for element in climate_list:
					if element[0] == var:
						ax.errorbar(element[1][0],Y,np.zeros(len(Y)),np.array(element[1][1]),fmt='ro',label= "Climat.")
			
			# Handles Legend
			ax.legend(loc=3,ncol = 2)
			
			# Set Plot lim 
			#ax.set_ylim([])
			#ax.set_xlim([])

			figure.tight_layout()
	return python_view.figure_to_image(figure);

""" 

layout1.AssignView(2, pythonView3)

#bbox_to_anchor=(0.3+0.1*num**2,-0.05)
#bbox_to_anchor=(1./(num**2+1)
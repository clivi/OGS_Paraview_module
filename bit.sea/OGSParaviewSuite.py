# Class for OGS visualization with Paraview
#
# Cosimo Livi, MHPX 2017


import vtk
import numpy as np 
import os
import netCDF4 as NC4
from vtk.util import numpy_support as npvtk
from ctypes import *
from numpy.ctypeslib import ndpointer


class OGSvtk(object):
	def __init__(self, maskpath):
			# Variable required for vtk version check
			self.vtkVersion = int(os.path.basename(vtk.vtkVersion.GetVTKVersion()).rsplit(".")[0])

			self.maskpath   = maskpath	
			self.resolution = 'undefined'
			self.jpk = 0
			self.jpj = 0
			self.jpi = 0
			self.LoadNetCDF = 0


	def createVTKscaf(self,varname,data,type=vtk.VTK_FLOAT):
		'''
		Creates a VTK scalar field array from variable data
		with a specific variable name.

		Inputs:
			varname: VTK variable name.
			data: numpy array containing variable data.
			jpk: z dimension max.
			jpj: y dimension max.
			jpi: x dimension max.
			type (optional): VTK data type

		Outputs:
			scaf: VTK scalar field.
		'''
		# Create vtk array from numpy data
		jpk = self.jpk 
		jpj = self.jpj 
		jpi = self.jpi
		scaf = npvtk.numpy_to_vtk(data[:(jpk-1),:(jpj-1),:(jpi-1)].ravel(),
			                      True,type);
		# Set variable name
		scaf.SetName(varname);

		# Return scalar field
		return scaf
	def createVTKvecf3(self,varname,xdata,ydata,zdata,type=vtk.VTK_FLOAT):
			'''
			Creates a VTK 3D vector field array from variable data
			with a specific variable name.

			Inputs:
				varname: VTK variable name.
				xdata: numpy array containing variable data of 1st dimension.
				ydata: numpy array containing variable data of 2nd dimension.
				zdata: numpy array containing variable data of 3rd dimension.

			Outputs:
				vecf: VTK vector field.
			'''
			# Concatenate 3 dimensional vector
			data = np.concatenate((xdata[:(self.jpk-1),:(self.jpj-1),:(self.jpi-1),np.newaxis],
							   	   ydata[:(self.jpk-1),:(self.jpj-1),:(self.jpi-1),np.newaxis],
							   	   zdata[:(self.jpk-1),:(self.jpj-1),:(self.jpi-1),np.newaxis]),
							  	  axis=3);
			# Create vtk array from numpy data
			vecf = npvtk.numpy_to_vtk(data.ravel(),True,type);
			# Set variable name and number of components
			vecf.SetName(varname);
			vecf.SetNumberOfComponents(3);
			vecf.SetNumberOfTuples( (self.jpi-1)*(self.jpj-1)*(self.jpk-1) );
			# Return scalar field
			return vecf

	def createVTKstrf(self,varname,data):
		'''
		Creates a VTK string field array from variable data
		with a specific variable name.

		Inputs:
				varname: VTK variable name.
		data: string containing variable data.

		Outputs:
			strf: VTK string field.  
		'''
		# Create string array
		strf = vtk.vtkStringArray();
		# Set properties
		strf.SetName(varname);
		strf.SetNumberOfTuples(1);
		# Set value
		strf.SetValue(0,data);
		# Return
		return strf;

	def SetResolution(self,resolution):
		# Routine for creating a vtkRectilienarGrid with the required dimension accordingly to the desired resolution
		self.resolution = resolution

		if   self.resolution == 'low':
			# Load Conversion from Lon Lat and Depth into meters
			self.Lon2Meters =         np.load(str(self.maskpath)+'/low/Lon2Meters.npy')    # x conversion
			self.Lat2Meters =         np.load(str(self.maskpath)+'/low/Lat2Meters.npy')	  # y conversion
			self.nav_lev    = -1000 * np.load(str(self.maskpath)+'/low/nav_lev.npy')		  # z conversion
			# Load coordinates
			self.jpk        = np.load(str(self.maskpath)+'/low/dims.npy')[0]     # z coordinate 
			self.jpj        = np.load(str(self.maskpath)+'/low/dims.npy')[1]     # y coordinate
			self.jpi        = np.load(str(self.maskpath)+'/low/dims.npy')[2]     # x coordinate
		elif self.resolution == 'mid':
			# Load Conversion from Lon Lat and Depth into meters
			self.Lon2Meters =         np.load(str(self.maskpath)+'/mid/Lon2Meters.npy')    # x conversion
			self.Lat2Meters =         np.load(str(self.maskpath)+'/mid/Lat2Meters.npy')	  # y conversion
			self.nav_lev    = -1000 * np.load(str(self.maskpath)+'/mid/nav_lev.npy')		  # z conversion
			# Load coordinates
			self.jpk        = np.load(str(self.maskpath)+'/mid/dims.npy')[0]     # z coordinate 
			self.jpj        = np.load(str(self.maskpath)+'/mid/dims.npy')[1]     # y coordinate
			self.jpi        = np.load(str(self.maskpath)+'/mid/dims.npy')[2]     # x coordinate
		elif self.resolution == 'high':
			# Load Conversion from Lon Lat and Depth into meters
			self.Lon2Meters =         np.load(str(self.maskpath)+'/high/Lon2Meters.npy')    # x conversion
			self.Lat2Meters =         np.load(str(self.maskpath)+'/high/Lat2Meters.npy')	   # y conversion
			self.nav_lev    = -1000 * np.load(str(self.maskpath)+'/high/nav_lev.npy')	   # z conversion
			# Load coordinates
			self.jpk        = np.load(str(self.maskpath)+'/high/dims.npy')[0]     # z coordinate 
			self.jpj        = np.load(str(self.maskpath)+'/high/dims.npy')[1]     # y coordinate
			self.jpi        = np.load(str(self.maskpath)+'/high/dims.npy')[2]     # x coordinate
		else:
			raise ValueError('ERROR: Resolution must be "low", "mid" or "high" ')
		#Initialize reader with the desired resolution
		self.LoadNetCDF = LoadNetCDF(self.jpk,self.jpj,self.jpi)

	def CreateRectilinearGrid(self):
		if self.resolution == 'undefined':
			raise ValueError('ERROR, resolution not set. Please run SetResolution method')
		# Create VTK arrays
		daLon = npvtk.numpy_to_vtk(self.Lon2Meters.ravel(),True,vtk.VTK_FLOAT)
		daLat = npvtk.numpy_to_vtk(self.Lat2Meters.ravel(),True,vtk.VTK_FLOAT)
		daDep = npvtk.numpy_to_vtk(self.nav_lev .ravel(),True,vtk.VTK_FLOAT)

		# Create a vtk grid
		rg = vtk.vtkRectilinearGrid()
		rg.SetDimensions(self.jpi,self.jpj,self.jpk)

		# Set the coordinates into the rectilinear grid
		rg.SetXCoordinates(daLon)
		rg.SetYCoordinates(daLat)
		rg.SetZCoordinates(daDep)

		# Set Meta Data arrays

		rg.GetFieldData().AddArray(self.createVTKstrf("path_to_mesh","%s" % str(self.maskpath)))
		rg.GetFieldData().AddArray(self.createVTKstrf("resolution","%s" % str(self.resolution)))


		# Return the rectilinear grid
		return rg;
	#Routine for saving on disk a vtk unstructured or rectilinear grid  

	def WriteGridToVtk(self,inp,savename):
		# This is if inp is a vtkRectilinearGrid
		if (inp.IsA('vtkRectilinearGrid')):
			output = vtk.vtkXMLRectilinearGridWriter()
		# This is for vtkUnstructuredGrid
		elif (inp.IsA('vtkUnstructuredGrid')):
			output = vtk.vtkXMLUnstructuredGridWriter()
		else:
			raise ValueError('ERROR: WriteGridToVtk input must be either a vtkRectilinearGrid or a vtkUnstructuredGrid')	
		
		# Write the grid to the desired file
		output.SetFileName(savename)
		if (self.vtkVersion <= 5):
			output.SetInput(inp)
		else:
			output.SetInputData(inp)

		output.Write()


	def AddMask(self, input, AddSubMask = True, AddCoastMask = True, AddDepth = True):
		if self.resolution == 'undefined': raise ValueError('ERROR: resolution undefined. Use method CreateRectilinearGrid to set it.')
		
		if AddSubMask: # Add basins mask to previously created rectilinear grid
			sub_mask = np.load(str(self.maskpath)+'/'+str(self.resolution)+'/basins_mask.npy')
			input.GetCellData().AddArray( self.createVTKscaf('basins mask', sub_mask,))

		if AddCoastMask: # Add coasts mask to previously created rectilinear grid
			coasts_mask = np.load(str(self.maskpath)+'/'+str(self.resolution)+'/coasts_mask.npy')
			input.GetCellData().AddArray( self.createVTKscaf('coasts mask', coasts_mask))

		if AddDepth:
			depth = np.zeros((self.jpk,self.jpj,self.jpi))
			for z in range(len(self.nav_lev)):
				depth[z,:,:] = self.nav_lev[z]
			input.GetCellData().AddArray( self.createVTKscaf('depth', depth))

	def LoadVolume(self):
		if self.resolution == 'undefined':
			raise ValueError('Error: Resolution must be set. Use SetResolution method')

		return np.load(self.maskpath+'/'+self.resolution+'/volume.npy')

	def LoadArea(self):
		if self.resolution == 'undefined':
			raise ValueError('Error: Resolution must be set. Use SetResolution method')

		return np.load(self.maskpath+'/'+self.resolution+'/area.npy')


class LoadNetCDF(object):

	def __init__(self,jpk,jpj,jpi):
		self.jpk = jpk 
		self.jpj = jpj 
		self.jpi = jpi

	def GetVariable(self,filename,varname,maxval=1e20):
		'''
		Reads information inside a NetCDF file and returns a parsed
		numpy array with 0. when val > maxval. Also cuts the file 
		according to jpk, jpj and jpi.

		If the file cannot be read, the function aborts.

		Inputs:
			filename: full path to netcdf4 file
			varname:  name of the variable to extract
			jpk: z dimension max
			jpj: y dimension max
			jpi: x dimension max
			maxval: value from which start to conver to 0. [default = 1e20]

		Outputs:
			data: numpy array with variable data
		'''
		# Check if file exists
		jpk = self.jpk
		jpj = self.jpj
		jpi = self.jpi
		if not os.path.isfile(filename):
			raise ValueError("Cannot open file <%s>!" % filename);
		# Read raw data from NetCDF
		data_raw = NC4.Dataset(filename,"r");
		data_raw.set_auto_mask(False); # Disable masked array behaviour
		data     = data_raw.variables[varname][0,:jpk,:,:];
		data_raw.close(); # Close file

		# Now we will check if the data needs to be cut
		dpk, dpj, dpi = data.shape; # Get the array's dimensions

		# Compute the difference
		diffk = abs(dpk-jpk); # Always must be zero since it is cut before
		if (diffk != 0): raise ValueError("something went wrong!");
		diffj = abs(dpj-jpj);
		diffi = abs(dpi-jpi);
		if diffj > 1 or diffi>1: raise ValueError("ERROR: wrong resolution")

		# Cut the data
		data = data[:,diffj:,diffi:];

		# Fix for missing values
		data[ data >= maxval ] = 0.;

		# Return processed data
		return data;

	def faces2cellcenter(self,U,V,W):
		'''
		Convers a face centered velocity into a cell centered field.
		Useful for further use on VTK grids.

		Inputs:
			U: 1st component of the velocity (face centered)
			V: 2nd component of the velocity (face centered)
			W: 3rd component of the velocity (face centered)

		Outputs:
			U: 1st component of the velocity (cell centered)
			V: 2nd component of the velocity (cell centered)
			W: 3rd component of the velocity (cell centered)

		'''
		# Start interpolation
		aux = np.zeros([self.jpk,self.jpj,1])*np.NaN; Up1  = np.append(U,aux,2);
		aux = np.zeros([self.jpk,1,self.jpi])*np.NaN; Vp1  = np.append(V,aux,1);
		aux = np.zeros([1,self.jpj,self.jpi])*np.NaN; Wp1  = np.append(W,aux,0);

		# Copy and roll
		Up1r = Up1.copy(); Up1r = np.roll(Up1r,1,axis=2);  Up1r[:,:,0]=np.NaN;
		Vp1r = Vp1.copy(); Vp1r = np.roll(Vp1r,1,axis=1);  Vp1r[:,0,:]=np.NaN;
		Wp1r = Wp1.copy(); Wp1r = np.roll(Wp1r,-1,axis=0); Wp1r[self.jpk-1,:,:]=np.NaN;

		# Fixed correct syntaxis using nanmean from numpy instead of scipy
		with warnings.catch_warnings():
			# We anticipate catching the warnings to to averaging a row of only NaN
			warnings.simplefilter("ignore", category=RuntimeWarning);
			Uint = np.nanmean( np.array([Up1r,Up1]),axis=0);
			Vint = np.nanmean( np.array([Vp1r,Vp1]),axis=0);
			Wint = np.nanmean( np.array([Wp1r,Wp1]),axis=0);

		# Return to original dimensions
		Uint = Uint[:self.jpk,:self.jpj,:self.jpi];
		Vint = Vint[:self.jpk,:self.jpj,:self.jpi];
		Wint = Wint[:self.jpk,:self.jpj,:self.jpi];
	
		# Return value
		return Uint, Vint, Wint;

	# Load all the variable for a defined date string and stores into a python dictionary
	def GetAllVariablesAtDate(self,pathtodata,date):
		res = []
		files = os.listdir(pathtodata)
		sep = "."
		for filename in files:
			prefix,datestr,varname,_ = os.path.basename(filename).rsplit(sep)
			if datestr == date:
				key  = varname
				print key

				data = self.GetVariable(pathtodata+'/'+filename,key)
				
				res.append([key,data])
		return res

    # Same as GetAllVariablesAtDate but selects only variables in varlist
	def GetListVariablesAtDate(self,pathtodata,date,varlist):
		res = []
		files = os.listdir(pathtodata)
		sep = "."
		for filename in files:
			prefix, datestr, varname, _ = os.path.basename(filename).rsplit(sep)
			for variable in varlist:
				if (datestr == date) and (varname == variable):
					key  = varname
					data = self.GetVariable(pathtodata+'/'+filename,key)
					res.append([key,data])
		return res

class OGSStatistics(object):

	def __init__(self,librarypath,ActivateOMP = False):
		self.clibrarypath = librarypath
		# Initialization of C library for statistcs
		if (ActivateOMP):
			self.dso = CDLL(str(self.clibrarypath)+"/libomp.so")
		else:
			self.dso = CDLL(str(self.clibrarypath)+"/lib.so")

		# Function for evaluating areal averages at every nav_lev(depth) level
		self.dso.Areal_Statistics_C.argtypes = [c_long,ndpointer(c_double, flags = "C_CONTIGUOUS"),c_int, ndpointer(c_double, flags = "C_CONTIGUOUS"), c_int, ndpointer(c_float, flags = "C_CONTIGUOUS"),ndpointer(c_float, flags = "C_CONTIGUOUS"),  c_char_p]
		self.dso.Areal_Statistics_C.resttype = None

		# Second test version of areal statistcs
		self.dso.Areal_Statistics_C2.argtypes = [c_long,ndpointer(c_double, flags = "C_CONTIGUOUS"),c_int, ndpointer(c_double, flags = "C_CONTIGUOUS"), c_int, ndpointer(c_double, flags = "C_CONTIGUOUS"),  c_char_p]
		self.dso.Areal_Statistics_C2.resttype = None
		# Function for evaluating volume averages at every nav_lev(depth) level
		self.dso.Volume_Statistics_C.argtypes = [c_long,ndpointer(c_double, flags = "C_CONTIGUOUS"),c_int, ndpointer(c_double, flags = "C_CONTIGUOUS"), c_int,ndpointer(c_double, flags = "C_CONTIGUOUS"), c_int, ndpointer(c_float, flags = "C_CONTIGUOUS"),ndpointer(c_float, flags = "C_CONTIGUOUS"),  c_char_p]
		self.dso.Volume_Statistics_C.resttype = None

		# Test volume statistics for better perfomances
		self.dso.Volume_Statistics_C2.argtypes = [c_long,ndpointer(c_double, flags = "C_CONTIGUOUS"),c_int, ndpointer(c_double, flags = "C_CONTIGUOUS"), c_int,ndpointer(c_double, flags = "C_CONTIGUOUS"), c_int, ndpointer(c_double, flags = "C_CONTIGUOUS"),  c_char_p]
		self.dso.Volume_Statistics_C2.resttype = None
		
		self.dso.cwp3.argtypes = [ndpointer(c_double, flags = "C_CONTIGUOUS"),ndpointer(c_double, flags = "C_CONTIGUOUS"),c_long, ndpointer(c_double, flags = "C_CONTIGUOUS"),c_long,ndpointer(c_double, flags = "C_CONTIGUOUS")]
		self.dso.cwp3.resttype = None


		# Functions for adding a vector which lenght is = to len(nav_lev) to a vtk unstructured grid
		self.dso.add_vtkvectorC.argtypes = [ndpointer(c_double, flags = "C_CONTIGUOUS"),c_long, c_char_p]
		self.dso.add_vtkvectorC.resttype = None


	def Statistic_to_vtkUnstructuredGrid(self,data,varname,input_array):
		
		# Routine for adding an array which len = len(nav_lev) to any thresholded vtkUnstructuredGrid
		
		addr = int(data.GetAddressAsString('vtkUnstructuredGrid')[5:], 16)
		self.dso.add_vtkvectorC(input_array,c_long(addr),varname)

	# Developing Area:

	def FlexibleArealStatistic(self,inp,nav,varname,percs = [0.05,0.25,0.5,0.75,0.95]):
		nav_lev = np.array(nav,dtype = np.float64)
		perc = np.array(percs,dtype=np.float64)
		nperc = len(perc)

		Statistics = np.zeros((len(nav)*9),dtype = np.float64)
		addr = int(inp.GetAddressAsString('vtkUnstructuredGrid')[5:], 16)
		self.dso.Areal_Statistics_C2(c_long(addr),nav_lev,c_int(len(nav_lev)), perc, c_int(nperc), Statistics, varname)
		
		return Statistics.reshape((len(nav_lev) , -1))	




	# Slower, but able to handle every kind of thresholded domain
	def FlexibleVolumeStatistic(self,inp,nav,varname,depth_list,percs = [0.05,0.25,0.5,0.75,0.95]):

		nav_lev = -1 * np.array(nav,dtype = np.float64)
		perc = np.array(percs,dtype=np.float64)
		nperc = len(perc)
		Statistics = np.zeros((len(nav)*9),dtype = np.float64)
		addr = int(inp.GetAddressAsString('vtkUnstructuredGrid')[5:], 16)
		self.dso.Volume_Statistics_C2(c_long(addr),np.array(depth_list,dtype=np.float64),c_int(len(depth_list)),nav_lev,c_int(len(nav_lev)), perc, c_int(nperc), Statistics, varname)
		

		return Statistics.reshape((len(nav_lev) , -1))

	def FastArealStatistic(self,Structure,rg,sublist,coast,var,perc = [0.05,0.25,0.5,0.75,0.95],ReturnCutMask = False):
			loadarea = Structure.LoadArea()
			
			area = loadarea[:loadarea.shape[0]-1,:loadarea.shape[1]-1]
			nav = -1 * Structure.nav_lev[:len(Structure.nav_lev)-1]
			nstat = 5 + 4

			data       = npvtk.vtk_to_numpy(rg.GetCellData().GetArray(var))
			basinsmask = npvtk.vtk_to_numpy(rg.GetCellData().GetArray('basins mask'))
			coastmask  = npvtk.vtk_to_numpy(rg.GetCellData().GetArray('coasts mask'))
			
			data   = data.reshape(Structure.jpk-1,Structure.jpj-1,Structure.jpi-1)
			basins = basinsmask.reshape(Structure.jpk-1,Structure.jpj-1,Structure.jpi-1)
			coasts = coastmask.reshape(Structure.jpk-1,Structure.jpj-1,Structure.jpi-1)

			Statistics = np.zeros((len(nav),nstat))	
			CutMask = np.zeros((np.shape(basins)), dtype = np.bool )

			if len(sublist)>=1 and sublist[0] != 'med':
				for sub in sublist:
					if sub == 'alb' :	CutMask[basins == 1] = 1;
					if sub == 'swm1':	CutMask[basins == 2] = 1;
					if sub == 'swm2':	CutMask[basins == 3] = 1;
					if sub == 'nwm' :	CutMask[basins == 4] = 1;
					if sub == 'tyr1':	CutMask[basins == 5] = 1;
					if sub == 'tyr2':	CutMask[basins == 6] = 1;
					if sub == 'adr1':	CutMask[basins == 7] = 1;
					if sub == 'adr2':	CutMask[basins == 8] = 1;
					if sub == 'aeg' :	CutMask[basins == 9] = 1;
					if sub == 'ion1':	CutMask[basins == 10] = 1;
					if sub == 'ion2':	CutMask[basins == 11] = 1;
					if sub == 'ion3':	CutMask[basins == 12] = 1;
					if sub == 'lev1':	CutMask[basins == 13] = 1;
					if sub == 'lev2':	CutMask[basins == 14] = 1;
					if sub == 'lev3':	CutMask[basins == 15] = 1;
					if sub == 'lev4':	CutMask[basins == 16] = 1;
			elif sublist[0] == 'med':
				CutMask[basins > 0] = 1
			else:
				raise ValueError('Error: wrong subbasins list.')

			if   coast == 'open_sea'  : CutMask[coasts != 2] = 0;
			elif coast == 'coast'     :	CutMask[coasts != 1] = 0;
			else:
				if coast != 'everywhere':
					raise ValueError('Error: wrong coast name')	
				

			
			
			bottom = 0
			for k in range(len(nav)):
				
				V = data[k,:,:]
				M = np.copy(CutMask)[k,:,:]

				
				Statistics[k,:] = self.CoreStatistics(np.array(V[M],dtype = np.float64),np.array(area[M],dtype = np.float64),perc)
			if ReturnCutMask:
				return Statistics, CutMask
			else:	
				return Statistics


	# Fast, but works only for subbasins
	def FastVolumeStatistic(self,Structure,rg,sublist,coast,depth_list,var,perc = [0.05,0.25,0.5,0.75,0.95],ReturnCutMask = False):
		loadvol = Structure.LoadVolume()
		volume = loadvol[:loadvol.shape[0]-1,:loadvol.shape[1]-1,:loadvol.shape[2]-1]
		nav = -1 * Structure.nav_lev[:len(Structure.nav_lev)-1]
		nstat = 5 + 4

		data       = npvtk.vtk_to_numpy(rg.GetCellData().GetArray(var))
		basinsmask = npvtk.vtk_to_numpy(rg.GetCellData().GetArray('basins mask'))
		coastmask  = npvtk.vtk_to_numpy(rg.GetCellData().GetArray('coasts mask'))
		
		data   = data.reshape(Structure.jpk-1,Structure.jpj-1,Structure.jpi-1)
		basins = basinsmask.reshape(Structure.jpk-1,Structure.jpj-1,Structure.jpi-1)
		coasts = coastmask.reshape(Structure.jpk-1,Structure.jpj-1,Structure.jpi-1)

		Statistics = np.zeros((len(depth_list),nstat))	
		CutMask = np.zeros((np.shape(basins)), dtype = np.bool )

		if len(sublist)>=1 and sublist[0] != 'med':
			for sub in sublist:
				if sub == 'alb' :	CutMask[basins == 1] = 1;
				if sub == 'swm1':	CutMask[basins == 2] = 1;
				if sub == 'swm2':	CutMask[basins == 3] = 1;
				if sub == 'nwm' :	CutMask[basins == 4] = 1;
				if sub == 'tyr1':	CutMask[basins == 5] = 1;
				if sub == 'tyr2':	CutMask[basins == 6] = 1;
				if sub == 'adr1':	CutMask[basins == 7] = 1;
				if sub == 'adr2':	CutMask[basins == 8] = 1;
				if sub == 'aeg' :	CutMask[basins == 9] = 1;
				if sub == 'ion1':	CutMask[basins == 10] = 1;
				if sub == 'ion2':	CutMask[basins == 11] = 1;
				if sub == 'ion3':	CutMask[basins == 12] = 1;
				if sub == 'lev1':	CutMask[basins == 13] = 1;
				if sub == 'lev2':	CutMask[basins == 14] = 1;
				if sub == 'lev3':	CutMask[basins == 15] = 1;
				if sub == 'lev4':	CutMask[basins == 16] = 1;
		elif sublist[0] == 'med':
			CutMask[basins > 0] = 1
		else:
			raise ValueError('Error: wrong subbasins list.')

		if   coast == 'open_sea'  : CutMask[coasts != 2] = 0;
		elif coast == 'coast'     :	CutMask[coasts != 1] = 0;
		else:
			if coast != 'everywhere':
				raise ValueError('Error: wrong coast name')	
			

		
		
		bottom = 0
		for k in range(len(depth_list)):
			depth = depth_list[k]
			V = data
			M = np.copy(CutMask)
			M[(nav > depth) | (nav < bottom),:,:] = False
			
			Statistics[k,:] = self.CoreStatistics(np.array(V[M],dtype = np.float64),np.array(volume[M],dtype = np.float64),perc)
			bottom = depth
		if ReturnCutMask:
			return Statistics,CutMask 
		else:
			return Statistics

	def CoreStatistics(self,Conc, Weight,perc):
		Statistics      = np.zeros(9,np.float64)

		if Conc.nbytes == 0:
			return Statistics
		Weight_sum      = Weight.sum()
		Mass            = (Conc * Weight).sum()
		Weighted_Mean   = Mass/Weight_sum        
		Weighted_Std    = ((Conc - Weighted_Mean)**2*Weight).sum()/Weight_sum

		Statistics[0]   = Weighted_Mean
		Statistics[1]   = np.sqrt(Weighted_Std)
		#Statistics[2:5] = wp(Conc,Weight,[.25, .50,.75]) #for nstat=5

		Statistics[2]   = Conc.min()
		perc = np.array(perc,dtype = np.float64)
		self.dso.cwp3(Conc,Weight,len(Conc),perc,len(perc),Statistics[3:8])
		
		Statistics[8]   = Conc.max()

		return Statistics

	# Deprecated
	def ArealStatistic(self,inp,nav,varname,DoPercentili = False, percs = []):
		nav_lev =  np.array(nav,dtype = np.float64)
		WM = np.zeros(len(nav_lev),dtype=np.float32)
		perc = np.array(percs,dtype=np.float64)
		nperc = len(perc)
		perc_out=np.zeros((len(nav_lev),nperc),dtype=np.float32)
		perc_out = perc_out.reshape(1,-1)
		area = []
		addr = int(inp.GetAddressAsString('vtkUnstructuredGrid')[5:], 16)
		self.dso.Areal_Statistics_C(c_long(addr),nav_lev,c_int(len(nav_lev)), perc, c_int(nperc), WM, perc_out, varname)
		perc_out = perc_out.reshape((len(nav_lev) , nperc))
		return WM,perc_out		

	def VolumeStatistic(self,inp,nav,varname,depth_list,DoPercentili = False, percs = []):

		nav_lev =  np.array(nav,dtype = np.float64)
		WM = np.zeros(len(nav_lev),dtype=np.float32)
		perc = np.array(percs,dtype=np.float64)
		nperc = len(perc)
		perc_out=np.zeros((len(nav_lev),nperc),dtype=np.float32)
		perc_out = perc_out.reshape(1,-1)
		loc_volume = []
		addr = int(inp.GetAddressAsString('vtkUnstructuredGrid')[5:], 16)
		self.dso.Volume_Statistics_C(c_long(addr),-1 * np.array(depth_list,dtype=np.float64),c_int(len(depth_list)),nav_lev,c_int(len(nav_lev)), perc, c_int(nperc), WM, perc_out, varname)
		perc_out = perc_out.reshape((len(nav_lev), nperc))

		return WM,perc_out

	def test2_VolumeStatistic(self,sub,coast,nav,depth_list,var):
		path = '/home/cosimo/MHPC/OGS/Tesi/code_repo/TestCosimo/MESHMASK/mid'
		volume = np.load(str(path)+'/volume.npy')
		coastmask = np.load(str(path)+'/'+str(coast)+'.npy')
		submask = np.load(str(path)+'/'+str(sub)+'.npy')
		nstat = 5 + 4
		Statistics = np.zeros((len(depth_list),nstat))	
		mask = submask & coastmask
		bottom = 0
		for k in range(len(depth_list)):
			depth = depth_list[k]
			V = var
			M = np.copy(mask)
			M[nav > depth,:,:] = False
			M[nav < bottom,:,:] = False
			
			Statistics[k,:] = self.CoreStatistics(np.array(V[M],dtype = np.float64),np.array(volume[M],dtype = np.float64))
			bottom = depth
		return Statistics

	def test2_ArealStatistic(self,sub,coast,jpk,var):
		path = '/home/cosimo/MHPC/OGS/Tesi/code_repo/TestCosimo/MESHMASK/mid'
		area = np.load(str(path)+'/area.npy')
		coastmask = np.load(str(path)+'/'+str(coast)+'.npy')
		submask = np.load(str(path)+'/'+str(sub)+'.npy')
		nstat = 5 + 4
		Statistics = np.zeros((jpk,nstat))	
		mask = submask & coastmask

		for k in range(jpk):
			V = var[k,:,:]
			M = mask[k,:,:]
			Statistics[k,:] = self.CoreStatistics(np.array(V[M],dtype = np.float64),np.array(area[M],dtype = np.float64))

		return Statistics

if __name__ == "__main__":
	import time
	tot_start = time.time()
	
	Structure = OGSvtk('/home/cosimo/MHPC/OGS/MESHMASK')
	Structure.SetResolution('mid')
	rg = Structure.CreateRectilinearGrid()
	AddMask_start = time.time()
	Structure.AddMask(rg)
	AddMask_end = time.time()
	
	jpk = Structure.jpk 
	jpj = Structure.jpj 
	jpi = Structure.jpi

	
	
	data = Structure.LoadNetCDF.GetVariable('/home/cosimo/MHPC/OGS/INPUT/ave.19990116-12:00:00.N3n.nc','N3n')

	#test_data = np.ones((jpk,jpj,jpi))
	#rg.GetCellData().AddArray(createVTKscaf("N3n",data,jpi,jpj,jpk))
	
	#rg.GetCellData().AddArray(Structure.createVTKscaf("test",test_data))
	AddScaf_start = time.time()
	rg.GetCellData().AddArray(Structure.createVTKscaf("N3n",data))
	AddScaf_end = time.time()

	ThresTime_start = time.time()
	t = vtk.vtkThreshold()
	t.SetInputData(rg)
	t.ThresholdBetween(0.1,1.1)
	t.SetInputArrayToProcess(0,0,0,1,'basins mask')
	t.Update()

	ThreshTime_end = time.time()
	thresh_rg = t.GetOutput()
	#thresh_data = npvtk.vtk_to_numpy(thresh_rg.GetCellData().GetArray("N3n"))
	Statistic = OGSStatistics('/home/cosimo/MHPC/OGS/clib/src')
	#EvalVolStat_start = time.time() 
	#newstat_start = time.time()
	#WM_t,perc_out_t = Statistic.test_ArealStatistic(inp = thresh_rg,nav = Structure.nav_lev,varname = "test",DoPercentili = True, percs = [.05,.25,.5,.75,.9])
	#newstat = time.time() - newstat_start
	#oldstat_start = time.time()
	#WM,perc_out	 = Statistic.ArealStatistic(inp = thresh_rg,nav = Structure.nav_lev,varname = "P_l",DoPercentili = True, percs = [.05,.25,.5,.75,.9])
	#oldstat = time.time() - oldstat_start
	

	#print WM_t,WM
	#print WM - WM_t
	#print perc_out_t - perc_out
	#print "new stat = %f, old stat = %f" % (newstat,oldstat)
	

	data_prova = data[:data.shape[0]-1,:data.shape[1]-1,:data.shape[2]-1]
	# print basinsmask.shape
	# print data.shape
	# print data_prova.shape
	perc = [.05,.25,.5,.75,.95]
	oldstat_start = time.time()
	FStatistics = Statistic.FlexibleVolumeStatistic(inp = thresh_rg,nav = Structure.nav_lev,varname = "N3n", depth_list = [200*1000,600*1000,6000*1000],DoPercentili = True, percs = [.05,.25,.5,.75,.95])
	#WM,perc_out	 = Statistic.ArealStatistic(inp = thresh_rg,nav = Structure.nav_lev,varname = "N3n",DoPercentili = True, percs = [.05,.25,.5,.75,.9])
	#FStatistics = Statistic.FlexibleArealStatistic(inp = thresh_rg,nav = Structure.nav_lev,varname = "N3n",DoPercentili = True, percs = [.05,.25,.5,.75,.95])
	#Statistics = Statistic.test2_VolumeStatistic('med','everywhere',Structure.nav_lev,[200*1000,600*1000,6000*1000],data)

	oldstat = time.time() - oldstat_start

	newstat_start = time.time()
	#WM_t,perc_out_t = Statistic.VolumeStatistic(inp = thresh_rg,nav = Structure.nav_lev,varname = "N3n" , depth_list = [200*1000,1000*1000,2000*1000],DoPercentili = True, percs = [.05,.25,.5,.75,.9])
	#Statistics = Statistic.test2_ArealStatistic('med','everywhere',Structure.jpk,data)

	#Statistics = Statistic.FastVolumeStatistic(Structure,rg,['med'],'everywhere',[200*1000,600*1000,6000*1000],"N3n",perc)
	Statistics = Statistic.FastArealStatistic(Structure,rg,['alb'],'everywhere',"N3n")

	newstat = time.time() - newstat_start
	
	# data = np.zeros((len(Structure.nav_lev),6))
	# data[:,0] = WM
	# data[:,1:] = perc_out
	np.savetxt('Farea_test.txt',FStatistics)
	np.savetxt('Sarea_test.txt',Statistics)
	#print perc_out - perc_out_t
	#for z in range(thresh_rg.GetNumberOfCells()):



	print "new stat = %f, old stat = %f" % (newstat,oldstat)

	#EvalVolStat_end = time.time()

	#AddStattoGrid_start = time.time()
	#Statistic.Statistic_to_vtkUnstructuredGrid(thresh_rg,"test avg",WM_t)
	#AddStattoGrid_end = time.time()

	# for i in range(len(perc_out_t[0])):
	# 	perc_aux = np.array(perc_out[:,i])
	# 	perc_aux_t = np.array(perc_out_t[:,i])
	# 	Statistic.Statistic_to_vtkUnstructuredGrid(thresh_rg,"new perc "+str(i),perc_aux_t)
	# 	Statistic.Statistic_to_vtkUnstructuredGrid(thresh_rg,"old perc "+str(i),perc_aux)

	#Write_start = time.time()
	#Structure.WriteGridToVtk(thresh_rg,'write_test.vtr')
	#Write_end = time.time()
	#tot_end = time.time()
	#print "total	AddMask 	AddScaf 	Threshold 	EvalStat 	AddStat 	WriteVtk"
	#print tot_end-tot_start,AddMask_end-AddMask_start,AddScaf_end - AddScaf_start, ThreshTime_end - ThresTime_start, EvalVolStat_end -EvalVolStat_start, AddStattoGrid_end - AddStattoGrid_start, Write_end - Write_start
	#print WM,perc_out
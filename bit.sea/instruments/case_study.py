from lovbio_float import BioFloat, BioFloatProfile
import pylab as pl

filename='/gss/gss_work/DRES_OGS_BiGe/Observations/TIME_RAW_DATA/ONLINE/FLOAT_LOVBIO/6901511/output_lovbio035b_277_00.nc'

F=BioFloat.from_file(filename)

Pres,Profile, Profile_adj, Qc = F.read_very_raw('CHLA')
fig, ax = F.plot(Pres,Profile,linestyle='None', marker='.', color='r')
fig, ax = F.plot(Pres,Profile_adj, fig, ax, linestyle='None', marker='.', color='g')
ax.grid()
fig.show()
DIFF = Profile_adj-Profile
fig,ax = pl.subplots()

fig,ax = F.plot(Pres,DIFF,linestyle='None', marker='.')
fig.show()


Pres,Profile, Qc     = F.read_raw('CHLA',read_adjusted=False)
Pres,Profile_adj, Qc = F.read_raw('CHLA',read_adjusted=True)

fig, ax = F.plot(Pres,Profile,color='r') #linestyle='_', marker='.', color='r')
fig, ax = F.plot(Pres,Profile_adj, fig, ax, color='g')
ax.grid()
fig.show()

p=BioFloatProfile(F.time,F.lon,F.lat,F,F.available_params)
from basins.region import Rectangle
from commons.time_interval import TimeInterval
delta=0.01
r = Rectangle(F.lon-delta,F.lon+delta,F.lat-delta, F.lat+delta)
from bio_float import FloatSelector
TI = TimeInterval('20150520','20150830','%Y%m%d')
Coriolis_Profile_List= FloatSelector('CHLA', TI, r)
Fc = Coriolis_Profile_List[0]._my_float

CPres,CProfile, CProfile_adj, CQc = Fc.read_very_raw('CHLA')
fig, ax = Fc.plot(CPres,CProfile,linestyle='None', marker='.', color='r')
fig, ax = Fc.plot(CPres,CProfile_adj, fig, ax, linestyle='None', marker='.', color='g')
ax.grid()
fig.show()

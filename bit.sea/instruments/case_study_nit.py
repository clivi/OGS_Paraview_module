from lovbio_float import BioFloat, BioFloatProfile
import pylab as pl

#filename='/gss/gss_work/DRES_OGS_BiGe/Observations/TIME_RAW_DATA/ONLINE/FLOAT_LOVBIO/6901766/output_lovbio085d_016_00.nc' # bad case
filename='/gss/gss_work/DRES_OGS_BiGe/Observations/TIME_RAW_DATA/ONLINE/FLOAT_LOVBIO/6901769/output_lovbio089d_080_00.nc'

F=BioFloat.from_file(filename)

Pres,NO3, NO3_ADJ, Qc = F.read_very_raw('NO3')
Pres,SR_NO3, SR_NO3_ADJ, Qc = F.read_very_raw('SR_NO3')
Pres,NO3_GLODAP_ADJ, _, Qc = F.read_very_raw('SR_NO3_GLODAP_ADJUSTED')

fig,ax=pl.subplots()

F.plot(Pres,NO3    ,fig,ax, linestyle='None', marker='.',label='NO3')
F.plot(Pres,NO3_ADJ,fig,ax, linestyle='None', marker='o',color='None',markeredgecolor='r',label='NO3_ADJ')
F.plot(Pres,SR_NO3,fig,ax, linestyle='None', marker='.',color='g',label='SR_NO3')


F.plot(Pres,SR_NO3_ADJ,fig,ax, linestyle='None', marker='.',color='k',label='SR_NO3_ADJ')
F.plot(Pres,NO3_GLODAP_ADJ,fig,ax, linestyle='None', marker='o',color='None',markeredgecolor='c',label='NO3_GLODAP_ADJ')
ax.legend(loc=3)
ax.grid()
ax.set_ylim(800,0)
fig.show()

import sys
sys.exit()




#zoom(0-400 , -1 15 in valore)

F.read('CHLA',read_adjusted=True)
F.read('DOXY',read_adjusted=False)
F.read('SR_NO3',read_adjusted=True)



import os
import numpy as np
LAST=np.zeros((124),np.float32)
for cycle in range(124):
    filename='/gss/gss_work/DRES_OGS_BiGe/Observations/TIME_RAW_DATA/ONLINE/FLOAT_LOVBIO/6901766/output_lovbio085d_%03d_00.nc' %cycle
    if not os.path.exists(filename) :
        LAST[cycle]=np.nan
        continue
    F=BioFloat.from_file(filename)
    Pres,NO3, NO3_ADJ, Qc = F.read_very_raw('NO3')
    ii=np.isnan(NO3)
    LAST[cycle] = NO3[~ii][-1]

fig, ax = pl.subplots()
ax.plot(LAST,'.')
ax.set_xlabel('cycle')
ax.set_ylabel('NO3')
ax.grid()
fig.suptitle('bottom value')
#fig.set_size_inches()
fig.savefig('lovbio085d_nitrate_bottom.png')

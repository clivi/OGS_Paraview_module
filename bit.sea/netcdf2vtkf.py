#!/usr/bin/env pvpython
#
# NetCDF to VTK functions
#
# This file contains all functions needed to convert
# netCDF4 files into vtk files, used in Paraview.
#
# Arnau Miro, SoHPC 2017

import os, warnings, vtk
import netCDF4 as NC4
import numpy as np
from numpy import nanmean
from vtk.util import numpy_support as npvtk

def datestr2datenum(datestr):
	'''
	From a given date string, convert it to a single numerical
	value that represents the given date. The first 8 digits
	before the point represent the date whereas the 5 digits after
	the point represent the time.

	Inputs:
		- datestr: string containing the date in the format
					YYYYMMDD-HH:MM:SS

	Outputs:
		- datenum: numerical representation of the date.
	'''
	# Separate date and time
	datestrp = datestr.split('-');
	date = datestrp[0]; time = datestrp[1];
	# Convert it to a floating point number
	timep = time.split(':');
	timenum = float(timep[0])*3600 + \
    	      float(timep[1])*60   + \
        	  float(timep[2]);
	return(float(date) + timenum*1e-5);

def datenum2datestr(datenum):
	'''
	From a given date in a numerical representation, return
	the date in the datestr format of YYYYMMDD-HH:MM:SS.

	Inputs:
		- datenum: numerical representation of the date.

	Outputs:
		- datestr: string containing the date in the format
					YYYYMMDD-HH:MM:SS
	'''
	# Separate time and date
	date    = int(datenum);
	timenum = int((datenum-date)*1e5);
	# Convert time
	hour = int(timenum/3600);
	minu = int((timenum-hour*3600)/60);
	seco = int(timenum-hour*3600-minu*60);
	# Generate datestr
	return( "%d-%02d:%02d:%02d" % (date,hour,minu,seco) );

def ReadMeshmask(file):
	'''
    '''
	# Check file exists
	if not os.path.isfile(file):
		raise ValueError("Cannot open file <%s>! Aborting..." % file);
	# Open the file and get dimensions
	mask = NC4.Dataset(file,"r");
	jpi=mask.dimensions['x'].size;
	jpj=mask.dimensions['y'].size;
	jpk=mask.dimensions['z'].size;
	# Get nav_lev
	nav_lev =  np.abs(mask.variables['nav_lev']).copy();
	# Close
	mask.close(); # Once done, close the file
	# Return
	return jpi,jpj,jpk,nav_lev

def readNetCDF(filename,varname,jpk,jpj,jpi,maxval=1e20):
	'''
	Reads information inside a NetCDF file and returns a parsed
	numpy array with 0. when val > maxval. Also cuts the file 
	according to jpk, jpj and jpi.

	If the file cannot be read, the function aborts.

	Inputs:
		filename: full path to netcdf4 file
		varname:  name of the variable to extract
		jpk: z dimension max
		jpj: y dimension max
		jpi: x dimension max
		maxval: value from which start to conver to 0. [default = 1e20]

	Outputs:
		data: numpy array with variable data
	'''
	# Check if file exists
	if not os.path.isfile(filename):
		raise ValueError("Cannot open file <%s>!" % filename);
	# Read raw data from NetCDF
	data_raw = NC4.Dataset(filename,"r");
	data_raw.set_auto_mask(False); # Disable masked array behaviour
	data     = data_raw.variables[varname][0,:jpk,:,:];
	data_raw.close(); # Close file

	# Now we will check if the data needs to be cut
	dpk, dpj, dpi = data.shape; # Get the array's dimensions
	# Compute the difference
	diffk = abs(dpk-jpk); # Always must be zero since it is cut before
	if (diffk != 0): raise ValueError("something went wrong!");
	diffj = abs(dpj-jpj);
	diffi = abs(dpi-jpi);
	# Cut the data
	data = data[:,diffj:,diffi:];

	# Fix for missing values
	data[ data >= maxval ] = 0.;

	# Return processed data
	return data;

def createSeaMask(data):
	'''
	Creates a numpy array that can be used as a variable to distinguish
	sea from land. The values are:
		0:   Sea
		0.4: Atlantic buffer
		0.6: Isolated zones on the Iberic peninsula and Bay of Biscay
		1:   Land

	Input:
		data: Variable data in which to compute the mask

	Output:
		mask: Sea mask data
	'''
	# Create empty array
	mask = np.copy(data)*0;

	# Eliminate Bay of Biscay and small isolated zones west of iberian peninsula
	mask[:,0:170,:80] = 0.4; # Atlantic buffer
	mask[:,170:,:10]  = 0.6; # Isolated zones cut
	mask[:,300:,:200] = 0.6; # Bay of Biscay cut

	# Get land values (Land overimposed to sea)
	mask[ data == 0 ] = 1;

	# Return
	return mask;

def faces2cellcenter(U,V,W,jpk,jpj,jpi):
	'''
	Convers a face centered velocity into a cell centered field.
	Useful for further use on VTK grids.

	Inputs:
		U: 1st component of the velocity (face centered)
		V: 2nd component of the velocity (face centered)
		W: 3rd component of the velocity (face centered)
		jpk: z dimension max
		jpj: y dimension max
		jpi: x dimension max

	Outputs:
		U: 1st component of the velocity (cell centered)
		V: 2nd component of the velocity (cell centered)
		W: 3rd component of the velocity (cell centered)

	'''
	# Start interpolation
	aux = np.zeros([jpk,jpj,1])*np.NaN; Up1  = np.append(U,aux,2);
	aux = np.zeros([jpk,1,jpi])*np.NaN; Vp1  = np.append(V,aux,1);
	aux = np.zeros([1,jpj,jpi])*np.NaN; Wp1  = np.append(W,aux,0);

	# Copy and roll
	Up1r = Up1.copy(); Up1r = np.roll(Up1r,1,axis=2);  Up1r[:,:,0]=np.NaN;
	Vp1r = Vp1.copy(); Vp1r = np.roll(Vp1r,1,axis=1);  Vp1r[:,0,:]=np.NaN;
	Wp1r = Wp1.copy(); Wp1r = np.roll(Wp1r,-1,axis=0); Wp1r[jpk-1,:,:]=np.NaN;

	# Fixed correct syntaxis using nanmean from numpy instead of scipy
	with warnings.catch_warnings():
		# We anticipate catching the warnings to to averaging a row of only NaN
		warnings.simplefilter("ignore", category=RuntimeWarning);
		Uint = nanmean( np.array([Up1r,Up1]),axis=0);
		Vint = nanmean( np.array([Vp1r,Vp1]),axis=0);
		Wint = nanmean( np.array([Wp1r,Wp1]),axis=0);

	# Return to original dimensions
	Uint = Uint[:jpk,:jpj,:jpi];
	Vint = Vint[:jpk,:jpj,:jpi];
	Wint = Wint[:jpk,:jpj,:jpi];
	
	# Return value
	return Uint, Vint, Wint;

def createVTKRectilinearGrid(Lon2MetersF,Lat2MetersF,nav_lev,jpi,jpj,jpk):
	'''
	Create a VTK rectilinear grid for further postprocessing.
	Also converts latitude and longitude values into meters.

	Inputs:	
		Lon2MetersF: .npy file containing the conversion of 
					 longitude to meters.
		Lat2MetersF: .npy file containing the conversion of
					 latitude to meters.
		nav_lev: Depth values.
		jpk: z dimension max.
		jpj: y dimension max.
		jpi: x dimension max.

	Outputs:
		rg: VTK rectilinear grid.
		x:  X coordinate of the VTK grid.
		y:  Y coordinate of the VTK grid.
		z:  Z coordinate of the VTK grid.
	'''
	# Load the conversion
	# LonLat2Meters must have been executed beforehand!
	Lon2Meters=np.load(Lon2MetersF);
	Lat2Meters=np.load(Lat2MetersF);

	# Create variable arrays
	x = -1.*Lon2Meters; # x coordinate
	y = Lat2Meters;     # y coordinate
	z = 1000.*nav_lev;  # z coordinate

	# Create VTK arrays
	daLon = npvtk.numpy_to_vtk(x.ravel(),True,vtk.VTK_FLOAT);
	daLat = npvtk.numpy_to_vtk(y.ravel(),True,vtk.VTK_FLOAT);
	daDep = npvtk.numpy_to_vtk(z.ravel(),True,vtk.VTK_FLOAT);

	# Create a vtk grid
	rg = vtk.vtkRectilinearGrid();
	rg.SetDimensions(jpi,jpj,jpk);

	# Set the coordinates into the rectilinear grid
	rg.SetXCoordinates(daLon);
	rg.SetYCoordinates(daLat);
	rg.SetZCoordinates(daDep);

	# Return the rectilinear grid
	return rg;

def createVTKRectilinearGridP(output,Lon2MetersF,Lat2MetersF,nav_lev,jpi,jpj,jpk):
	'''
	Create a VTK rectilinear grid for further postprocessing.
	Also converts latitude and longitude values into meters.

	Inputs:	
		Lon2MetersF: .npy file containing the conversion of 
					 longitude to meters.
		Lat2MetersF: .npy file containing the conversion of
					 latitude to meters.
		nav_lev: Depth values.
		jpk: z dimension max.
		jpj: y dimension max.
		jpi: x dimension max.

	Outputs:
		rg: VTK rectilinear grid.
		x:  X coordinate of the VTK grid.
		y:  Y coordinate of the VTK grid.
		z:  Z coordinate of the VTK grid.
	'''
	# Load the conversion
	# LonLat2Meters must have been executed beforehand!
	Lon2Meters=np.load(Lon2MetersF);
	Lat2Meters=np.load(Lat2MetersF);

	# Create variable arrays
	x = -1.*Lon2Meters; # x coordinate
	y = Lat2Meters;     # y coordinate
	z = 1000.*nav_lev;  # z coordinate

	# Create VTK arrays
	daLon = npvtk.numpy_to_vtk(x.ravel(),True,vtk.VTK_FLOAT);
	daLat = npvtk.numpy_to_vtk(y.ravel(),True,vtk.VTK_FLOAT);
	daDep = npvtk.numpy_to_vtk(z.ravel(),True,vtk.VTK_FLOAT);

	# Create a vtk grid
	rg = output.GetOutput();
	rg.SetDimensions(jpi,jpj,jpk);

	# Set the coordinates into the rectilinear grid
	rg.SetXCoordinates(daLon);
	rg.SetYCoordinates(daLat);
	rg.SetZCoordinates(daDep);

	# Return the rectilinear grid
	return rg;

def createVTKscaf(varname,data,jpi,jpj,jpk,type=vtk.VTK_FLOAT):
	'''
	Creates a VTK scalar field array from variable data
	with a specific variable name.

	Inputs:
		varname: VTK variable name.
		data: numpy array containing variable data.
		jpk: z dimension max.
		jpj: y dimension max.
		jpi: x dimension max.
		type (optional): VTK data type

	Outputs:
		scaf: VTK scalar field.
	'''
	# Create vtk array from numpy data
	scaf = npvtk.numpy_to_vtk(data[:(jpk-1),:(jpj-1),:(jpi-1)].ravel(),
		                      True,type);
	# Set variable name
	scaf.SetName(varname);

	# Return scalar field
	return scaf

def createVTKvecf3(varname,xdata,ydata,zdata,jpi,jpj,jpk,type=vtk.VTK_FLOAT):
	'''
	Creates a VTK 3D vector field array from variable data
	with a specific variable name.

	Inputs:
		varname: VTK variable name.
		xdata: numpy array containing variable data of 1st dimension.
		ydata: numpy array containing variable data of 2nd dimension.
		zdata: numpy array containing variable data of 3rd dimension.
		jpk: z dimension max.
		jpj: y dimension max.
		jpi: x dimension max.

	Outputs:
		vecf: VTK vector field.
	'''
	# Concatenate 3 dimensional vector
	data = np.concatenate((xdata[:(jpk-1),:(jpj-1),:(jpi-1),np.newaxis],
		                   ydata[:(jpk-1),:(jpj-1),:(jpi-1),np.newaxis],
		                   zdata[:(jpk-1),:(jpj-1),:(jpi-1),np.newaxis]),
						  axis=3);
	# Create vtk array from numpy data
	vecf = npvtk.numpy_to_vtk(data.ravel(),True,type);
	# Set variable name and number of components
	vecf.SetName(varname);
	vecf.SetNumberOfComponents(3);
	vecf.SetNumberOfTuples( (jpi-1)*(jpj-1)*(jpk-1) );
	# Return scalar field
	return vecf

def createVTKstrf(varname,data):
	'''
    Creates a VTK string field array from variable data
    with a specific variable name.

    Inputs:
        varname: VTK variable name.
        data: string containing variable data.

    Outputs:
        strf: VTK string field.  
    '''
	# Create string array
	strf = vtk.vtkStringArray();
	# Set properties
	strf.SetName(varname);
	strf.SetNumberOfTuples(1);
	# Set value
	strf.SetValue(0,data);
	# Return
	return strf;

def createVTKdatetime(varname,datestr):
	'''
	Creates a VTK field where to store time data in datenum
	format for a specific file.

	Inputs:
		varname: VTK variable name.
		datestr: string containing the date in the format
		         YYYYMMDD-HH:MM:SS

	Outputs:
		field: VTK field.
	
	'''
	# Create float array
	field = vtk.vtkFloatArray();
	field.SetName(varname);
	field.SetNumberOfTuples(1);
	# Populate array
	field.SetTuple1(0,datestr2datenum(datestr));
	# Return value
	return field

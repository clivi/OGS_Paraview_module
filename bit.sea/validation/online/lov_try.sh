#! /bin/bash

export MASKFILE=/gpfs/work/OGS_prod/OPA/V2C/prod/wrkdir/2/MODEL/meshmask.nc
STARTTIME_a=20161029
END__TIME_a=20161031
STARTTIME_f=20161101
END__TIME_f=20161104
TMP_DIR_p=/gpfs/work/OGS_prod/OPA/V2C/prod/wrkdir/2/POSTPROC/AVE_FREQ_1/online_validation/PREVIOUS/TMP
TMP_DIR_a=/gpfs/work/OGS_prod/OPA/V2C/prod/wrkdir/2/POSTPROC/AVE_FREQ_1/TMP
PROFILERDIR_p=$PWD/validation_dir/PREVIOUS/PROFILATORE
PROFILERDIR_a=$PWD/validation_dir/ACTUAL/PROFILATORE
IMAGES_DIR_p=$PWD/validation_dir/PREVIOUS/matchup_outputs
IMAGES_DIR_a=$PWD/validation_dir/ACTUAL/matchup_outputs
IMAGES_DIR=$PWD/validation_dir/matchup_outputs

mkdir -p $PROFILERDIR_a $PROFILERDIR_p
mkdir -p $IMAGES_DIR_a $IMAGES_DIR_p

python float_extractor.py -st ${STARTTIME_a} -et ${END__TIME_a} -i $TMP_DIR_p -b $PROFILERDIR_p  -o $IMAGES_DIR_p -m $MASKFILE
python float_extractor.py -st ${STARTTIME_f} -et ${END__TIME_f} -i $TMP_DIR_p -b $PROFILERDIR_p  -o $IMAGES_DIR_p -m $MASKFILE
python float_extractor.py -st ${STARTTIME_f} -et ${END__TIME_f} -i $TMP_DIR_a -b $PROFILERDIR_a  -o $IMAGES_DIR_a -m $MASKFILE



mkdir -p $IMAGES_DIR
VARDESCR=$IMAGES_DIR/BioFloats_Descriptor.xml
python profileplotter_3.py -p $IMAGES_DIR_p -a $IMAGES_DIR_a  -o $IMAGES_DIR -f $VARDESCR


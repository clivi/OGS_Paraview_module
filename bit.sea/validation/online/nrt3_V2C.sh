#! /bin/bash
HERE=$PWD
MASKFILE=/pico/home/usera07ogs/a07ogs00/OPA/V4/etc/static-data/MED1672_cut/MASK/meshmask.nc
INPUTDIR=/pico/scratch/userexternal/gbolzon0/NRT/V2C/wrkdir/ANALYSIS_over_2_weeks/
BASEDIR=$INPUTDIR/PROFILATORE
ACTUALDIR=/pico/home/usera07ogs/a07ogs00/OPA/V2C/wrkdir/2/POSTPROC/AVE_FREQ_1/TMP
PREV_DIR=/pico/home/usera07ogs/a07ogs00/OPA/V2C/wrkdir/2/POSTPROC/AVE_FREQ_1/online_validation/PREVIOUS/TMP
mkdir -p $INPUTDIR/TMP/ $INPUTDIR/PROFILATORE/PROFILES

cd $INPUTDIR/TMP
#rm -f *nc
for I in `ls $PREV_DIR/*nc`; do
   ln -fs $I
done

for I in `ls $ACTUALDIR/*nc`; do
   ln -fs $I
done

ACTUALDIR=/pico/home/usera07ogs/a07ogs00/OPA/V2C/wrkdir/2/POSTPROC/AVE_FREQ_1/online_validation/ACTUAL/PROFILATORE/PROFILES/
PREV_DIR=/pico/home/usera07ogs/a07ogs00/OPA/V2C/wrkdir/2/POSTPROC/AVE_FREQ_1/online_validation/PREVIOUS/PROFILATORE/PROFILES
cd $INPUTDIR/PROFILATORE/PROFILES
for I in `ls $PREV_DIR/*nc`; do
   ln -fs $I
done

for I in `ls $ACTUALDIR/*nc`; do
   ln -fs $I
done

DATE=20160816
OUTDIR=/pico/scratch/userexternal/gbolzon0/NRT/V2C/NRT3_outputs
OUTFILE=$OUTDIR/BioFloat_Weekly_validation_${DATE}.nc
cd $HERE
#python nrt3.py -o $OUTFILE -b $BASEDIR -m $MASKFILE -i $INPUTDIR/TMP -d $DATE

#exit 0

# history ---------------

INPUTDIR=/pico/scratch/userexternal/gbolzon0/NRT/V2C/TMP/
BASEDIR=/pico/scratch/userexternal/gbolzon0/NRT/V2C/PROFILATORE/

OUTDIR=/pico/scratch/userexternal/gbolzon0/NRT/V2C/NRT3_outputs
timelistfile=timelist_V2C_recupero.txt
for DATE in `cat $timelistfile`; do
   echo $DATE
   mkdir ${OUTDIR}/${DATE}
   #act_file=${OUTDIR}/${DATE}.nc
   OUTFILE=${OUTDIR}/BioFloat_Weekly_validation_${DATE}.nc
   #mv $act_file $OUTFILE 
   python nrt3.py -o $OUTFILE -b $BASEDIR -m $MASKFILE -i $INPUTDIR -d $DATE
done

#! /bin/bash
MASKFILE=/pico/home/usera07ogs/a07ogs00/OPA/V4/etc/static-data/MED1672_cut/MASK/meshmask.nc
INPUTDIR=/pico/scratch/userexternal/gbolzon0/NRT/V4/TMP/
BASEDIR=/pico/scratch/userexternal/gbolzon0/NRT/V4/PROFILATORE/

OUTDIR=/pico/scratch/userexternal/gbolzon0/NRT/V4/NRT3_outputs
timelistfile=timelist.txt
for DATE in `cat $timelistfile`; do
   echo $DATE
   mkdir ${OUTDIR}/${DATE}
   act_file=${OUTDIR}/${DATE}.nc
   OUTFILE=${OUTDIR}/${DATE}/BioFloat_Weekly_validation_${DATE}.nc
   mv $act_file $OUTFILE 
   #python nrt3.py -o $OUTFILE -b $BASEDIR -m $MASKFILE -i $INPUTDIR -d $DATE
done
